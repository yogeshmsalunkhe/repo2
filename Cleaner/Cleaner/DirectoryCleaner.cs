﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Added
using System.IO;
using System.Text.RegularExpressions;

namespace Cleaner
{
    class DirectoryCleaner
    {
        public static async Task CleanDirectoriesAsync()
        {
            var cleanDotNetDirTask = CleanDotNetTempDirectoriesAsync();
            await cleanDotNetDirTask;

            var cleanOtherDirTask = CleanOtherDirectoriesAsync();
            await cleanOtherDirTask;
        }

        private static async Task CleanDotNetTempDirectoriesAsync()
        {
            Console.WriteLine("Cleaning .Net Temp Directories...");
            var directory1Task = CleanDirectoryAsync(@"C:\Windows\Microsoft.NET\Framework\v2.0.50727\Temporary ASP.NET Files");
            var directory2Task = CleanDirectoryAsync(@"C:\Windows\Microsoft.NET\Framework\v4.0.30319\Temporary ASP.NET Files");
            var directory3Task = CleanDirectoryAsync(@"C:\Windows\Microsoft.NET\Framework64\v2.0.50727\Temporary ASP.NET Files");
            var director4Task = CleanDirectoryAsync(@"C:\Windows\Microsoft.NET\Framework64\v4.0.30319\Temporary ASP.NET Files");

            await Task.WhenAll(directory1Task, directory2Task, directory2Task, director4Task);
            Console.WriteLine("Cleaned .Net Temp Directories...");
        }

        private static async Task CleanOtherDirectoriesAsync()
        {
            Console.WriteLine("Cleaning AppData Temp Directory and IIS Logs...");

            var directory1Task = CleanDirectoryAsync(@"C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\12\LOGS");
            var directory2Task = CleanDirectoryAsync(@"C:\inetpub\logs\LogFiles");
            
            string appDataTempPath = Path.GetTempPath();
            var match = Regex.Match(appDataTempPath, @".*(\\Temp)");
            if (match.Success)
            {
                appDataTempPath = match.Value;
            }
            var directory3Task = CleanDirectoryAsync(appDataTempPath);

            await Task.WhenAll(directory1Task, directory2Task, directory3Task);
            Console.WriteLine("Cleaned AppData Temp Directory and IIS Logs...");
        }

        private static Task CleanDirectoryAsync(string dirPath)
        {           
            return Task.Run(() =>
            {
                CleanDirectory(dirPath);
            });
        }

        private static void CleanDirectory(string dirPath)
        {
            try
            {
                if (!Directory.Exists(dirPath))
                    return;

                foreach (var filePath in Directory.GetFiles(dirPath))
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }

                foreach (var directoryPath in Directory.GetDirectories(dirPath))
                {
                    try
                    {
                        Directory.Delete(directoryPath, true);
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error while cleaning directory {0}. {1}", dirPath, ex.Message));
            }
        }
    }
}
