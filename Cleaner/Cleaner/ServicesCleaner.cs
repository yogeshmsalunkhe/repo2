﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Added
using System.ServiceProcess;
using System.Diagnostics;

namespace Cleaner
{
    public class ServicesCleaner
    {
        public static async Task RestartServicesAsync()
        {
            var restartMSSqlServerTask = RestartMSSqlServerAsync();
            var iisRestartTask = IISRestartAsync();

            await Task.WhenAll(restartMSSqlServerTask, iisRestartTask);
        }

        private static Task RestartMSSqlServerAsync()
        {
            return Task.Run(() =>
            {
                RestartMSSqlServer();
            });
        }

        private static void RestartMSSqlServer()
        {
            try
            {
                ServiceController[] services = ServiceController.GetServices();
                ServiceController service = new ServiceController("MSSQLSERVER");
              
                if (service.Status != ServiceControllerStatus.Stopped)
                {
                    Console.WriteLine("Attempting SQL Server Stop...");
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped);
                    Console.WriteLine("SQL Server Stopped...");
                }
                Console.WriteLine("Attempting SQL Server Start...");
                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running);                
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error while restarting SQL Server. {0}", ex.Message));
            }
        }

        private static Task IISRestartAsync()
        {
            return Task.Run(() =>
            {
                IISRestart();
            });
        }

        private static void IISRestart()
        {
            try
            {
                Console.WriteLine("IIS Restarting...");
                Process iisReset = new Process();
                iisReset.StartInfo.FileName = "iisreset.exe";
                iisReset.StartInfo.RedirectStandardOutput = true;
                iisReset.StartInfo.UseShellExecute = false;
                iisReset.Start();
                iisReset.WaitForExit();                
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error while restarting IIS. {0}", ex.Message));
            }
        }
    }
}
