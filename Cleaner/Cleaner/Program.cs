﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cleaner
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CleanAsync().Wait();
                Console.WriteLine("\nCleaned Sucessfully !");
                Thread.Sleep(1000);                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        private static async Task CleanAsync()
        {
            Console.WriteLine("Restarting IIS and SQL Server...");
            await ServicesCleaner.RestartServicesAsync();
            Console.WriteLine("Restarted IIS and SQL Server Sucessfully !");
            Console.WriteLine("\nCleaning Temp Directories and Log Files...");
            await DirectoryCleaner.CleanDirectoriesAsync();
            Console.WriteLine("Cleaned Temp Directories and Log Files Sucessfully !");
        }
    }
}
