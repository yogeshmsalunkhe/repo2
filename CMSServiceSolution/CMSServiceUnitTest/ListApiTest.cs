﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

//IM namespaces
using IM.CMS.CommonContracts;
using IM.CMS.CommonContracts.Constants;

namespace IngramMicro.CMSService.UnitTest
{
    [TestClass()]
    public class ListApiTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        /// Fetch all custom promotion data.
        /// </summary>
        [TestMethod()]        
        public void GetCustomPromotionSimple()
        {
            var listRequest = new CustomPromotionRequest();
            listRequest.SiteCode = "es";
            listRequest.UserSelectedCulture = "en-us";

            IM.CMSService.ServiceImplementations.CMSService CMSService = new IM.CMSService.ServiceImplementations.CMSService();
            var customPromotions = CMSService.GetCustomPromotion(listRequest);
            foreach (CustomPromotionDTO customPromotion in customPromotions.CustomPromotions)
            {
                string title = customPromotion.Title;
                string name = customPromotion.Name;
            }
        }

        /// <summary>
        /// Fetch with required columns.Fetch custom promotion data of only Title column.
        /// </summary>
        [TestMethod()]
        public void GetCustomPromotionWithRequiredFields()
        {
            var listRequest = new CustomPromotionRequest();
            listRequest.SiteCode = "es";
            listRequest.UserSelectedCulture = "en-us";
            
            listRequest.RequiredFields = new List<string> { "Title" };

            IM.CMSService.ServiceImplementations.CMSService CMSService = new IM.CMSService.ServiceImplementations.CMSService();
            var customPromotions = CMSService.GetCustomPromotion(listRequest);
            foreach (CustomPromotionDTO customPromotion in customPromotions.CustomPromotions)
            {
                string title = customPromotion.Title;
                string name = customPromotion.Name;
            }            
        }

        /// <summary>
        /// Fetch with criteria.Fetch custom promotion data that matches criteria.
        /// </summary>
        [TestMethod()]
        public void GetCustomPromotionWithCriteria()
        {
            var listRequest = new CustomPromotionRequest();
            listRequest.SiteCode = "es";
            listRequest.UserSelectedCulture = "en-us";

            List<Criterion> criteria = new List<Criterion>();
            Criterion criterion = new Criterion();
            criterion.FieldName = "MenuItemVisibility";
            criterion.Operator = Operator.Equals;
            criterion.FieldValue = "false";
            criteria.Add(criterion);

            Criterion criterion2 = new Criterion();
            criterion2.FieldName = "Title";
            criterion2.Operator = Operator.Like;
            criterion2.FieldValue = "%products%";
            criterion2.Combiner = Combiner.Or;
            criteria.Add(criterion2);

            listRequest.Criteria = criteria;

            IM.CMSService.ServiceImplementations.CMSService CMSService = new IM.CMSService.ServiceImplementations.CMSService();
            var customPromotions = CMSService.GetCustomPromotion(listRequest);
            foreach (CustomPromotionDTO customPromotion in customPromotions.CustomPromotions)
            {
                string title = customPromotion.Title;
                string name = customPromotion.Name;
            }
        }

        /// <summary>
        /// Fetch all custom promotion data.
        /// </summary>
        [TestMethod()]
        public void GetMarketingUrlsSimple()
        {
            var listRequest = new MarketingUrlRequest();
            listRequest.SiteCode = "es";
            listRequest.UserSelectedCulture = "en-US";

            IM.CMSService.ServiceImplementations.CMSService CMSService = new IM.CMSService.ServiceImplementations.CMSService();
            var marketingUrl = CMSService.GetMarketingUrl(listRequest);
            
            Assert.IsTrue(marketingUrl.Acknowledge == AcknowledgeType.Success);

        }

        /// <summary>
        /// Fetch all custom promotion data.
        /// </summary>
        [TestMethod()]
        public void GetCustomPromotionWithAttachmentSimple()
        {
            var listRequest = new CustomPromotionRequest();
            listRequest.SiteCode = "es";
            listRequest.IgnoreCulture = true;            

            IM.CMSService.ServiceImplementations.CMSService CMSService = new IM.CMSService.ServiceImplementations.CMSService();
            var customPromotions = CMSService.GetCustomPromotion(listRequest);

            Assert.IsTrue(customPromotions.Acknowledge == AcknowledgeType.Success);

            Assert.IsTrue(customPromotions.CustomPromotions.Cast<CustomPromotionDTO>().Any(customPromotion => customPromotion.Attachment != null));

        }
    }
}
