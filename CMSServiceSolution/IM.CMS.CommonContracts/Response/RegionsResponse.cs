﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class RegionsResponse : ResponseBase
    {
        [DataMember]
        public IList<DisplayRegionDTO> DisplayRegions;

        [DataMember]
        public string CMSContactId;

        [DataMember]
        public string CMSBaseUrl;
    }
}


