﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class WebExclusivePromotionResponse : ResponseBase
    {
        [DataMember]
        public IEnumerable<WebExclusivePromotionsDTO> WebExclusivePromotions;
    }
}
