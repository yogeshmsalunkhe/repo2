﻿using System;
using System.Data;
using System.Reflection;
using System.Configuration;
using System.Runtime.Serialization;
using IM.CMS.CommonContracts.Constants;

namespace IM.CMS.CommonContracts
{
        /// <summary>
        /// Base class for all response messages to clients of the web service. It standardizes 
        /// communication between web services and clients with a series of common values and 
        /// their initial defaults. Derived response message classes can override the default 
        /// values if necessary.
        /// </summary>
        [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
        public class ResponseBase
        {
            /// <summary>
            /// A flag indicating success or failure of the web service response back to the 
            /// client. Default is false
            /// </summary>
            [DataMember]
            public AcknowledgeType Acknowledge = AcknowledgeType.Failure;

            /// <summary>
            /// CorrelationId mostly returns the RequestId back to client. 
            /// </summary>
            [DataMember]
            public string CorrelationId;

            /// <summary>
            /// Message back to client. Mostly used when a web service failure occurs. 
            /// </summary>
            [DataMember]
            public string Message;

            /// <summary>
            /// Version number (in major.minor format) of currently executing web service. 
            /// Used to offer a level of understanding (related to compatibility issues) between
            /// the client and the web service as the web services evolve over time. 
            /// Ebay.com uses this in their API.
            /// </summary>
            [DataMember]
            //public string Version =
                //Assembly.GetExecutingAssembly().GetName().Version.Major + "." +
                //Assembly.GetExecutingAssembly().GetName().Version.Minor;
            public string Version;
            /// <summary>
            /// Build number of currently executing web service. Used as an indicator
            /// to client whether certain code fixes are included or not.
            /// Ebay.com uses this in their API.
            /// </summary>
            [DataMember]
            //public string Build =
                //Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();
            public string Build; 

        }
}
