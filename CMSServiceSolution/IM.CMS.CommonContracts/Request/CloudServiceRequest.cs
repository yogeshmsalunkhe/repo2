﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{

    /// <summary>
    /// To Fetch Cloud Services Data
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class CloudServiceRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.CloudServices; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    CloudServicesID, 
                    ServiceName ,
                    Vendor, 
                    ShortDescription, 
                    Category,
                    SubCategory, 
                    ServiceType,
                    LargeLogo, 
                    SmallLogo, 
                    Technology, 
                    Architecture, 
                    WebVisible, 
                    IsShowMoreTemplate, 
                    ShowMoreURL, 
                    DocumentCreatedWhen,
                    LongDescription,
                    TechnicalSpecification,
                    PricingContent
                };
            }
        }
        public string CloudServicesID { get { return "CloudServicesID"; } }
        public string ServiceName { get { return "ServiceName"; } }
        public string Vendor { get { return "Vendor"; } }
        public string ShortDescription { get { return "ShortDescription"; } }
        public string Category { get { return "Category"; } }
        public string SubCategory { get { return "SubCategory"; } }
        public string ServiceType { get { return "ServiceType"; } }
        public string LargeLogo { get { return "LargeLogo"; } }
        public string SmallLogo { get { return "SmallLogo"; } }
        public string Technology { get { return "Technology"; } }
        public string Architecture { get { return "Architecture"; } }
        public string WebVisible { get { return "WebVisible"; } }
        public string IsShowMoreTemplate { get { return "IsShowMoreTemplate"; } }
        public string ShowMoreURL { get { return "ShowMoreURL"; } }
        public string DocumentCreatedWhen { get { return "DocumentCreatedWhen"; } }
        public string LongDescription { get { return "LongDescription"; } }
        public string TechnicalSpecification { get { return "TechnicalSpecification"; } }
        public string PricingContent { get { return "PricingContent"; } }
    }
}
