﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class CreateUserRequest : RequestBase
    {
        [DataMember]
        public string UserName;

        [DataMember]
        public string FullName;

        [DataMember]
        public Guid UserGUID;

        [DataMember]
        public bool UserEnabled;
    }
}
