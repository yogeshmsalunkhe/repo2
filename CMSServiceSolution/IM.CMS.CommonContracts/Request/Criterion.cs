﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

//IM namespaces
using IM.CMS.CommonContracts.Constants;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used to specify where condition to fetch list data. 
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class Criterion
    {
        [DataMember]
        public string FieldName;

        [DataMember]
        public Operator Operator;

        [DataMember]
        public string FieldValue;

        [DataMember]
        public Combiner Combiner;
    }
}
