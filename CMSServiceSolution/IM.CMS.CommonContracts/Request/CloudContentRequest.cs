﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used to fetch Cloud Contend ata.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class CloudContentRequest:ListRequest
    {
        public override string ListName { get { return Constants.Constants.CloudContent; } }
        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Title,
                    Publisher,
                    Topic,
                    Description,
                    TypeOfFile,
                    FileFromMedia,
                    DocumentExternalURL,
                    Page,
                    LogoURL,
                    ContentType,
                    Technology,
                    Architecture,
                    DateCreated
                };
            }
        }

        public string Title { get { return "Title"; } }

        public string Publisher { get { return "Publisher"; } }

        public string Topic { get { return "Topic"; } }

        public string Description { get { return "Description"; } }

        public string TypeOfFile { get { return "TypeOfFile"; } }

        public string FileFromMedia { get { return "FileFromMedia"; } }

        public string DocumentExternalURL { get { return "DocumentExternalURL"; } }

        public string Page { get { return "Page"; } }

        public string LogoURL { get { return "LogoURL"; } }

        public string ContentType { get { return "ContentType"; } }

        public string Technology { get { return "Technology"; } }

        public string Architecture { get { return "Architecture"; } }

        public string DateCreated { get { return "DocumentCreatedWhen"; } }
    }
}
