﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used to fetch Direct Ship data.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class DirectShipContentRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.DirectShipContent; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Text,
                    Message,
                    VendorCode              
                };
            }
        }

        public string Text { get { return "Name"; } }
        public string Message { get { return "Message"; } }
        public string VendorCode { get { return "VendorCode"; } }
    }
}
