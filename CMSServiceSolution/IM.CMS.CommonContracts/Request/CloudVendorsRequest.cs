﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used to fetch Direct Ship data.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class CloudVendorsRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.CloudVendors; } }

       
        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Vendor, 
                    CompanyDetails
                };
            }
        }

        public string Vendor { get { return "CompanyName"; } }
        public string CompanyDetails { get { return "CompanyDetails"; } }
    }
}
