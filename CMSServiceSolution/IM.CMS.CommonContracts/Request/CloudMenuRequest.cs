﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used to fetch Cloud Menu data.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class CloudMenuRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.CloudMenu; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Title,
                    URL,
                    OpenURL
                                
                };
            }
        }

        public string Title { get { return "Title"; } }
        public string URL { get { return "URL"; } }
        public string OpenURL { get { return "OpenURL"; } }
        
    }





}
