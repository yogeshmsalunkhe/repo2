﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class UploadQuotePreferenceLogoRequest : RequestBase
    {
        [DataMember]
        public string Des_File_path;

        [DataMember]
        public string filename;

        [DataMember]
        public string fileurl;
    }
}
