﻿using System.Runtime.Serialization;
using System.Collections.Generic;
namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class ContactGroupRequest : RequestBase
    {
        [DataMember]
        public string ContactGUID;

        [DataMember]
        public string ContactGroups;
    }
}