﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    public class UpdateContactGroupResellersRequest : RequestBase
    {
        public string GroupName;

        public List<string> Resellers;
    }
}
