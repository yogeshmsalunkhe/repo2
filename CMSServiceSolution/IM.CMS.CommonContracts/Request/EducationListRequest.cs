﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{    
    /// <summary>
    /// Used to fetch Custom Table data.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class EducationListRequest : ListRequest
    {
        public override string ListName { get { return CustomListName; } }
        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Name             
                };
            }
        }

        public string Name { get { return "Name"; } }

        public string CustomListName { get; set; }
    }
}
