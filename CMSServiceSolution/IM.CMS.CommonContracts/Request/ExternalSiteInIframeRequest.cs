﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{    
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class ExternalSiteInIframeRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.ExternalSiteInIframe ; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    IMSiteName,
                    ExternalSiteURL,
                    AddEncryptedIDtoURL,
                    MenuItemVisibility,
                    DisplayedIn,
                    PunchoutSystemName,
                    PunchbackURL
                };
            }
        }
       
        public string IMSiteName { get { return "IMSiteName"; } }
        public string ExternalSiteURL { get { return "ExternalSiteURL"; } }
        public string AddEncryptedIDtoURL { get { return "AddEncryptedIDtoURL"; } }
        public string MenuItemVisibility { get { return "MenuItemVisibility"; } }
        public string DisplayedIn { get { return "DisplayedIn"; } }
        public string PunchoutSystemName { get { return "PunchoutSystemName"; } }
        public string PunchbackURL { get { return "PunchbackURL"; } }
    }
}