﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class BoutiqueRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.Boutiques; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Name,
//                    Label,
                    Url,
                    MenuItemVisibility,
                    NodeAliasPath,
                    FeaturedImage,
                    FeaturedImageToolTip
                };
            }
        }

        public string Name { get { return "Name"; } }
        public string Label { get { return "Label"; } }
        public string Url { get { return "Url"; } }
        public string MenuItemVisibility { get { return "MenuItemVisibility"; } }
        public string NodeAliasPath { get { return "NodeAliasPath"; } } // so we know if it's a product or vendor boutique
        public string FeaturedImage { get { return "FeaturedImage"; } }
        public string FeaturedImageToolTip { get { return "TextFeaturedImage"; } }

    }
}
