﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class SavedEventRequest : ListRequest
    {
        public override string ListName
        {
            get { return Constants.Constants.SavedEvent; }
        }
        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    EventsID,
                    EventName,
                    EventPublic,
                    EventLocation,
                    EventSponsor,
                    EventDateBegin,
                    EventDateEnd,
                    EventLinkURL,
                    NodeAlias
                };
            }
        }

        public string EventsID { get { return "EventsID"; }  }
        public string EventName { get { return "EventName"; } }
        public string EventPublic { get { return "EventPublic"; } }
        public string EventLocation { get { return "EventLocation"; } }
        public string EventSponsor { get { return "EventSponsor"; } }
        public string EventDateBegin { get { return "EventDateBegin"; } }
        public string EventDateEnd { get { return "EventDateEnd"; } }
        public string EventLinkURL { get { return "EventLinkURL"; } }
        public string NodeAlias { get { return "NodeAlias"; } }

    }
}
