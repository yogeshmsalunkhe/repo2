﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class SocialMediaRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.SocialMedia; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Name,
                    Url,
                };
            }
        }

        public string Name { get { return "SocialMediaService"; } }
        public string Url { get { return "Url"; } }
    }
}
