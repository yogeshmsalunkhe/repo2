﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{    
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class ReusableContentRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.ReusableContent ; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Title,
                    Content,
                    Comments,
                    LastModified
                };
            }
        }
       
        public string Title { get { return "Title"; } }
        public string Content { get { return "Content"; } }
        public string Comments { get { return "Comments"; } }
        public string LastModified { get { return "DocumentModifiedWhen"; } } 
    }
}