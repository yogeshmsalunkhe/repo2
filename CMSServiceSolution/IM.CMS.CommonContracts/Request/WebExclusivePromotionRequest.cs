﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class WebExclusivePromotionRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.TopMenuLink; } }

        /// <summary>
        /// Selection criteria 
        /// </summary>
        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    
                };
            }
        }    

        [DataMember]
        public bool IsUserLoggedIn;
    }
}
