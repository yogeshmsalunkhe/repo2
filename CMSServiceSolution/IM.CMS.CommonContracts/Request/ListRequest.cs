﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Base class for all particular list request class.
    /// All particular list request class must inherit this class and implement abstract members.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public abstract class ListRequest : RequestBase
    {
        // List name to be fetched from storage. For internal use only.
        public abstract string ListName { get; }

        // Returns list of all field names of particular list. This will be used when property RequiredFields returns empty.
        // For internal use only.
        public abstract List<string> AllFields { get; }        

        // Culture which should be considered while fetching requested list data. For e.g. en-US        
        [DataMember]
        public string UserSelectedCulture;

        // Contains list of field names to be considered while fetching requested list data.
        [DataMember]
        public List<string> RequiredFields;

        // Contains criteria/condition to be considered while fetching requested list data.
        [DataMember]
        public List<Criterion> Criteria;

        // Ignore User Selected Culture value and return all Cultures
        [DataMember] 
        public bool IgnoreCulture;        
    }
}