﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// To Fetch Marketing Url Data
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class MarketingUrlRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.MarketingUrls; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    SearchBreadCrumbName,
                    CommaSeperatedListOfSkus,
                    Comments,
                    Active,
                    DocumentCreatedWhen,
                    DocumentModifiedWhen,
                    NodeOwnerUserName
                };
            }
        }
        
        public string SearchBreadCrumbName { get { return "SearchBreadCrumbName"; } }
        public string CommaSeperatedListOfSkus { get { return "CommaSeperatedListOfSkus"; } }
        public string Comments { get { return "Comments"; } }
        public string Active { get { return "Active"; } }
        public string DocumentCreatedWhen { get { return "DocumentCreatedWhen"; } }
        public string DocumentModifiedWhen { get { return "DocumentModifiedWhen"; } }
        public string NodeOwnerUserName { get { return "NodeOwnerUserName"; } }        
    }
}
