﻿using System.Runtime.Serialization;

namespace IM.CMS.CommonContracts
{
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class RegionRequest : RequestBase
    {
        /// <summary>
        /// Selection criteria 
        /// </summary>
        [DataMember]
        public string KenticoCurrentContactGuid;

        [DataMember]
        public string BrowserUserAgent;

        [DataMember]
        public bool IsSecureConnection;

        [DataMember]
        public string UserSelectedCulture;

        [DataMember]
        public string HostPageName;

        [DataMember]
        public string SAMAccountName;        

        [DataMember]
        public string ParamURL;

        [DataMember]
        public bool UseLoggedInOut;
    }
}
