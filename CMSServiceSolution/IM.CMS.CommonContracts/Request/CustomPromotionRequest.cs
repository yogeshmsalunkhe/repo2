﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used to fetch custom promotion data.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public class CustomPromotionRequest : ListRequest
    {
        public override string ListName { get { return Constants.Constants.CustomPromotion; } }

        public override List<string> AllFields
        {
            get
            {
                return new List<string>{
                    Title,
                    DocumentName,
                    Url,
                    PromotionType,
                    Description,
                    SmallIconPath,
                    LargeIconPath,
                    MenuItemVisibility,                    
                    DocumentPublishFrom,
                    DocumentPublishTo,
                    ContactGroup,
                    IncludeInSearchFacet,
                    OpenUrlIn,
                    ValueUnit
                };
            }
        }

        public string Title { get { return "Title"; } }
        public string DocumentName { get { return "DocumentName"; } }
        public string Url { get { return "Url"; } }
        public string PromotionType { get { return "PromotionType"; } }
        public string Description { get { return "Description"; } }
        public string SmallIconPath { get { return "SmallIconPath"; } }
        public string LargeIconPath { get { return "LargeIconPath"; } }
        public string MenuItemVisibility { get { return "MenuItemVisibility"; } }
        public string DocumentPublishFrom { get { return "DocumentPublishFrom"; } }
        public string DocumentPublishTo { get { return "DocumentPublishTo"; } }
        public string DocumentID { get { return "DocumentID"; } }
        public string AttachmentPromotion { get { return "Attachment"; } }
        public string Attachment { get { return "AttachmentBinary"; } }
        public string AttachmentName { get { return "AttachmentName"; } }
        public string OpenUrlIn { get { return "OpenUrlIn"; } }
        public string ContactGroup { get { return "ContactGroup"; } }
        public string IncludeInSearchFacet { get { return "IncludeInSearchFacet"; } }
        public string ValueUnit { get { return "ValueUnit"; } }

        [DataMember]
        public bool FetchResellerIds;
    }
}