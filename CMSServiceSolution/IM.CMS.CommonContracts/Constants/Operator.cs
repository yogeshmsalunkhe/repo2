﻿using System;
using System.Data;
using System.Configuration;
using System.Runtime.Serialization;

namespace IM.CMS.CommonContracts.Constants
{
    /// <summary>
    /// Used in Criteria class to define condition.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public enum Operator
    {
        [EnumMember]
        Equals = 0,

        [EnumMember]
        NotEquals = 1,

        [EnumMember]
        GreaterThan = 2,

        [EnumMember]
        GreaterThanOrEquals = 3,

        [EnumMember]
        LessThan = 4,

        [EnumMember]
        LessThanOrEquals = 5,

        [EnumMember]
        Like = 6,

        [EnumMember]
        NotLike = 7,

        [EnumMember]
        IN = 8
    }
}
