﻿using System;
using System.Data;
using System.Configuration;
using System.Runtime.Serialization;

namespace IM.CMS.CommonContracts.Constants
{
    /// <summary>
    /// Used in Criteria class for joining conditions by And or Or.
    /// </summary>
    [DataContract(Namespace = "http://ingrammicro.com/cms/types/")]
    public enum Combiner
    {
        [EnumMember]
        And = 0,

        [EnumMember]
        Or = 1
    }
}
