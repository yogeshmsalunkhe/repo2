﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts.Constants
{
    public static class Constants
    {
        #region Kentico document constants        
        //Document names        
        public const string CustomPromotion = "CustomPromotionDocument";
        public const string ReusableContent = "ReusableContentDocument";
        public const string ExternalSiteInIframe = "ExternalSiteInIframeDocument";
        public const string SavedEvent = "Events";
        public const string MarketingUrls = "MarketingUrlDocument";
        public const string Boutiques = "BoutiqueDocument";
        public const string SocialMedia = "SocialMediaDocument";
        public const string TopMenuLink = "TopMenuLinkDocument";
        public const string DirectShipContent = "DirectShipContent";
        public const string CloudServices = "CloudServices";
        public const string CloudContent = "CloudContent";
        public const string CloudMenu = "CloudMenu";
        public const string CloudVendors = "CloudVendors";       
 
        #endregion

        #region Vendor Configurable Link constants
        //Document names        
        public const string VendorConfigurableLinks = "VendorConfigurableLinksDocument"; 

        #endregion
      
    }
}
