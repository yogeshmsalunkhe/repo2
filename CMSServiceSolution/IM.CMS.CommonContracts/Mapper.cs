﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IM.CMS.CommonContracts
{
    public static class Mapper
    {
        public static IList<DisplayRegionDTO> ToDataTransferObjects(IEnumerable<DisplayRegionDAO> regions)
        {
            if (regions == null) return null;
            return regions.Select(i=> ToDataTransferObject(i)).ToList();
        }

        public static DisplayRegionDTO ToDataTransferObject(DisplayRegionDAO region)
        {
            if (region == null) return null;
            return new DisplayRegionDTO
            { 
                   TagId=region.TagId,
                   Html=region.Html,
                   MetaContent = region.MetaContent,
                   MetaName = region.MetaName
            };
        }
    }
}