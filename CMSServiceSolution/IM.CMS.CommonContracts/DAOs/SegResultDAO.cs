﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    public class SegResultDAO
    {
        public string ContactId { get; set; }
        public IList<DisplayRegionDAO>  Regions { get; set; }
        public string CMSBaseUrl { get; set; }
        public SegResultDAO()
        { 
            //defaults
            ContactId = string.Empty;
            Regions=new List<DisplayRegionDAO>();
        }
    }
}
