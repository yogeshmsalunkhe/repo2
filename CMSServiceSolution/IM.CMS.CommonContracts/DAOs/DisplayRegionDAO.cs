﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    public class DisplayRegionDAO
    {
        public string TagId { get; set; }
        public string Html { get; set; }
        public string MetaName { get; set; }
        public string MetaContent { get; set; }
    }
}
