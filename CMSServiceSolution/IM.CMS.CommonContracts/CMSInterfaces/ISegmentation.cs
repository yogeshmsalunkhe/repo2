﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts.CMSInterfaces
{
    public interface ISegmentationDAO
    {
        SegResultDAO GetDisplayRegions(RegionRequest request);
        void BannerClick(string bannerClickUrl);
    }
}
