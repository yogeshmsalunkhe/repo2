﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IM.CMS.CommonContracts;

namespace IM.CMS.CommonContracts.ServiceInterfaces
{
    [ServiceContract(Namespace = "http://ingrammicro.com")]
    public interface ICMSService
    {
        [OperationContract]
        //// call cms to retrieve html of regions of a particular page 
        RegionsResponse GetDisplayRegions(RegionRequest request);

        [OperationContract]
        //// forward a Banner Click request to Kentico
        void BannerClick(string bannerClickUrl);

        // Get custom promotion data.
        [OperationContract]
        CustomPromotionResponse GetCustomPromotion(CustomPromotionRequest request);

        // Get reusable contents.
        [OperationContract]
        ReusableContentResponse GetReusableContent(ReusableContentRequest request);

        // Get ExternalSiteInIframe list data.
        [OperationContract]
        ExternalSiteInIframeResponse GetExternalSiteInIframe(ExternalSiteInIframeRequest request);

        // Get marketing URLs
        [OperationContract]
        MarketingUrlResponse GetMarketingUrl(MarketingUrlRequest request);

        // Get marketing Boutiques
        [OperationContract]
        BoutiqueResponse GetBoutiques(BoutiqueRequest request);

        // Get marketing Boutiques
        [OperationContract]
        SocialMediaResponse GetSocialMedia(SocialMediaRequest request);

        // Get Banner Redirect Url
        [OperationContract]
        string GetBannerRedirectUrl(BannerUrlRequest request);

        // Get Banner Url
        [OperationContract]
        string GetBannerUrl(BannerUrlRequest request);

        // GetWebExclusivePromotions
        [OperationContract]
        WebExclusivePromotionResponse GetWebExclusivePromotions(WebExclusivePromotionRequest request);

        // Create User in Kentico
        [OperationContract]
        void CreateUserRequest(CreateUserRequest request);

        [OperationContract]
        bool CheckContactGroupContainsContact(ContactGroupRequest request);

        //Get Direct Ship Content List
        [OperationContract]
        DirectShipContentResponse GetDirectShipContent(DirectShipContentRequest request);

        //Get Education Content List[Ex- Architecture,Technology]
        [OperationContract]
        EducationListResponse GetEducationContentList(EducationListRequest request);

        //Get Cloud Content
        [OperationContract]
        CloudContentResponse GetCloudContent(CloudContentRequest request);

        //Get Direct Ship Content List
        [OperationContract]
        CloudServicesResponse GetCloudServices(CloudServiceRequest request);

        //Get Cloud Menu List
        [OperationContract]
        CloudMenuResponse GetCloudMenu(CloudMenuRequest request);

        [OperationContract]
        CloudVendorsResponse GetCloudVendors(CloudVendorsRequest request);

        [OperationContract]
        ResponseBase UpdateCustomPromotion(CustomPromotionRequest request, CustomPromotionDTO data, bool isUpdateAttachmentOnly);

        [OperationContract]
        ResponseBase UpdateContactGroupResellers(UpdateContactGroupResellersRequest request);

        [OperationContract]
        ResponseBase UploadQuotePreferenceLogo(UploadQuotePreferenceLogoRequest request);

        [OperationContract]
        ResponseBase DeleteQuotePreferenceLogo(UploadQuotePreferenceLogoRequest request);

        [OperationContract]
        SavedEventResponse GetSavedEvents(SavedEventRequest request);

        [OperationContract]
        VendorConfigurableLinksResponse GetVendorConfigurableLinks(VendorConfigurableLinksRequest request);

    }
}
