﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Data;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used as a response WebExclusivePromotionsDTO data.
    /// </summary>
    [DataContract(Name = "WebExclusivePromotion", Namespace = "http://ingrammicro.com/cms/types/")]
    public class WebExclusivePromotionsDTO
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string OpenURLIn { get; set; }

        [DataMember]
        public List<WebExclusivePromotionsDTO> MenuItems { get; set; }

        [DataMember]
        public int MenuItemVisibility { get; set; }     
    }
}