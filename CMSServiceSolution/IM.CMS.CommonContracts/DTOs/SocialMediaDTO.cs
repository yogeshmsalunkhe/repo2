﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Name = "SocialMedia", Namespace = "http://ingrammicro.com/cms/types/")]
    public class SocialMediaDTO
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

    }
}
