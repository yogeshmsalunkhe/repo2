﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Data;

namespace IM.CMS.CommonContracts
{
    [DataContract(Name = "ExternalSiteInIframe", Namespace = "http://ingrammicro.com/cms/types/")]
    public class ExternalSiteInIframeDTO
    {
        [DataMember]
        public string IMSiteName { get; set; }

        [DataMember]
        public string ExternalSiteURL { get; set; }

        [DataMember]
        public bool AddEncryptedIDtoURL { get; set; }

        [DataMember]
        public int MenuItemVisibility { get; set; }

        [DataMember]
        public string DisplayedIn { get; set; }

        [DataMember]
        public string PunchoutSystemName { get; set; }

        [DataMember]
        public string PunchbackURL { get; set; }   
    }
}
