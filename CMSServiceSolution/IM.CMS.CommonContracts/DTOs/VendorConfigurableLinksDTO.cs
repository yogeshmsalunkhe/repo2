﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Data;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used as a response VendorConfigurableLinksDTO data.
    /// </summary>
    [DataContract(Name = "VendorConfigurableLinks", Namespace = "http://ingrammicro.com/cms/types/")]
    public class VendorConfigurableLinksDTO
    {
        [DataMember]
        public string VendorName { get; set; }

        [DataMember]
        public string ContentName { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public string ThumbnailUrl { get; set; }       

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public int Priority { get; set; }
       
        public DateTime DocumentLastPublished { get; set; }  
    }
}