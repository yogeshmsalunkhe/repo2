﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace IM.CMS.CommonContracts
{
    [DataContract(Name="DisplayRegion", Namespace = "http://ingrammicro.com/cms/types/")]
    public class DisplayRegionDTO
    {
        [DataMember]
       public string TagId { get; set; }

        [DataMember]
       public string Html { get; set; }

        [DataMember]
        public string MetaName { get; set; }

        [DataMember]
        public string MetaContent { get; set; }
    }
}
