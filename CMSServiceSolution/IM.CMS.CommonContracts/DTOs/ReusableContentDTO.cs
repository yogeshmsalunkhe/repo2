﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Data;

namespace IM.CMS.CommonContracts
{    
    [DataContract(Name = "ReusableContent", Namespace = "http://ingrammicro.com/cms/types/")]
    public class ReusableContentDTO
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public DateTime LastModified { get; set; }  
    }
}
