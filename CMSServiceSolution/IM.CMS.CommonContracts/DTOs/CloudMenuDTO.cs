﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used as a response Cloud Menu data.
    /// </summary>
    [DataContract(Name = "CloudMenu", Namespace = "http://ingrammicro.com/cms/types/")]
   public class CloudMenuDTO
   {
       [DataMember]
        public string _columnNameCloudMenuTitle { get; set; }

       [DataMember]
       public string _columnNameCloudMenuUrl { get; set; }

       [DataMember]
       public string DisplayedIn { get; set; }

      

       

   }
}
