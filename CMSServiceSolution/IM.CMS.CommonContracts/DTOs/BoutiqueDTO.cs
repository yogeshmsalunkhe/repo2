﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Name = "Boutique", Namespace = "http://ingrammicro.com/cms/types/")]
    public class BoutiqueDTO
    {
        [DataMember]
        public string Name { get; set; }

//        [DataMember]
//        public string Label { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public bool IsPrivate { get; set; }
        [DataMember]
        public string FeaturedImage { get; set; }
        [DataMember]
        public string FeaturedImageToolTip { get; set; }

    }
}
