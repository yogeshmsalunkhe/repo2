﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used as a response Cloud Content data.
    /// </summary>
    [DataContract(Name = "CloudContent", Namespace = "http://ingrammicro.com/cms/types/")]
    public class CloudContentDTO
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Publisher { get; set; }
        [DataMember]
        public string Topic { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string TypeOfFile { get; set; }
        [DataMember]
        public string FileFromMedia { get; set; }
        [DataMember]
        public string DocumentExternalURL { get; set; }
        [DataMember]
        public string Page { get; set; }
        [DataMember]
        public string LogoURL { get; set; }
        [DataMember]
        public string ContentType { get; set; }
        [DataMember]
        public string Technology { get; set; }
        [DataMember]
        public string Architecture { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
    }
}
