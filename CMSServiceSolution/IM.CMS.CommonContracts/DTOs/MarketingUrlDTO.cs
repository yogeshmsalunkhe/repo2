﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used as a response custom promotion data.
    /// </summary>
    [DataContract(Name = "MarketingUrl", Namespace = "http://ingrammicro.com/cms/types/")]
    public class MarketingUrlDTO
    {
        [DataMember]
        public string SearchBreadCrumbName { get; set; }

        [DataMember]
        public string CommaSeperatedListOfSkus { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public bool Active { get; set; }

        [DataMember]
        public string NodeOwnerUserName { get; set; }

        [DataMember]
        public DateTime DocumentCreatedWhen { get; set; }

        [DataMember]
        public DateTime DocumentModifiedWhen { get; set; }       
    }
}
