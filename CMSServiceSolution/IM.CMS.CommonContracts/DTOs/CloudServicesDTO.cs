﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used as a response Cloud Services data.
    /// </summary>
    [DataContract(Name = "CloudService", Namespace = "http://ingrammicro.com/cms/types/")]
    public class CloudServicesDTO
    {

        [DataMember]
        public string CloudServicesID { get; set; }

        [DataMember]
        public string VPN { get; set; }

        [DataMember]
        public string ServiceID { get; set; }

        [DataMember]
        public string ServiceName { get; set; }

        [DataMember]
        public string Vendor { get; set; }

        [DataMember]
        public string ShortDescription { get; set; }

         [DataMember]
        public string Category { get; set; }

         [DataMember]
        public string SubCategory { get; set; }

         [DataMember]
        public string ServiceType { get; set; }

        [DataMember]
        public string LargeLogo { get; set; }

        [DataMember]
        public string SmallLogo { get; set; }

        [DataMember]
        public string Technology { get; set; }

        [DataMember]
        public string Architecture { get; set; }

        [DataMember]
        public bool WebVisible { get; set; }

         [DataMember]
        public bool IsShowMoreTemplate { get; set; }

         [DataMember]
        public string ShowMoreURL { get; set; }

        [DataMember]
        public DateTime DocumentCreatedWhen { get; set; }

        [DataMember]
        public string LongDescription { get; set; }

        [DataMember]
        public string TechnicalSpecification { get; set; }

        [DataMember]
        public string PricingContent { get; set; }

    }
}



//Original services table


	//[SalesOrganization] [nvarchar](50) NULL,
	//[Division] [nvarchar](50) NULL,
	//[DistributionChannel] [nvarchar](50) NULL,
	
