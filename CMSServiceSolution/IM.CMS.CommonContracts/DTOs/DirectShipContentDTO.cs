﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used as a response Direct Ship Content data.
    /// </summary>
    [DataContract(Name = "DirectShipContent", Namespace = "http://ingrammicro.com/cms/types/")]
    public class DirectShipContentDTO
    {
        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string VendorCode { get; set; }

    }
}
