﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.CommonContracts
{
    [DataContract(Name = "SavedEvent", Namespace = "http://ingrammicro.com/cms/types/")]
    public class SavedEventDTO
    {
        
        [DataMember]
        public string EventName { get; set; }
        [DataMember]
        public string EventSponsor { get; set; }
        [DataMember]
        public string EventLocation { get; set; }
        [DataMember]
        public bool EventPublic { get; set; }
        [DataMember]
        public int EventID { get; set; }

        [DataMember]
        public DateTime EventDateBegin { get; set; }

        [DataMember]
        public DateTime EventDateEnd { get; set; }

        [DataMember]
        public string EventLinkURL { get; set; }

        [DataMember]
        public string NodeAlias { get; set; }
       
    }
}
