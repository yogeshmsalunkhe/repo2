﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Data;

namespace IM.CMS.CommonContracts
{
    /// <summary>
    /// Used as a response custom promotion data.
    /// </summary>
    [DataContract(Name = "CustomPromotion", Namespace = "http://ingrammicro.com/cms/types/")]
    public class CustomPromotionDTO
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string PromotionType { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string SmallIconPath { get; set; }

        [DataMember]
        public string LargeIconPath { get; set; }

        [DataMember]
        public int MenuItemVisibility { get; set; }

        [DataMember]
        public DateTime DocumentPublishFrom { get; set; }

        [DataMember]
        public DateTime DocumentPublishTo { get; set; }

        [DataMember]
        public byte[] Attachment { get; set; }

        [DataMember]
        public string AttachmentName { get; set; }

        [DataMember]
        public int ContactGroup { get; set; }

        [DataMember]
        public string[] ResellerIDs { get; set; }

        [DataMember]
        public bool IncludeInSearchFacet { get; set; }

        [DataMember]
        public string OpenUrlIn { get; set; } 

        [DataMember]
        public string ValueUnit { get; set; } 
        
    }
}