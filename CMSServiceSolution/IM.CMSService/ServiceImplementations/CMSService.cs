﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

using Microsoft.Practices.EnterpriseLibrary.Logging;

using IM.ErrorLoggingUtilities;
using IM.CMS.CommonContracts;
using IM.CMS.CommonContracts.Constants;
using IM.CMS.CommonContracts.CMSInterfaces;
using IM.CMS.CommonContracts.ServiceInterfaces;
using IM.CMS.DataObjects.Kentico;
using IM.CMS.Rules.Kentico;


namespace IM.CMSService.ServiceImplementations
{
    [ServiceBehavior(Namespace = "http://ingrammicro.com", InstanceContextMode = InstanceContextMode.PerCall)]
    public class CMSService : ICMSService
    {
        // kentico implemenations of segmentation functionality via this dao.-- use IOC pattern for new non kentico CMS implementations
        protected ISegmentationDAO segmentationDAO = new IM.CMS.DataObjects.Kentico.KenticoSegDao();
        protected RulesController Rules = new IM.CMS.Rules.Kentico.RulesController();

        // call cms to retrieve html of stuctured data or particular control page, etc.
        public RegionsResponse GetDisplayRegions(RegionRequest request)
        {
            RegionsResponse response = new RegionsResponse();
            string validation = Rules.ValidateRegionRequest(request);
            try
            {
                if (validation == RuleMessages.ValidationPassed)
                {
                    SegResultDAO result = segmentationDAO.GetDisplayRegions(request);
                    response.CMSBaseUrl = result.CMSBaseUrl;
                    response.CMSContactId = result.ContactId;
                    response.DisplayRegions = IM.CMS.CommonContracts.Mapper.ToDataTransferObjects(result.Regions);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (System.Net.WebException we)
            {
                System.Net.HttpWebResponse wr = (System.Net.HttpWebResponse)we.Response;
                if (wr.StatusCode.Equals(System.Net.HttpStatusCode.NotFound))
                {
                    response.Acknowledge = AcknowledgeType.Success;
                    //                    response.Message = "CMSService Http error: " + wr.StatusDescription;
                    //                    ErrorLogging.HandleException(we, Constants.ExceptionPolicyName);
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        /// <summary>
        /// Forwards a Banner Click event to Kentico to count the click
        /// </summary>
        /// <param name="bannerClickUrl"></param>
        public void BannerClick(string bannerClickUrl)
        {
            segmentationDAO.BannerClick(bannerClickUrl);
        }

        public CustomPromotionResponse GetCustomPromotion(CustomPromotionRequest request)
        {
            var response = new CustomPromotionResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.CustomPromotions = listManager.GetCustomPromotion(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        public ReusableContentResponse GetReusableContent(ReusableContentRequest request)
        {
            var response = new ReusableContentResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.ReusableContents = listManager.GetReusableContent(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        public ExternalSiteInIframeResponse GetExternalSiteInIframe(ExternalSiteInIframeRequest request)
        {
            var response = new ExternalSiteInIframeResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.ExternalSiteInIframes = listManager.GetExternalSiteInIframe(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        public MarketingUrlResponse GetMarketingUrl(MarketingUrlRequest request)
        {
            var response = new MarketingUrlResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.MarketingUrls = listManager.GetMarketingUrl(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        public CloudServicesResponse GetCloudServices(CloudServiceRequest request)
        {
            var response = new CloudServicesResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.CloudServices = listManager.GetCloudServices(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }


        public BoutiqueResponse GetBoutiques(BoutiqueRequest request)
        {
            var response = new BoutiqueResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.Boutiques = listManager.GetBoutiques(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        public SocialMediaResponse GetSocialMedia(SocialMediaRequest request)
        {
            var response = new SocialMediaResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.SocialMedia = listManager.GetSocialMedia(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        /// <summary>
        /// Returns the URL for the Kentico Banner Redirect page
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string GetBannerRedirectUrl(BannerUrlRequest request)
        {
            return KenticoApiManager.GetBannerRedirectUrl(request.SiteCode);
        }

        /// <summary>
        /// Returns the Advert URL for a given Banner ID
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string GetBannerUrl(BannerUrlRequest request)
        {

            KenticoApiManager kam = KenticoApiManager.GetInstance(ListManager.GetConnectionString(request.SiteCode));
            string rtnVar = kam.GetBannerUrl(request.BannerId);
            return rtnVar;
        }

        public WebExclusivePromotionResponse GetWebExclusivePromotions(WebExclusivePromotionRequest request)
        {

            var response = new WebExclusivePromotionResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    string conStr = ListManager.GetConnectionString(request.SiteCode);
                    KenticoApiManager kam = KenticoApiManager.GetInstance(conStr);
                    response.WebExclusivePromotions = kam.GetWebExclusivePromotions(conStr, request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;

                }

            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = "Error in WebExclusivePromotions " + ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
                Logger.Write(response.Message, Category.General, Priority.Highest, EventId.GeneralMessage);
            }

            return response;
        }

        public void CreateUserRequest(CreateUserRequest request)
        {
            try
            {
                UpdateKenticoData.CreateUserRequest(request);
            }
            catch (Exception ex)
            {
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
                Logger.Write(string.Format("Error occured in CreateUser for user {0} {1}", request.FullName, ex.Message), Category.General, Priority.Highest, EventId.GeneralMessage);
            }
        }

        public bool CheckContactGroupContainsContact(ContactGroupRequest contactGrpReq)
        {
            bool contactFound = false;

            try
            {
                if (!String.IsNullOrEmpty(contactGrpReq.SiteCode))
                {
                    string siteName = Rules.GetContentDomainSiteDisplayName(contactGrpReq.SiteCode);
                    KenticoApiManager kam = KenticoApiManager.GetInstance(ListManager.GetConnectionString(contactGrpReq.SiteCode));
                    return kam.CheckContactGroupContainsContact(contactGrpReq.ContactGUID, contactGrpReq.ContactGroups, siteName);
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
                Logger.Write(ex.Message, Category.General, Priority.Highest, EventId.GeneralMessage);
            }
            return contactFound;


        }
        public DirectShipContentResponse GetDirectShipContent(DirectShipContentRequest request)
        {
            var response = new DirectShipContentResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.DirectShipContent = listManager.GetDirectShipContent(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        public CloudMenuResponse GetCloudMenu(CloudMenuRequest request)
        {
            var response = new CloudMenuResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.CloudMenuKentico = listManager.GetCloudMenu(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }
        public EducationListResponse GetEducationContentList(EducationListRequest request)
        {
            var response = new EducationListResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response = listManager.GetEducationContentList(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }
        public CloudContentResponse GetCloudContent(CloudContentRequest request)
        {
            var response = new CloudContentResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.CloudContent = listManager.GetCloudContent(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }
        public CloudVendorsResponse GetCloudVendors(CloudVendorsRequest request)
        {
            CloudVendorsResponse response = new CloudVendorsResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response = listManager.GetCloudVendors(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        public ResponseBase UpdateCustomPromotion(CustomPromotionRequest request, CustomPromotionDTO data, bool isUpdateAttachmentOnly)
        {
            var response = new ResponseBase();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    bool result = listManager.UpdateCustomPromotion(request, data, isUpdateAttachmentOnly);

                    if (result)
                        response.Acknowledge = AcknowledgeType.Success;
                    else
                    {
                        response.Acknowledge = AcknowledgeType.Failure;
                        response.Message = RuleMessages.UpdateFailed;
                    }
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }

        public ResponseBase UpdateContactGroupResellers(UpdateContactGroupResellersRequest request)
        {
            var response = new ResponseBase();
            try
            {
                string validation = Rules.ValidateRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    var contactGroupManager = new ContactGroupManager(request);
                    bool result = contactGroupManager.UpdateContactGroupResellers(request);

                    if (result)
                        response.Acknowledge = AcknowledgeType.Success;
                    else
                    {
                        response.Acknowledge = AcknowledgeType.Failure;
                        response.Message = RuleMessages.UpdateFailed;
                    }
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }


        public ResponseBase UploadQuotePreferenceLogo(UploadQuotePreferenceLogoRequest request)
        {
            var response = new ResponseBase();
            try
            {
                var uploadImagemanager = new UploadImageManager(request);
                bool result = uploadImagemanager.ImportQuotePreferenceLogo(request);

                response.Acknowledge = AcknowledgeType.Success;
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;

                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
                Logger.Write(string.Format("Error occured in uploading the image for the file name  {0} {1}", request.filename, ex.Message), Category.General, Priority.Highest, EventId.GeneralMessage);
            }

            return response;
        }

        public ResponseBase DeleteQuotePreferenceLogo(UploadQuotePreferenceLogoRequest request)
        {
            var response = new ResponseBase();
            try
            {
                var uploadImagemanager = new UploadImageManager(request);
                bool result = uploadImagemanager.DeleteQuotePreferenceLogo(request);

                response.Acknowledge = AcknowledgeType.Success;
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;

                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
                Logger.Write(string.Format("Error occured in Deleting the image for the file name  {0} {1}", request.filename, ex.Message), Category.General, Priority.Highest, EventId.GeneralMessage);
            }

            return response;
        }
        public SavedEventResponse GetSavedEvents(SavedEventRequest request)
        {
            var response = new SavedEventResponse();
            try
            {
                string validation = Rules.ValidateListRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    ListManager listManager = new ListManager(request);
                    response.SavedEvents = listManager.GetSavedEventLists(request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;
                }
            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
            }
            return response;
        }
        public VendorConfigurableLinksResponse GetVendorConfigurableLinks(VendorConfigurableLinksRequest request)
        {
            var response = new VendorConfigurableLinksResponse();
            try
            {
                string validation = Rules.ValidateVendorLinkRequest(request);
                if (validation == RuleMessages.ValidationPassed)
                {
                    string conStr = ListManager.GetConnectionString(request.SiteCode);
                    KenticoApiManager kam = KenticoApiManager.GetInstance(conStr);
                    response.VendorConfigurableLinks = kam.GetVendorConfigurableLinks(conStr, request);
                    response.Acknowledge = AcknowledgeType.Success;
                }
                else
                {
                    response.Acknowledge = AcknowledgeType.Failure;
                    response.Message = validation;

                }

            }
            catch (Exception ex)
            {
                response.Acknowledge = AcknowledgeType.Failure;
                response.Message = "Error in VendorConfigurableLinks " + ex.Message;
                ErrorLogging.HandleException(ex, Constants.ExceptionPolicyName);
                Logger.Write(response.Message, Category.General, Priority.Highest, EventId.GeneralMessage);
            }

            return response;
        }
    }
}
