﻿// unobtrusive javascript look it up
var asyncLoading = {
    success: false,
    getPageName: function () {
        var pagename, path, i , j;
        path = window.location.href;
        i = path.lastIndexOf('/');
        //j = path.lastIndexOf('.aspx');  // ignore query strings etc
        //if (i >= 0) {
        //    if (j>0)
        //        pagename = path.substring(i + 1, j + i  );
        //    else
        //        pagename = path.substring(i + 1);
        //}
        pagename = path.substring(i + 1);
        return pagename;
    },

    getHtml: function (pageName) {
       // use ajax method to get html from generic synchrronous http handler - ashx 
        var passedData = { "pagename": pageName };
        $.ajax({
            url: "/handlers/cmshandler.ashx",
            async: 'true',
            data: passedData,
            type: 'POST',
            dataType: 'json',
            timeout: (100 * 1000), //seconds before timeout
            success: function (data) {
                asyncLoading.onSuccess(data);
            },
            error: function (objAJAXRequest, strError) {
                asyncLoading.onFailure(objAJAXRequest);
            }
        });
    },

    loadZones: function (jQuery) {
        // call ajax to populate json array with html of all page regions returned by cms
        asyncLoading.getHtml(asyncLoading.getPageName());
    },

    validateResponse: function (jsonArray){
        if (jsonArray != undefined) {
            if (jsonArray.length > 0) return true;
        }
        return false;
    },

    updateDom: function (jsonArray) {
        // for loop goes through array and finds matching elements in array and updates html of those elements with 
        // note: may need to make sure html is properly encoded and decoded so html links are correctly put in dom.
        for (var i = 0; i < jsonArray.length; i++) {
            $("#" + jsonArray[i].tag).html(jsonArray[i].html);
        }
    },

    onSuccess: function(jsonObject)
    {
        if (asyncLoading.validateResponse(jsonObject)) {
            asyncLoading.success = true;
            asyncLoading.updateDom(jsonObject);
        }
        else asyncLoading.onFailure(null);
    },
     
    onFailure: function (errorResponse) {
        // handle error (timeout, wrong url, etc. here)
        alert('Handle this error!');
    }

};

// Code to run when the document is ready.
$(document).ready(asyncLoading.loadZones);
   


