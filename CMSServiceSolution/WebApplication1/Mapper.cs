﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWebAppImOnline.Handlers;
using IM.CMS.Integration.CMSServiceProxy;

namespace TestWebAppImOnline
{
    internal static class Mapper
    {
        public static List<CMSHandler.RegionInfo> FromDataTransferObjects(List<DisplayRegion> regions)
        {
            if (regions == null) return null;
            return regions.Select(i=> FromDataTransferObject(i)).ToList();
        }

        public static CMSHandler.RegionInfo FromDataTransferObject(DisplayRegion region)
        {
            if (region == null) return null;
            return new CMSHandler.RegionInfo
            {
                   tag=region.TagId,
                   html=region.Html
            };
        }

    }
}