﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using IM.CMS.Integration;
using IM.CMS.Integration.CMSServiceProxy;
using HtmlAgilityPack;


namespace TestWebAppImOnline.Handlers
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class CMSHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class RegionInfo
        {
            public string tag { get; set; }
            public string html { get; set; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string pageName = string.Empty;
            // need to pass this to handler:  page  to determine which page to read from kentico
            if (!String.IsNullOrEmpty(context.Request.Form["pagename"]))
                pageName = context.Request.Form["pagename"];
            else
                pageName = "homepage.aspx";
            
            context.Response.Clear();
            context.Response.ContentType = "application/json";
        
            // call cms service and populate htmllist here.
            RegionRequest request = new RegionRequest();
            request.SiteCode = "test.ingrammicro.com";
            request.BrowserUserAgent = context.Request.UserAgent;
            request.IsSecureConnection = HttpContext.Current.Request.IsSecureConnection;
            request.UserSelectedCulture =  "nl-nl";  //for test can only use en-NL or nl-NL
          //  request.SAMAccountName = "dariusldariusl";           
            request.HostPageName = pageName;
            request.KenticoCurrentContactGuid = string.Empty;

            CMSController controller = new CMSController();
            string address = "http://localhost:61747/Service.svc";
            RegionsResponse response = controller.GetHtmlForRegions(address,request);
           
            if (response.Acknowledge == AcknowledgeType.Success)
            {
                List<RegionInfo> htmlList  = Mapper.FromDataTransferObjects(response.DisplayRegions);
                 /// Note:
                /// 1) will need to parse links to webtrends settings based on special class -- see GetEncryptedParameters in LegacyIntegrationHelper.cs in imonline
                /// 
                /// test only
                ///   htmlList = PopulateList();

                AddEncryptedQueryStrings(htmlList);

                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                string json = jsSerializer.Serialize(htmlList);
                context.Response.Write(json);
                context.Response.End();
            }
        }

        // used by 3rd party user tracking tool in imonline to track html a link clicks
        // this function updates href link with proper query string based if named class is present
        private void AddEncryptedQueryStrings(List<RegionInfo> htmlList)
        {
            foreach (RegionInfo region in htmlList)
            {
                AddEncryptedQueryStrings(region, "AddUserInfo");
            }
        }

        private void AddEncryptedQueryStrings(RegionInfo region, string className)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(region.html);

            // find all a  links with specific class AddUserWebtrendsInfo

            // get all div html in the main block section
            string aLinksClassFilter = "//a[contains(@class,'" + className + "')]";
            //HtmlNodeCollection resultHtml = htmlDoc.DocumentNode.SelectNodes(divmainBlock);
            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(aLinksClassFilter);

            if (nodes != null && nodes.Count > 0)
            {
                foreach (HtmlNode link in nodes)
                {
                    // add query string to those a links
                    HtmlAttribute att = link.Attributes["href"];
                    att.Value = UpdateHyperlinkAddress(att.Value);
                }

                //update html in region object
                region.html = doc.DocumentNode.OuterHtml;
            }
        }

        private string UpdateHyperlinkAddress(string originalValue)
        {
            // call special function to provide parameters see e.g. GetEncryptedParameters in LegacyIntegrationHelper.cs in imonline
            //testing only below
            string testParams = "?a1=test&a2=test2";
            return originalValue + testParams;
        }


        // for testing html delivery only
        private List<RegionInfo> PopulateList()
        {
            List<RegionInfo> listRegions = new List<RegionInfo>();

            RegionInfo region = new RegionInfo();
            StringBuilder sb = new StringBuilder();

            region.tag = "region1";
            sb.Append("<div>this is test of region 1</div>");
            region.html = sb.ToString();
            listRegions.Add(region);

            region = new RegionInfo();
            region.tag = "region2";
            sb.Append("<div> this is a test of region 2");
            sb.Append(@"<img src=""/Images/smiley.gif"" alt=""Smiley face"" height=""42"" width=""42"">");
            sb.Append(" </div>");
            region.html = sb.ToString();
            listRegions.Add(region);

            region = new RegionInfo();
            region.tag = "region3";
            sb=new StringBuilder();
            sb.Append("<div> this is a test of region 3");
            sb.Append(@"<img src=""/Images/bullet.png"" alt=""Smiley face"" height=""42"" width=""42"">");
            sb.Append(" </div>");
            region.html = sb.ToString();
            listRegions.Add(region);


            return listRegions;
        }
    }
}