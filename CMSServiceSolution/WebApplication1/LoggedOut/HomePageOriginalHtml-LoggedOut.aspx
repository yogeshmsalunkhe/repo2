﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomePageOriginalHtml-LoggedOut.aspx.cs" Inherits="TestWebAppImOnline.HomePageLoggedOutOriginalHtml" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Computer and Technology Products - Services for Business to Business Needs - Ingram Micro
    </title>
    <%--<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.min.js"></script>--%>

    <meta id="ctl00_sid" name="sidName" content="4008"/>
</head>
<body>
    
    <link href="../ScriptsImages/combined1.css" rel="stylesheet" type="text/css">
    <link href="../ScriptsImages/combined2.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="../ScriptsImages/print.css" media="print">
    <link rel="stylesheet" type="text/css" href="../ScriptsImages/license.css">
    <!--[if IE 6]>
<link rel="stylesheet" type="text/css" href="/_layouts/images/CSDefaultSite/styles/ie6.css">
<![endif]-->

    <!--[if IE 7]>
<link href="/_layouts/images/CSDefaultSite/styles/ie7.css" type="text/css" rel="stylesheet" />
<style type="text/css">
#superbox{margin-top: 40%;}

.in-stock img {padding-left:20px;}
</style>
<![endif]-->

    <!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="/_layouts/images/CSDefaultSite/styles/ie8.css">
<![endif]-->
    <!--[if lt IE 10]>
<script type="text/javascript" src="/_layouts/images/CSDefaultSite/javascripts/PIE.js"></script>
<![endif]-->

     <script type="text/javascript" src="../ScriptsImages/jquery-1.js"></script>
    <script type="text/javascript" src="../ScriptsImages/jquery-migrate-1.js"></script>
    <script type="text/javascript" src="../ScriptsImages/jquery-ui-1.js"></script>
    <%--<script type="text/javascript" src="../ScriptsImages/combined.js"></script>--%>
    <script type="text/javascript" src="../ScriptsImages/MiscJQPlugins.js"></script>
<%--                <script type="text/javascript" src="../ScriptsImages/combined_003.js"></script>--%>
       <script type="text/javascript" src="../ScriptsImages/WebTrendsMisc.js"></script>

    <meta name="description" content="Ingram Micro is the world�s largest distributor of computer and technology products, offering the largest selection of hardware and software products and services for business-to-business needs."/>
    <meta name="keywords" content="computer and technology products, software products, business to business, supply chain management, technology resellers, Ingram Micro"/>
      <!--[if lt IE 8]>
		<div style=' text-align: center; clear: both; height: 44px; padding:2px 0px 0px 0px; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
			<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
			</a>
		</div>
	<![endif]-->
    <form id="form1" runat="server">
        <div>
        </div>
        <div>
        </div>
       <div id="wrapper" class="">
           <div id="header">
                <div id="headerContainer" class="float-left header-width ">
                    <div class="header-top " id="master_header_top">
                        <a href="javascript:window.location.href=get_hostname(window.location)" id="qtp_logo">
                            <img src="../ScriptsImages/logo_imi.gif" alt="Ingram Micro Inc." class="ingram-logo" border="0" height="50" width="85"></a>
                        <a href="#" class="open-country-select-popup" id="qtp_country">
                            <span class="country-change">Change Country</span>
                        </a>
                        <div class="location-language">
                            Ingram Micro
                <a href="#" class="open-country-select-popup" id="qtp_country_popup">Netherlands</a> <span id="ctl00_spanverticalBar" class="vert-bar-space">|</span>
                            <select name="ctl00$lngSelecter$LanguagesDropDown" onchange="javascript:setTimeout('__doPostBack(\'ctl00$lngSelecter$LanguagesDropDown\',\'\')', 0)"
                                id="ctl00_lngSelecter_LanguagesDropDown" class="langselector">
                                <option value="nl-NL">Nederlands</option>
                                <option selected="selected" value="en-NL">English</option>
                            </select>
                            <input name="ctl00$lngSelecter$hidURLSelector" id="ctl00_lngSelecter_hidURLSelector" type="hidden">
                            <a href="#" class="open-country-select-popup" id="qtp_country_language"></a>
                            <div>
                                <a href="https://test.ingrammicro.com/Pages/help.aspx" id="qtp_country_help">Help</a>
                                <span class="vert-bar-space">|</span>
                                <a href="https://test.ingrammicro.com/Pages/IMAboutIngramMicro.aspx">About Us</a>
                            </div>
                        </div>
                        <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/Login.aspx" class=" key-login-btn">Hello. Login to<br>
                            your Account<span></span></a>

                        <div class="header-right-border"></div>
                    </div>
                    <div id="main-nav-expand">
                        <a name="skip-to-nav" id="qtp_skip"></a>
                        <table id="ctl00_tableLoggedIn" class="logged-out">
                            <tbody>
                                <tr>
                                    <td id="ctl00_tdProducts"><a href="javascript:void(0)" class="prod" id="product-activate">Products               
                                    </a></td>
                                    <td><a href="javascript:void(0)" class="vend" id="vendor-activate">Vendors</a></td>
                                    <td id="ctl00_tdCloudServices"><a href="javascript:void(0)" id="cloud-activate" class="cloud">Cloud Services</a></td>
                                    <td id="ctl00_tdServices"><a href="https://test.ingrammicro.com/Pages/ServicesAndSupport.aspx" class="serv" id="qtp_services">Services &amp; Support</a></td>
                                    <td><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/AllNews.aspx" class="news" id="qtp_news">News &amp; Events</a></td>
                                    <td><a href="https://test.ingrammicro.com/Pages/IGMContactUs.aspx" class="cont" id="qtp_contact">Contact Us</a></td>
                                    <td id="ctl00_tdBecomeAReseller">
                                        <a id="ResellerAppPageUrl" href="https://test.ingrammicro.com/Pages/BecomeAReseller.aspx" class="acct">Become a Reseller</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>


                    <div id="sub-navigation">
                        <div id="products-menu-container" class="mega-menu-container">
                            <div class="primary-menu" id="primary-products-menu">
                                <div class="inner-spacing">
                                    <div>
                                        <div class="pane-title">Categories</div>
                                        <div class="all-link"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=Np:All&amp;mnc=true">View All</a></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="prodTopDown">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="prodTopUp" class="deactivate">Up</a></div>
                                    </div>
                                    <div class="pane-container" id="primary-menu-page-container">
                                        <div class="pane-menu">
                                        </div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="prodBottomDown">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="prodBottomUp" class="deactivate">Up</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="vendor-menu-container" class="mega-menu-container">
                            <div class="vendor-inner">
                                <div class="primary-menu" id="primary-vendors-menu">
                                    <div class="alpha-list" id="vendors-alpha-list">
                                    </div>
                                    <div>
                                        <div class="pane-title">Vendors</div>
                                        <div class="all-link"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/AllVendors.aspx">View All</a></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="vendorTopDown" href="#" class="">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="vendorTopUp" class="deactivate" href="#">Up</a></div>
                                    </div>
                                    <div class="pane-container">
                                        <div class="pane-menu">
                                        </div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="vendorBottomDown" href="#" class="">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="vendorBottomUp" class="deactivate" href="#">Up</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Div1">
                        <div id="cloud-menu-container" class="mega-menu-container" style="position: absolute; left: 172.333px;">
                            <div class="primary-menu" id="primary-clouds-menu">
                                <div class="inner-spacing">
                                    <div style="margin-left: 5px; margin-top: 5px" class="pane-menu"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/Education.aspx" style="width: 90px;" id="EducationBtn">Education<span style="position: absolute; left: 90px;"></span></a></div>
                                    <div>
                                        <div class="pane-title" style="clear: both; margin-top: 5px">Categories</div>
                                        <div class="all-link"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:244">View All</a></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="A2">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="A3" class="deactivate">Up</a></div>
                                    </div>

                                    <div class="pane-container" id="primary-menu-Cloudpage-container" style="clear: both;">
                                        <div class="pane-menu">
                                        </div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="A4">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="A5" class="deactivate">Up</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="search" id="qtp_searchbar">

                        <div class="search-content" id="searchOptions">
                            <div>
                                <div class="live-search-container" id="search-field-container">
                                    <input value="Search by Keyword(s), VPN or IM SKU" style="color: rgb(175, 175, 175);" id="searchBox_Global" class="search-text placeholder search-box-global" rel="Search by Keyword(s), VPN or IM SKU" autocomplete="off" type="text">
                                    <div id="live-search" class="global">
                                    </div>
                                </div>
                                <a href="#" class="search-submit tooltip-clickable" id="search-submit-anchor" rel="global-search-tooltip">Search:
                                </a>
                                <a href="#" class="search-tips tooltip-text-search" style="cursor: help;" id="search-tips-anchor"></a>
                                <div style="clear: left;"></div>
                            </div>
                        </div>
                        <div class="hide" id="global-search-tooltip-info">
                            <p>Search</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div id="dashboardContainer" class="float-left w237 h100pct ">
                </div>

                <div class="clear"></div>
            </div>
           <div id="container">
                <a name="begin-content"></a>
                <div id="main-content">
                    <div id="ctl00_fullLayoutDiv">

                        <div id="ctl00_PlaceHolderIMHome_PNAAvailability" name="PNAAvailabilityMesg" class="pna-alert-home hidden-control">
                            <div class="content">
                                <div class="pna-alert-icon"></div>
                                <p>Pricing is currently not real time and may change.</p>
                                <div class="clear"></div>
                            </div>
                        </div>

                          <%--CMS insertion in this div--  ID="leaderboard" will be added  --%>
                        <div class="leaderboard main-block">
                            <div class="content">
                                <div class="ExternalClass9DB2FD0FDD0B41718BEBD9FD94B2DF15">
                                    <div>
                                        <table border="0" cellpadding="0" cellspacing="0" width="700">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <img src="../ScriptsImages/NL-Logged-Out-1.jpg" border="0" height="140" width="714"></td>
                                                </tr>
                                                <tr>
                                                    <td><a href="https://test.ingrammicro.com/Pages/BecomeAReseller.aspx">
                                                        <img src="../ScriptsImages/NL-Logged-Out-2.jpg" border="0" height="23" width="714"></a></td>
                                                </tr>
                                                <tr>
                                                    <td><a href="https://test.ingrammicro.com/Pages/help.aspx">
                                                        <img src="../ScriptsImages/NL-Logged-Out-3.jpg" border="0" height="42" width="714"></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input name="ctl00$PlaceHolderIMHome$imAdvertisementSlider$hdnSliderTimeSpan" id="ctl00_PlaceHolderIMHome_imAdvertisementSlider_hdnSliderTimeSpan" value="5000" type="hidden">

                        <script type="text/javascript">
                            $(document).ready(function () {
                                var timeBetweenTransitions = $('#ctl00_PlaceHolderIMHome_imAdvertisementSlider_hdnSliderTimeSpan').val();
                                setupContentRotator('ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdSlider', 'ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdPager', timeBetweenTransitions);
                            });

                            function setupContentRotator(sliderId, pagerId) {
                                var sliderControl = $('#' + sliderId);
                                var arrSlides = new Array();
                                var pagerControl = $('#' + pagerId);
                                var Imagetitle;

                                if (sliderControl.children().length > 1) {
                                    sliderControl.cycle({
                                        pager: '#' + pagerId,
                                        cleartype: 1,
                                        pagerClick: function () {
                                            sliderControl.cycle('pause');
                                        },
                                        after: function (isNext, idx, slide) {
                                            Imagetitle = $('A', this)[0].title;
                                            if ($.inArray(Imagetitle, arrSlides) <= -1) {
                                                var v1 = $(this);
                                                cleanupWebtrends();
                                                dcsMultiTrack('DCS.dcsuri', '/Marketing Placement', 'WT.ad', $('A', this)[0].title, 'WT.dl', '50');
                                                arrSlides.push(Imagetitle);
                                            }
                                        }
                                    });
                                    pagerControl.css('left', sliderControl.width() / 2 - pagerControl.width() / 2 + 'px');
                                }
                                else if (sliderControl.children().length == 1) {
                                    pagerControl.hide();
                                    cleanupWebtrends();
                                    var imagetitle = '';
                                    if (sliderControl.children()[0].children.length > 0) {
                                        imagetitle = sliderControl.children()[0].children[0].title;
                                    }
                                    dcsMultiTrack('DCS.dcsuri', '/Marketing Placement', 'WT.ad', imagetitle, 'WT.dl', '50');

                                }
                                else {
                                    pagerControl.hide();
                                }
                            }
                        </script>

                        <%--CMS insertion in this div--  ID="adrotator" will be added & special js added for rotator to work --%>
                        <div id="adrotator" class="main-block ad-rotator">
                            <div class="content">
                                <div id="ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdRotator" class="image-slider">
                                    <div style="left: 344px;" id="ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdPager" class="image-pager">
                                        <a class="activeSlide" href="#">1</a>
                                        <a class="" href="#">2</a>
                                    </div>
                                    <div style="position: relative; width: 718px; height: 151px;" id="ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdSlider" class="image-slideshow">
                                        <div style="position: absolute; top: 0px; left: 0px; display: block; z-index: 3; opacity: 0.630421; width: 718px; height: 151px;">
                                            <a href="http://www.ingrammicro.nl/concrete/index.php?cID=3577" target="_blank"
                                                onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_x-03000149-campagneblok-as','WT.dl','50'); "
                                                title="MOSS_x-03000149-campagneblok-as">
                                                <img src="../ScriptsImages/X-03000149-campagneblok-AS.jpg"></a>
                                        </div>
                                        <div style="position: absolute; top: 0px; left: 0px; display: block; z-index: 2; opacity: 0.369579; width: 718px; height: 151px;">
                                            <a href="http://www.ingrammicro.nl/concrete/klanten/category_stores/mobility/" target="_blank"
                                                onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_x-03000179-campaignblock-ingrammicro2','WT.dl','50'); "
                                                title="MOSS_x-03000179-campaignblock-ingrammicro2">
                                                <img src="../ScriptsImages/X-03000179-Campaignblock-IngramMicro2.jpg">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                buildSliders('#ctl00_PlaceHolderIMHome_ctl02_pnlRecoSlider');

                                $(".content-slider a[id*='productDetailsLink']").each(function () {
                                    $(this).addClass("tooltip-clickable-webtrends");
                                });
                                $(".content-slider p.product-name a").each(function () {
                                    $(this).addClass("tooltip-clickable-webtrends");
                                });
                                $(".tooltip-clickable-webtrends").click(function () {
                                    var href = $(this).attr('href');
                                    setTimeout(function () { window.location = href }, 3000);
                                    return false;
                                });
                            });
                        </script>

                        <div id="ctl00_PlaceHolderIMHome_ctl02_mainDiv" class="ingram-recommendations">
                            <div class="blue-header">
                                <h2>Ingram Micro Recommendations</h2>
                                <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/MyOffers.aspx?zone=HOME_PAGE_BR_NAME">View All</a>
                            </div>

                            <div id="ctl00_PlaceHolderIMHome_ctl02_contentDiv" class="content">
                                <div id="ctl00_PlaceHolderIMHome_ctl02_pnlRecoSlider" class="content-slider">

                                    <div style="overflow: hidden;" class="slide-container">

                                        <div style="width: 715px;" class="display-control">
                                            <table style="float: left; width: 715px;" class="slide">
                                                <tbody>
                                                    <tr>
                                                        <td class="first-cell">
                                                            <a class="tooltip-clickable-webtrends" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_productDetailsLink"
                                                                onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');"
                                                                href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000002443400">
                                                                <img src="../ScriptsImages/1025124459.jpg" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductImageControl1_imgProduct" width="95">
                                                            </a>
                                                            <div class="adj-height">
                                                                <p style="height: 28px; overflow: auto;" class="product-name">
                                                                    <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink" class="tooltip-clickable tooltip-clickable-webtrends" onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000002443400"><span class="italic">HP</span>&nbsp;HP ProBook 450 i7-3632QM 15.6 4G/750G w8</a>
                                                                </p>
                                                                <div class="in-stock-details" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink-info" visible="False">
                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductTitleLinkControl1$PTLCWebTrendsSKU"
                                                                        id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductTitleLinkControl1_PTLCWebTrendsSKU" value="2443400" type="hidden">
                                                                    <p>
                                                                        HP ProBook 450 G0 15.6" LED Notebook - Intel - Core i7 i7-3632QM
 2.2GHz - 4 GB RAM - 750 GB HDD - DVD-Writer - AMD Radeon HD 8750M, 
Intel HD 4000 Graphics - Windows 8 Pro 64-bit - 1366 x 768 Display - 
Bluetooth
                                                                    </p>
                                                                </div>
                                                                <p style="height: 15px; overflow: auto;" class="vpn-num">
                                                                    VPN: <span id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_VPN">A6G72EA#ABH</span>
                                                                </p>
                                                                <script type="text/javascript">
                                                                    var CloudMode = 'EDV_Full';
                                                                </script>
                                                                <div class="pricing" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl_PNA_000000000002443400">
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    $("span.your-price.notax").css("display", "none");
                                                                </script>
                                                            </div>
                                                            <div class="specific-details">
                                                                <div id="custom-promo-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl001singleProductHighlightControl" class="tooltip">
                                                                    <img id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_img_HighlightCustomPromoImageControl" class="promotional-icon" src="../ScriptsImages/GoPro_SmallIconPath.jpg" style="border-width: 0px; height: 20px; width: 20px;"><br>
                                                                    <br>
                                                                </div>
                                                                <div class="tooltip-details" id="custom-promo-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl001singleProductHighlightControl-info">
                                                                    <p>
                                                                        Actieproduct GoPro
                                                                    </p>
                                                                </div>
                                                                <div class="special-flags">
                                                                    <!-- Free Item flag start here -->
                                                                    <!-- Free Item flag end here -->
                                                                </div>
                                                            </div>
                                                            <div class="icons">
                                                            </div>
                                                        </td>
                                                        <td class="dotted-divider"></td>
                                                        <td class="second-cell">
                                                            <a class="tooltip-clickable-webtrends" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_productDetailsLink"
                                                                onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');"
                                                                href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000002151304">
                                                                <img src="../ScriptsImages/1023603459.jpg" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductImageControl1_imgProduct" width="95">
                                                            </a>
                                                            <div class="adj-height">

                                                                <p style="height: 28px; overflow: auto;" class="product-name">
                                                                    <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink" class="tooltip-clickable tooltip-clickable-webtrends" onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000002151304"><span class="italic">LENOVO</span>&nbsp;ThinkPad Tablet 2 10.1  Z2760 64GB W8PRO</a>
                                                                </p>
                                                                <div class="in-stock-details" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink-info" visible="False">
                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductTitleLinkControl1$PTLCWebTrendsSKU"
                                                                        id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_PTLCWebTrendsSKU"
                                                                        value="2151304" type="hidden">
                                                                    <p>
                                                                        Lenovo ThinkPad Tablet 2 367925G 64GB Net-tablet PC - 10.1" - 3G
 - Intel - Atom Z2760 1.8GHz - 2 GB RAM - Windows 8 Pro - Slate - 1366 x
 768 Multi-touch Screen Display
                                                                    </p>
                                                                </div>

                                                                <p style="height: 15px; overflow: auto;" class="vpn-num">
                                                                    VPN: <span id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_VPN">N3S25MH</span>
                                                                </p>

                                                                <script type="text/javascript">
                                                                    var CloudMode = 'EDV_Full';
                                                                </script>

                                                                <div class="pricing" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl_PNA_000000000002151304">
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    $("span.your-price.notax").css("display", "none");
                                                                </script>
                                                            </div>

                                                            <div class="specific-details">
                                                                <div id="custom-promo-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl011singleProductHighlightControl" class="tooltip">
                                                                    <img id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_img_HighlightCustomPromoImageControl" class="promotional-icon" src="../ScriptsImages/GoPro_SmallIconPath.jpg" style="border-width: 0px; height: 20px; width: 20px;"><br>
                                                                    <br>
                                                                </div>
                                                                <div class="tooltip-details" id="custom-promo-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl011singleProductHighlightControl-info">
                                                                    <p>
                                                                        Actieproduct GoPro
                                                                    </p>
                                                                </div>
                                                                <div class="special-flags">
                                                                </div>
                                                            </div>

                                                            <div class="icons">
                                                            </div>
                                                        </td>
                                                        <td class="dotted-divider"></td>
                                                        <td class="third-cell">
                                                            <a class="tooltip-clickable-webtrends" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_productDetailsLink"
                                                                onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');"
                                                                href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000002510161">
                                                                <img src="../ScriptsImages/1026133503.jpg" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductImageControl1_imgProduct" width="95">
                                                            </a>
                                                            <div class="adj-height">

                                                                <p style="height: 28px; overflow: auto;" class="product-name">
                                                                    <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink"
                                                                        class="tooltip-clickable tooltip-clickable-webtrends"
                                                                        onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');"
                                                                        href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000002510161">
                                                                        <span class="italic">BROTHER</span>&nbsp;DCP-1510 Inkjet AIO</a>
                                                                </p>
                                                                <div class="in-stock-details" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink-info" visible="False">
                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductTitleLinkControl1$PTLCWebTrendsSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductTitleLinkControl1_PTLCWebTrendsSKU" value="2510161" type="hidden">
                                                                    <p>
                                                                        Brother DCP-1510 Laser Multifunction Printer - Monochrome - 
Plain Paper Print - Desktop - Copier/Printer/Scanner - 21 ppm Mono Print
 - 2400 x 600 dpi Print - 21 cpm Mono Copy - 600 dpi Optical Scan - 
Manual Duplex Print - 150 sheets Input - USB
                                                                    </p>
                                                                </div>
                                                                <p style="height: 15px; overflow: auto;" class="vpn-num">
                                                                    VPN: <span id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_VPN">DCP1510H1</span>
                                                                </p>

                                                                <script type="text/javascript">
                                                                    var CloudMode = 'EDV_Full';
                                                                </script>


                                                                <div class="pricing" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl_PNA_000000000002510161">
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    $("span.your-price.notax").css("display", "none");
                                                                </script>
                                                            </div>

                                                            <div class="specific-details">
                                                                <div class="special-flags">
                                                                </div>
                                                            </div>
                                                            <div class="icons">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fourth-cell"></td>
                                                        <td class="dotted-divider"></td>
                                                        <td class="fifth-cell"></td>
                                                        <td class="dotted-divider"></td>
                                                        <td class="sixth-cell"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div></div>
                                    <div style="cursor: default" class="slider-control left-control">
                                        <div class="tab-top-left"></div>
                                        <div class="tab-left control-off"></div>
                                        <div class="tab-bottom-left"></div>
                                    </div>
                                    <div style="cursor: default" class="slider-control right-control">
                                        <div class="tab-top-right"></div>
                                        <div class="tab-right control-off"></div>
                                        <div class="tab-bottom-right"></div>
                                    </div>
                                    <div class="clear"></div>

                                </div>
                            </div>
                        </div>

                        <link rel="stylesheet" href="../ScriptsImages/prettyPhoto.css" type="text/css" media="screen" charset="utf-8">
                        <script type="text/javascript" src="../ScriptsImages/combined_002.js"></script>

                        <div id="ctl00_PlaceHolderIMHome_pdSAPSpecialBids_pnlSpecialBidsPopup" class="modal-popup modal-popup-b special-bids-popup" style="display: none; position: fixed; z-index: 100001; top: 30px;">


                            <div id="specialBidsContent" class="special-bids-content h100pct p2">
                                <div class="product-summary w100pct">
                                    <div class="prod-image float-left">
                                        <ul id="ctl00_PlaceHolderIMHome_pdSAPSpecialBids_prodImgs" class="picture-gallery clearfix">
                                            <li><a id="prodImgLink" href="" rel="pictures[specialbid]" title="">
                                                <img id="prodImg" style="border-width: 0px;" src="" class="special-bid-popup-product-image" alt="" height="50"></a></li>
                                        </ul>
                                    </div>
                                    <div class="float-left mb5">
                                        <p class="product-name">
                                            <a href="" class="special-bid-popup-product-link">
                                                <span class="special-bid-popup-product-name" id="lblName"></span></a>
                                        </p>
                                        <p class="vpn-num">
                                            VPN: <span class="special-bid-popup-vpn-num" id="lblVPN"></span>
                                            <span class="vert-bar-space">|</span>
                                            EAN: <span class="special-bid-popup-ean-num" id="lblEAN"></span>

                                            <span class="vert-bar-space">|</span>
                                            <span class="sku">SKU: <span class="special-bid-popup-sku-num" id="lblSKU"></span>
                                            </span>
                                        </p>
                                    </div>
                                </div>

                                <div class="breadcrumb w97pct h16">
                                    <div id="sap-sb-breadcrumb-container"></div>
                                </div>
                            </div>
                            <a id="closePopup" class="cancel-btn-top cursor-pointer closeSpecialBid">Close
                            </a>
                        </div>

                        <script type="text/html" id="tmpl_sbSAPBreadCrumb">
                            <div class="filters">
                                <#
    if (BreadCrumbData != null && BreadCrumbData.Breadcrumbs != null && BreadCrumbData.Breadcrumbs.length > 0)
    {
        var firstCrumb = true;

        for(var i=0; i < BreadCrumbData.Breadcrumbs.length; i++) 
        {
            var crumb = BreadCrumbData.Breadcrumbs[i];
            var cDisplayName = crumb.DisplayName;
            var divId = "cCrumb" + i + "#" + crumb.CrumbType;
      
            if (firstCrumb) 
            {
                firstCrumb = false; 
#>
                <span class="filtered-label">${UI.FilteredByLbl} 
                    (<a href="#" class="breadcrumb-a" id="cCrumb#9">
                        ${UI.ClearAllLbl}
                    </a>):
                </span>
                                <#          
            }       
#>
            ${cDisplayName}
            <span class="remove-facet">[ <a href="#" class="breadcrumb-a" id="${divId}">x</a> ]
            </span>
                                <#
        }
    }
#>
                            </div>
                        </script>
                        <a aria-disabled="false" role="button" id="ctl00_PlaceHolderIMHome_lbnOpenEndUserPopup"
                            class="button popup-button end-user-button hide ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            <span class="ui-button-text">Select Vendor Special Bid Information</span>
                        </a>

                        <div class="modalBackground" style="display: none; position: fixed; left: 0px; top: 0px; z-index: 10000;" id="behaviorSpecialBid_backgroundElement"></div>
                    </div>
                    <div style="height: 66px;" class="filler" id="filler-div">
                        <div style="height: 53px;" class="inner"></div>
                    </div>
                </div>
            </div>

            <%--CMS insertion in this div -- will use existing id="sidebar" identify div  --%>
           <div id="sidebar">
                <div class="banner-ads sidebar-block">
                    <div class="ad-top">
                        <div><a href="http://www.ingrammicro.nl/concrete/index.php/home/werkenbij" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_werken bij ingram micro','WT.dl','50'); " title="MOSS_werken bij ingram micro">
                            <img src="../ScriptsImages/webspot-werken-bij-Ingram-Micro.jpg"></a></div>
                    </div>
                    <div class="divider"></div>
                    <div class="ad-top">
                        <div><a href="http://www.ingrammicro.nl/concrete/overig/volg-ingram-micro-via-social-media" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_solcial media new link','WT.dl','50'); " title="MOSS_solcial media new link">
                            <img src="../ScriptsImages/Socialmedia.png"></a></div>
                    </div>
                    <div class="divider"></div>
                    <div class="ad-top">
                        <div><a href="http://www.ingrammicro.nl/concrete/index.php?cID=2368" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_feedback-button','WT.dl','50'); " title="MOSS_feedback-button">
                            <img src="../ScriptsImages/Feedback-Button.jpg"></a></div>
                    </div>
                    <div class="divider"></div>
                    <div class="ad-top">
                        <div><a href="http://mobility.ingrammicro.com/?utm_source=ingram_micro&amp;utm_medium=banner&amp;utm_content=netherlands&amp;utm_campaign=brightpoint_announcement" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_webspot-brightpoint','WT.dl','50'); " title="MOSS_webspot-brightpoint">
                            <img src="../ScriptsImages/X-03000142-Webspot-Brightpoint.jpg"></a></div>
                    </div>
                    <div></div>
                </div>
            </div>
           <div id="footer">
                <ul id="ctl00_imFooter_Ul1">
                    <li id="ctl00_imFooter_Products">
                        <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=Np:All" id="qtp_footer_search">Products
                        </a>
                    </li>
                    <li id="ctl00_imFooter_Vendors">
                        <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/AllVendors.aspx?mnc=true">Vendors
                        </a>
                    </li>
                    <li id="ctl00_imFooter_CloudServices">
                        <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:244">Cloud Services
                        </a>
                    </li>
                    <li id="ctl00_imFooter_Services">
                        <a href="https://test.ingrammicro.com/Pages/ServicesAndSupport.aspx" id="qtp_footer_support">Services &amp; Support
                        </a>
                    </li>
                    <li>
                        <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/AllNews.aspx" id="qtp_footer_news">News and Events
                        </a>
                    </li>
                    <li>
                        <a href="https://test.ingrammicro.com/Pages/IGMContactUs.aspx" id="qtp_footer_contact">Contact Us
                        </a>
                    </li>
                    <li>
                        <a href="https://test.ingrammicro.com/Pages/IMAboutIngramMicro.aspx" id="qtp_footer_about">About Us
                        </a>
                    </li>
                </ul>
                <div class="clear">
                </div>
                <div class="copyright" id="qtp_footer_copyright">
                    <div class="bottom-footer-right">
                    </div>
                    <div class="bottom-footer-left">
                        Copyright © 2014 Ingram Micro. All rights reserved.
            <span class="spacing">&nbsp;</span><!-- Phone -->
                    </div>

                </div>
            </div>
        </div>

    </form>
    <ul style="display: none;" tabindex="0" id="ui-id-1" class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all"></ul>
    <div id="ingramTooltipCover" class="tooltip-cover-container"></div>
    <div style="display: none;" id="tooltip">
        <h3></h3>
        <div class="body"></div>
        <div class="url"></div>
    </div>

    <script type="text/javascript" src="Scripts/AsyncLoad.js"></script>
</body>
</html>
