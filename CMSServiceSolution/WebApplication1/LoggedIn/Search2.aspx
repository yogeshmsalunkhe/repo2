﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search2.aspx.cs" Inherits="TestWebAppImOnline.LoggedIn.Search2" %>

<!DOCTYPE html>

<html>
<!-- IMApplication -->
<head id="ctl00_Head1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <title>Product Search
    </title>
    <meta id="ctl00_sid" name="sidName" content="4008">
</head>
<body style="background: none repeat scroll 0% 0% transparent; cursor: default;" id="ctl00_MasterPageBodyTag">
    <link href="../ScriptsImages/combined1.css" rel="stylesheet" type="text/css">
    <link href="../ScriptsImages/combined2.css" rel="stylesheet" type="text/css">

<%--    <link rel="stylesheet" type="text/css" href="../ScriptsImages/print.css" media="print">
    <link rel="stylesheet" type="text/css" href="../ScriptsImages/license.css">--%>
    <!--[if IE 6]>
<link rel="stylesheet" type="text/css" href="/_layouts/images/CSDefaultSite/styles/ie6.css">
<style type="text/css">
.modalPopup 
{
    margin-left:-100px;
    margin-top:-100px;
}
</style>
<![endif]-->

    <!--[if IE 7]>
<link href="/_layouts/images/CSDefaultSite/styles/ie7.css" type="text/css" rel="stylesheet" />
<style type="text/css">
#superbox{margin-top: 40%;}
.email-content{width:398px;height:298px
}

</style>
<![endif]-->

    <!--[if lt IE 10]>
    <script type="text/javascript" src="/_layouts/images/CSDefaultSite/javascripts/PIE.js"></script>
    <![endif]-->

    <script type="text/javascript" src="../ScriptsImages/jquery-1.js"></script>
    <script type="text/javascript" src="../ScriptsImages/jquery-migrate-1.js"></script>
    <script type="text/javascript" src="../ScriptsImages/jquery-ui-1.js"></script>

    <script type="text/javascript" src="../ScriptsImages/MiscJQPlugins.js"></script>
    <script type="text/javascript" src="../ScriptsImages/WebTrendsMisc.js"></script>


    <form name="aspnetForm" method="post" id="aspnetForm">

        <div id="ctl00_dvNoMasterCodeNeeded">
            <a class="hide" href="#skip-to-nav" id="qtp_skip_to_nav">Skip to main navigation</a>
            <a class="hide" href="#begin-content" id="qtp_begin_content">Skip to main content</a>

            <div id="wrapper" class="logged-in">
                <div id="ctl00_fullLayoutDiv" class="searchFullLength">
                    <div id="header">

                        <div id="headerContainer" class="float-left header-width ">

                            <div class="header-top " id="master_header_top">
                                <a href="javascript:window.location.href=get_hostname(window.location)" id="qtp_logo">
                                    <img src="../ScriptsImages/logo_imi.gif" alt="Ingram Micro Inc." class="ingram-logo" border="0" height="50" width="85"></a>
                                <a href="#" class="open-country-select-popup" id="qtp_country">
                                    <span class="country-change">Change Country</span>
                                </a>
                                <div class="location-language">
                                    Ingram Micro
                                    <a href="#" class="open-country-select-popup" id="qtp_country_popup">Netherlands</a>
                                    <span id="ctl00_spanverticalBar" class="vert-bar-space">|</span>

                                    <select name="ctl00$lngSelecter$LanguagesDropDown" onchange="javascript:setTimeout('__doPostBack(\'ctl00$lngSelecter$LanguagesDropDown\',\'\')', 0)" id="ctl00_lngSelecter_LanguagesDropDown" class="langselector">
                                        <option value="nl-NL">Nederlands</option>
                                        <option selected="selected" value="en-NL">English</option>

                                    </select>
                                    <input name="ctl00$lngSelecter$hidURLSelector" id="ctl00_lngSelecter_hidURLSelector" type="hidden">

                                    <a href="#" class="open-country-select-popup" id="qtp_country_language"></a>


                                    <div>
                                        <a href="https://test.ingrammicro.com/Pages/help.aspx" id="qtp_country_help">Help</a> <span class="vert-bar-space">|</span> <a href="https://test.ingrammicro.com/Pages/IMAboutIngramMicro.aspx">About Us</a>

                                        <span class="vert-bar-space">|</span>
                                        <a id="ctl00_btnLogout" href="javascript:__doPostBack('ctl00$btnLogout','')">Log Out</a>
                                        <a onclick="showBusy(false, null);" id="ctl00_LinkButton1" href="javascript:__doPostBack('ctl00$LinkButton1','')"></a>

                                    </div>
                                </div>

                                <div class="help-about " id="login_help-about">
                                    <span class="body-blue-header welcome-header">Darius  Lezama</span><br>
                                    <div style="white-space: nowrap;">
                                        IM Account #: <span class="colorBlack">186992</span>
                                        <br>
                                    </div>
                                </div>
                                <div class="header-right-border"></div>
                            </div>
                            <div id="main-nav-expand">
                                <a name="skip-to-nav" id="qtp_skip"></a>
                                <table id="ctl00_tableLoggedIn" class="logged-in">
                                    <tbody>
                                        <tr>
                                            <td id="ctl00_tdProducts"><a href="javascript:void(0)" class="prod" id="product-activate">Products
                                            </a></td>
                                            <td><a href="javascript:void(0)" class="vend" id="vendor-activate">Vendors</a></td>
                                            <td id="ctl00_tdCloudServices"><a href="javascript:void(0)" id="cloud-activate" class="cloud">Cloud Services</a></td>
                                            <td id="ctl00_tdServices"><a href="https://test.ingrammicro.com/Pages/ServicesAndSupport.aspx" class="serv" id="qtp_services">Services &amp; Support</a></td>
                                            <td><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/AllNews.aspx" class="news" id="qtp_news">News &amp; Events</a></td>
                                            <td><a href="https://test.ingrammicro.com/Pages/IGMContactUs.aspx" class="cont" id="qtp_contact">Contact Us</a></td>
                                            <td id="ctl00_tdMyAccount" class="border-right-none"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/MyAccount.aspx" class="acct" id="qtp_account">My Account</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div id="sub-navigation">
                                <div id="products-menu-container" class="mega-menu-container">
                                    <div class="primary-menu" id="primary-products-menu">
                                        <div class="inner-spacing">
                                            <div>
                                                <div class="pane-title">Categories</div>
                                                <div class="all-link"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=Np:All&amp;mnc=true">View All</a></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a href="#" id="prodTopDown">Down</a></div>
                                                <div class="scroll-control scroll-up"><a href="#" id="prodTopUp" class="deactivate">Up</a></div>
                                            </div>
                                            <div class="pane-container" id="primary-menu-page-container">
                                                <div class="pane-menu">
                                                    <ul id="primary-menu-links">

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288449839&amp;t=pTab" id="3_4288449839PP1">Audio/Visual
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294967244&amp;t=pTab" id="3_4294967244PP1">Cables
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288449895&amp;t=pTab" id="3_4288449895PP1">Cameras &amp; Scanners
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294954486&amp;t=pTab" id="3_4294954486PP1">Communications
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294637804&amp;t=pTab" id="3_4294637804PP1">Components
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288450088&amp;t=pTab" id="3_4288450088PP1">Computers
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288450111&amp;t=pTab" id="3_4288450111PP1">Connectivity Devices
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a href="#" id="prodBottomDown">Down</a></div>
                                                <div class="scroll-control scroll-up"><a href="#" id="prodBottomUp" class="deactivate">Up</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="secondary-menu hide" id="secondary-menu">
                                        <div class="inner-spacing">
                                            <div class="pane-title">Sub-Categories</div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="catprodTopDown" href="#">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="catprodTopUp" href="#" class="deactivate">Up</a></div>
                                            </div>
                                            <div class="pane-container">
                                                <div class="pane-menu"></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a href="#" id="catprodBottomDown">Down</a></div>
                                                <div class="scroll-control scroll-up"><a href="#" id="catprodBottomUp" class="deactivate">Up</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vendor-menu hide" id="products-vendor-menu">
                                        <div class="inner-spacing">
                                            <div class="pane-title">Vendors</div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="subcatprodTopDown" href="#">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="subcatprodTopUp" href="#" class="deactivate">Up</a></div>
                                            </div>
                                            <div class="pane-container">
                                                <div class="pane-menu"></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="subcatprodBottomDown" href="#">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="subcatprodBottomUp" href="#" class="deactivate">Up</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="vendor-menu-container" class="mega-menu-container">
                                    <div class="vendor-inner">
                                        <div class="primary-menu" id="primary-vendors-menu">
                                            <div class="alpha-list" id="vendors-alpha-list">
                                                <ul>
                                                    <li class="first"><a id="a1" href="#" class="">#</a></li>
                                                    <li><a id="alpha-a" href="#" class="">a</a></li>
                                                    <li><a id="alpha-b" href="#" class="">b</a></li>
                                                    <li><a id="alpha-c" href="#" class="">c</a></li>
                                                    <li><a id="alpha-d" href="#" class="">d</a></li>
                                                    <li><a id="alpha-e" href="#" class="">e</a></li>
                                                    <li><a id="alpha-f" href="#" class="">f</a></li>
                                                    <li><a id="alpha-g" href="#" class="">g</a></li>
                                                    <li><a id="alpha-h" href="#" class="">h</a></li>
                                                    <li><a id="alpha-i" href="#" class="">i</a></li>
                                                    <li><a id="alpha-j" href="#" class="">j</a></li>
                                                    <li><a id="alpha-k" href="#" class="">k</a></li>
                                                    <li><a id="alpha-l" href="#" class="">l</a></li>
                                                    <li><a id="alpha-m" href="#" class="">m</a></li>
                                                    <li><a id="alpha-n" href="#" class="">n</a></li>
                                                    <li><a id="alpha-o" href="#" class="">o</a></li>
                                                    <li><a id="alpha-p" href="#" class="">p</a></li>
                                                    <li><a id="alpha-q" href="#" class="">q</a></li>
                                                    <li><a id="alpha-r" href="#" class="">r</a></li>
                                                    <li><a id="alpha-s" href="#" class="">s</a></li>
                                                    <li><a id="alpha-t" href="#" class="">t</a></li>
                                                    <li><a id="alpha-u" href="#" class="">u</a></li>
                                                    <li><a id="alpha-v" href="#" class="">v</a></li>
                                                    <li><a id="alpha-w" href="#" class="">w</a></li>
                                                    <li><a id="alpha-x" href="#" class="">x</a></li>
                                                    <li><a id="alpha-y" href="#" class="">y</a></li>
                                                    <li><a id="alpha-z" href="#" class="">z</a></li>
                                                </ul>
                                            </div>
                                            <div>
                                                <div class="pane-title">Vendors</div>
                                                <div class="all-link"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/AllVendors.aspx">View All</a></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="vendorTopDown" href="#" class="">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="vendorTopUp" class="deactivate" href="#">Up</a></div>
                                            </div>
                                            <div class="pane-container">
                                                <div class="pane-menu">
                                                    <ul>
                                                        <li><a class="boutique-highlight" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/Boutiques.aspx?&amp;t=vTab" id="vb">Boutiques
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288110840&amp;t=vTab" id="4_4288110840PP1">3M
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294811954&amp;t=vTab" id="4_4294811954PP1">Acco
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294930432&amp;t=vTab" id="4_4294930432PP1">Acer
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294723805&amp;t=vTab" id="4_4294723805PP1">Adaptec
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288135093&amp;t=vTab" id="4_4288135093PP1">Adcs
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294967215&amp;t=vTab" id="4_4294967215PP1">Adobe
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294782516&amp;t=vTab" id="4_4294782516PP1">Agfaphoto
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288135088&amp;t=vTab" id="4_4288135088PP1">Ako
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294862670&amp;t=vTab" id="4_4294862670PP1">Allied Telesis
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="vendorBottomDown" href="#" class="">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="vendorBottomUp" class="deactivate" href="#">Up</a></div>
                                            </div>
                                        </div>
                                        <div class="category-menu hide" id="vendors-category-menu">
                                            <div class="pane-title">Product Categories</div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="catvendorTopDown" href="#" class="">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="catvendorTopUp" class="deactivate" href="#">Up</a></div>
                                            </div>
                                            <div class="pane-container">
                                                <div class="pane-menu"></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="catvendorBottomDown" href="#" class="">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="catvendorBottomUp" class="deactivate" href="#">Up</a></div>
                                            </div>
                                        </div>
                                        <div class="subcategory-menu hide" id="vendors-subcategory-menu">
                                            <div class="pane-title">Sub-Categories</div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="subcatvendorTopDown" href="#" class="">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="subcatvendorTopUp" class="deactivate" href="#">Up</a></div>
                                            </div>
                                            <div class="pane-container">
                                                <div class="pane-menu"></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="subcatvendorBottomDown" href="#" class="">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="subcatvendorBottomUp" class="deactivate" href="#">Up</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="Div1">
                                <div id="cloud-menu-container" class="mega-menu-container" style="position: absolute; left: 175.85px;">
                                    <div class="primary-menu" id="primary-clouds-menu">
                                        <div class="inner-spacing">
                                            <div style="margin-left: 5px; margin-top: 5px" class="pane-menu"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/Education.aspx" style="width: 90px;" id="EducationBtn">Education<span style="position: absolute; left: 90px;"></span></a></div>
                                            <div>
                                                <div class="pane-title" style="clear: both; margin-top: 5px">Categories</div>
                                                <div class="all-link"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:244">View All</a></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a href="#" id="A2">Down</a></div>
                                                <div class="scroll-control scroll-up"><a href="#" id="A3" class="deactivate">Up</a></div>
                                            </div>

                                            <div class="pane-container" id="primary-menu-Cloudpage-container" style="clear: both;">
                                                <div class="pane-menu">
                                                    <ul id="Ul1">

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288181888&amp;t=pTab" id="A4">Infrastructure-As-A-Service
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288259282&amp;t=pTab" id="A5">Service Provider Licenties
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                        <li><a class="showArrow" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288181911&amp;t=pTab" id="A6">Software-As-A-Service
                                                            <div class="menu-arrow-link"></div>
                                                        </a></li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a href="#" id="A7">Down</a></div>
                                                <div class="scroll-control scroll-up"><a href="#" id="A8" class="deactivate">Up</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="secondary-menu hide" id="cloud-secondary-menu">
                                        <div class="inner-spacing">
                                            <div class="pane-title">Sub-Categories</div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="A9" href="#">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="A10" href="#" class="deactivate">Up</a></div>
                                            </div>
                                            <div class="pane-container">
                                                <div class="pane-menu"></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a href="#" id="A11">Down</a></div>
                                                <div class="scroll-control scroll-up"><a href="#" id="A12" class="deactivate">Up</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vendor-menu hide" id="clouds-vendor-menu">
                                        <div class="inner-spacing">
                                            <div class="pane-title">Vendors</div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="A13" href="#">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="A14" href="#" class="deactivate">Up</a></div>
                                            </div>
                                            <div class="pane-container">
                                                <div class="pane-menu"></div>
                                            </div>
                                            <div class="arrow-container">
                                                <div class="scroll-control scroll-down"><a id="A15" href="#">Down</a></div>
                                                <div class="scroll-control scroll-up"><a id="A16" href="#" class="deactivate">Up</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="search" id="qtp_search2">

                                <div class="quick-tab" id="quick-search2">
                                    <img style="display: none;" src="../ScriptsImages/New_Star.png" id="ctl00_PlaceHolderSearchBar_imSearchStar" class="new-star">
                                    Quick Links<img class="quick-arrow" alt="" src="../ScriptsImages/quick-up-arrow.png">
                                </div>


                                <div class="search-content" id="searchOptions">
                                    <div>
                                        <div class="live-search-container" id="search-field-container">
                                            <input value="Search by Keyword(s), VPN or IM SKU" style="color: rgb(175, 175, 175);" id="searchBox" class="search-text placeholder" rel="Search by Keyword(s), VPN or IM SKU" autocomplete="off" type="text">
                                            <div id="live-search">
                                            </div>
                                        </div>
                                        <a href="#" class="search-submit tooltip-clickable" id="global-search-submit-anchor" rel="global-search-tooltip">Search:</a>
                                        <a href="#" class="search-tips tooltip-text-search" style="cursor: help;" id="search-tips-anchor"></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="hide" id="global-search-tooltip-info">
                                <p>Search</p>
                            </div>

                        </div>
                        <div style="height: 149px;" id="dashboardContainer" class="float-left w237 h100pct ">

                            <style type="text/css">
                                .modalBackground
                                {
                                    background-color: Gray;
                                    filter: alpha(opacity=70);
                                    opacity: 0.7;
                                    z-index: 10000;
                                }

                                .modalPopup
                                {
                                    background-color: White;
                                    z-index: 10001;
                                }
                            </style>


                            <div style="height: 144px;" class="dashboard end-customer-mode" id="loggedInDashboard">
                                <div class="dashboard-inner-container">
                                    <div style="height: 136px;" class="inner-border">
                                        <div class="dashboard-top ">

                                            <div id="ctl00_imDashboard_panMiniCart">

                                                <input name="ctl00$imDashboard$hdnSelectedShopperList" id="ctl00_imDashboard_hdnSelectedShopperList" type="hidden">
                                                <input name="ctl00$imDashboard$hdnPostbackToChangeShopperList" id="ctl00_imDashboard_hdnPostbackToChangeShopperList" type="hidden">

                                                <input name="ctl00$imDashboard$hdnAddressId" id="ctl00_imDashboard_hdnAddressId" type="hidden">
                                                <input name="ctl00$imDashboard$HiddenAddressId" id="ctl00_imDashboard_HiddenAddressId" type="hidden">
                                                <input name="ctl00$imDashboard$HiddenCnpjNumber" id="ctl00_imDashboard_HiddenCnpjNumber" type="hidden">
                                                <input name="ctl00$imDashboard$HiddenCpfNumber" id="ctl00_imDashboard_HiddenCpfNumber" type="hidden">


                                                <div id="qtp_activeBasketContainer" class="active-basket-container">
                                                    <div class="active-basket">
                                                        <div class="items-in-cart">
                                                            0
                                                        </div>
                                                        <div class="basket-name-value">
                                                            <a id="ctl00_imDashboard_hypBasketName" class="basket" onclick="getClickTime(event);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/BasketDetails.aspx">MyBasket</a><br>
                                                            Value
                                        &nbsp; 
                : <span class="cart-total">€ 0,00
                </span>
                                                            <span class="cart-excl-tax">Excl btw</span>
                                                        </div>
                                                    </div>
                                                    <div id="ctl00_imDashboard_pnlBasketOptions" class="basket-options">


                                                        <a id="ctl00_imDashboard_lbnCheckOut" class="small-red-btn checkout-btn" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/BasketDetails.aspx">Basket</a>
                                                        <div id="ctl00_imDashboard_pnlLbnCheckoutRight" class="small-red-btn-right">
                                                        </div>
                                                        <a href="#" class="change-basket" id="baskets-link">
                                                            <img alt="" src="../ScriptsImages/arrow_down_dashboard.gif" id="baskets-dropdown">
                                                            Change Baskets</a>
                                                        <div class="clear">
                                                        </div>

                                                    </div>
                                                </div>
                                                <div id="active-baskets-container"></div>
                                            </div>
                                        </div>
                                        <div class="dashboard-bottom">
                                        </div>

                                        <div id="quickSearch-accordion" class="h57pct">
                                            <h3 class="header">Quick Order Search<a id="ctl00_imDashboard_imQuickOrderSearchControl_DimValGoButton" class="small-link" href='javascript:WebForm_DoPostBackWithOptions(new%20WebForm_PostBackOptions("ctl00$imDashboard$imQuickOrderSearchControl$DimValGoButton",%20"",%20true,%20"",%20"",%20false,%20true))'>View All</a>
                                            </h3>
                                            <div class="content-pane h80pct">
                                                <div class="order-search-dashboard h92pct">
                                                    <div class="order-search-dashboard">
                                                        <div onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_imDashboard_imQuickOrderSearchControl_btnSearchByNumber')">

                                                            <div class="item-entry-container">
                                                                <select name="ctl00$imDashboard$imQuickOrderSearchControl$ddlSearchBy" id="ctl00_imDashboard_imQuickOrderSearchControl_ddlSearchBy" class="quickOrderSearchSelect" style="font-size: X-Small; width: 130px;">
                                                                    <option selected="selected" value="ResellerPONumber">Reseller PO Number</option>
                                                                    <option value="IngramMicroOrderNumber">IM Order Number</option>
                                                                    <option value="EndUserPONumber">End Customer PO Number</option>

                                                                </select>
                                                                <input name="ctl00$imDashboard$imQuickOrderSearchControl$txtSearchByNumberInput" id="ctl00_imDashboard_imQuickOrderSearchControl_txtSearchByNumberInput" class="number" onkeypress="CancelDefaultEvent(event);" style="font-size: X-Small; width: 127px;" type="text">
                                                                <a onclick="return ValidateSearchValue();" id="ctl00_imDashboard_imQuickOrderSearchControl_btnSearchByNumber" class="search-btn quick-search-btn tooltip-clickable" rel="global-search-tooltip" href='javascript:WebForm_DoPostBackWithOptions(new%20WebForm_PostBackOptions("ctl00$imDashboard$imQuickOrderSearchControl$btnSearchByNumber",%20"",%20true,%20"",%20"",%20false,%20true))'>Search</a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>

                    <style type="text/css">
                        .modalBackground
                        {
                            background-color: Gray;
                            filter: alpha(opacity=70);
                            opacity: 0.7;
                        }

                        .modal-popup
                        {
                            background-color: White;
                        }
                    </style>

                    <div id="quick_links">
                        <div class="quick-links">

                            <a id="ctl00_MyLinks1_rptrLinks_ctl00_lnkQK" class=" blue-background DisplayPreferences" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="javascript:void(0);"><b><span>Display Preferences</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl01_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl02_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl03_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl04_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl05_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl06_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl07_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl08_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_rptrLinks_ctl09_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                            <a id="ctl00_MyLinks1_HyperLink3" class="settings" href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx">
                                <img src="../ScriptsImages/New_Star.png" id="ctl00_MyLinks1_Img12" class="new-star"><span>Settings</span>
                            </a>
                        </div>
                    </div>

                    <div id="container">
                        <a name="begin-content" id="qtp_begin"></a>
                        <div id="main-content">
                            <div style="position: relative;" id="ctl00_divMainBlock" class="main-block">
                                <div class="breadcrumb">
                                    <div class="text">
                                        <div id="breadcrumb-container">
                                            <div id="breadcrumb"><a href="#/Pages/default.aspx" class="breadcrumb-a" id="cCrumb0#100">Home</a>                   &nbsp;&gt;&nbsp;                                 <a href="#/_layouts/CommerceServer/IM/search2.aspx#PNavDS=Np:All" class="breadcrumb-a" id="cCrumb1#100">Products &amp; Services</a>  </div>
                                        </div>
                                    </div>
                                    <div class="links" id="page-action-links-container"><a class="email-icon-link" href="#">Email<span></span></a> <span class="link-divider">|</span>  <a id="aPrint" href="#" class="print-icon-link">Print<span></span></a>   </div>
                                    <div class="filters" id="search-filters-container"><span>You Searched For </span><span class="bold">test</span>              <span class="remove-facet">[<a href="#0" class="breadcrumb-a" id="cCrumb0#0">x</a>]</span> <span>Why Not&nbsp;<a class="save-search-icon-link" href="#">Save Search</a>?  </span></div>
                                </div>

                                <div style="min-height: 500px;" class="products-search">
                                    <a name="top"></a>
                                    <div class="content">
                                        <div id="recently-viewed-header"></div>
                                        <div id="boutique-header"></div>

                                        <div class="left-col">

                                            <div style="display: block;" class="blue-header hide" id="search-header-container">
                                                <h2 class="product-amt-header">155 Results</h2>
                                            </div>

                                            <div id="myaccount-navigation-menu"></div>
                                            <div id="search-facets">
                                                <div id="search-within-result-facet">
                                                    <div class="left-col-block facet-block within-results">
                                                        <div class="header grey-header active">
                                                            <div class="arrow-container"></div>
                                                            <h3>Search within Results</h3>
                                                            <a href="#" rel="search-within-tips" class="search-tips search-within-result-tip">
                                                                <img src="../ScriptsImages/information-sm.gif" alt="" class="standard-tooltip"></a>
                                                        </div>
                                                        <div style="display: block;" class="content">
                                                            <div class="spacing">
                                                                <input class="search-text-box" id="txtSearchWithinResult" maxlength="100" type="text">
                                                                <div class="hide" id="search-within-tips">
                                                                    <p></p>
                                                                    <h3>Search Tips</h3>
                                                                    <p>Refine search results by keywords, VPN, SKU or EAN</p>
                                                                    <p>Separate all keywords and phrases with a space</p>
                                                                    <p></p>
                                                                </div>
                                                                <a href="#" rel="global-search-tooltip" class="search-btn tooltip-clickable open-price go">Search</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="bid-reference-facet"></div>
                                                <div id="bid-end-user-facet"></div>
                                                <div id="bid-end-customer-facet"></div>
                                                <div id="product-categories-facet">
                                                    <div class="left-col-block facet-block refine-categories">
                                                        <div class="header grey-header active">
                                                            <div class="arrow-container"></div>
                                                            <h3>Categories</h3>
                                                        </div>
                                                        <div style="display: block;" class="content">
                                                            <a class="refine-categories-link" href="#4288449839">Audio/Visual (98)</a>          <a class="refine-categories-link" href="#4288450096">Networking (21)</a>          <a class="refine-categories-link" href="#4294967214">Software (18)</a>          <a class="refine-categories-link" href="#4294967007">Services &amp; Warranties (7)</a>          <a class="refine-categories-link" href="#4294954486">Communications (3)</a>          <a class="refine-categories-link" href="#4288449895">Cameras &amp; Scanners (2)</a>          <a class="refine-categories-link" href="#4294967244">Cables (1)</a>          <a class="refine-categories-link" href="#4294637804">Components (1)</a>
                                                            <div class="facet-actions" id="refinementActionLinks"><a href="#" class="see-all open-all-categories">View All Categories</a>          </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div id="product-vendors-facet">
                                                    <div class="left-col-block refine-vendors facet-block" id="vendors-facet">
                                                        <div class="header grey-header active" id="vendors-facet-header">
                                                            <div class="arrow-container"></div>
                                                            <h3>Vendors</h3>
                                                        </div>
                                                        <div style="display: block;" class="content" id="vendors-content">
                                                            <input id="chk-MILESTONE" class="non-pre-selected-refinement" onclick="handleFacetCheckboxClick(this, 'lnkActionButton')" name="4288133516" type="checkbox">
                                                            <label for="4288133516" id="lbl-MILESTONE"><a id="lnk-MILESTONE" class="refine-vendor-link" href="#4288133516">MILESTONE (98)</a>         </label>
                                                            <div class="clear"></div>
                                                            <input id="chk-MICROSOFT" class="non-pre-selected-refinement" onclick="    handleFacetCheckboxClick(this, 'lnkActionButton')" name="4294963152" type="checkbox">
                                                            <label for="4294963152" id="lbl-MICROSOFT"><a id="lnk-MICROSOFT" class="refine-vendor-link" href="#4294963152">MICROSOFT (19)</a>         </label>
                                                            <div class="clear"></div>
                                                            <input id="chk-POLYCOM" class="non-pre-selected-refinement" onclick="    handleFacetCheckboxClick(this, 'lnkActionButton')" name="4288806912" type="checkbox">
                                                            <label for="4288806912" id="lbl-POLYCOM"><a id="lnk-POLYCOM" class="refine-vendor-link" href="#4288806912">POLYCOM (8)</a>         </label>
                                                            <div class="clear"></div>
                                                            <input id="chk-STARTECH" class="non-pre-selected-refinement" onclick="    handleFacetCheckboxClick(this, 'lnkActionButton')" name="4294801957" type="checkbox">
                                                            <label for="4294801957" id="lbl-STARTECH"><a id="lnk-STARTECH" class="refine-vendor-link" href="#4294801957">STARTECH (5)</a>         </label>
                                                            <div class="clear"></div>
                                                            <input id="chk-CASE LOGIC" class="non-pre-selected-refinement" onclick="    handleFacetCheckboxClick(this, 'lnkActionButton')" name="4294863091" type="checkbox">
                                                            <label for="4294863091" id="lbl-CASE LOGIC"><a id="lnk-CASE LOGIC" class="refine-vendor-link" href="#4294863091">CASE LOGIC (4)</a>         </label>
                                                            <div class="clear"></div>
                                                            <input id="chk-APC - SCHNEIDER" class="non-pre-selected-refinement" onclick="    handleFacetCheckboxClick(this, 'lnkActionButton')" name="4289049427" type="checkbox">
                                                            <label for="4289049427" id="lbl-APC - SCHNEIDER"><a id="lnk-APC - SCHNEIDER" class="refine-vendor-link" href="#4289049427">APC - SCHNEIDER (3)</a>         </label>
                                                            <div class="clear"></div>
                                                            <input id="chk-ELO TOUCH SOLUTIONS" class="non-pre-selected-refinement" onclick="    handleFacetCheckboxClick(this, 'lnkActionButton')" name="4288152650" type="checkbox">
                                                            <label for="4288152650" id="lbl-ELO TOUCH SOLUTIONS"><a id="lnk-ELO TOUCH SOLUTIONS" class="refine-vendor-link" href="#4288152650">ELO TOUCH SOLUTIONS (2)</a>         </label>
                                                            <div class="clear"></div>
                                                            <input id="chk-KODAK" class="non-pre-selected-refinement" onclick="    handleFacetCheckboxClick(this, 'lnkActionButton')" name="4294846948" type="checkbox">
                                                            <label for="4294846948" id="lbl-KODAK"><a id="lnk-KODAK" class="refine-vendor-link" href="#4294846948">KODAK (2)</a>         </label>
                                                            <div class="clear"></div>
                                                            <div class="facet-actions" id="vendorFacetActionLinks"><a id="lnkActionButton" href="javascript:doMutlipleRefinementsSearch('refine-vendor-link')" class="search-btn tooltip-clickable open-price" rel="global-search-tooltip">Search</a>           <a id="lnkSeeAll" href="#" class="see-all">View All Vendors</a>                  </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div id="service-providers-facet"></div>
                                                <div id="price-range-facet">
                                                    <div class="left-col-block price-range facet-block">
                                                        <div class="header grey-header active">
                                                            <div class="arrow-container"></div>
                                                            <h3>Price</h3>
                                                        </div>
                                                        <div style="display: block;" class="content">
                                                            <div class="spacing">
                                                                <span class="currency-sign">€</span>
                                                                <input id="txtPriceRangeMin" onblur="showInputPrompt(this, 'Min');" onfocus="hideInputPrompt(this, 'Min');" value="Min" class="promptdimmed" type="text">
                                                                <span>-</span>
                                                                <input id="txtPriceRangeMax" onblur="showInputPrompt(this, 'Max');" onfocus="hideInputPrompt(this, 'Max');" value="Max" class="promptdimmed" type="text">
                                                                <a href="javascript:doPriceRangeSearch()" class="search-btn tooltip-clickable open-price" rel="global-search-tooltip">Search</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="product-status-facet">
                                                    <div class="left-col-block product-status facet-block" id="status-facet">
                                                        <div class="header grey-header active" id="qtp_showHideFacetTrigger">
                                                            <div class="arrow-container"></div>
                                                            <h3>Status</h3>
                                                        </div>
                                                        <div style="display: block;" class="content" id="status_content">
                                                            <div class="status-div">
                                                                <input id="qtp_SelectedStatusValuesList3" class="non-pre-selected-refinement in-stock-status stock-checkbox" name="P^202" onclick="    HandleTemplateStatus(this, 'qtp_facet_actions_btn');" type="checkbox">
                                                                <label for="P^202" class="checkbox-label in-stock-label" id="qtp_MinimumInStockQtyBoxSel3">In Stock  (24)</label>
                                                                <div class="min-stock">
                                                                    <label for="minStockTextBox">Min.</label>
                                                                    <input id="minStockTextBox" type="text">
                                                                    <div class="clear">&nbsp;</div>
                                                                </div>
                                                                <div class="clear">&nbsp;</div>
                                                            </div>
                                                            <div class="status-div">
                                                                <input id="qtp_SelectedStatusValuesList4" class="non-pre-selected-refinement in-stock-or-on-order-status" name="P^208" onclick="    HandleTemplateStatus(this, 'qtp_facet_actions_btn');" type="checkbox">
                                                                <label for="P^208" class="checkbox-label" id="qtp_MinimumInStockQtyBoxSel4">In Stock or On Order (24)</label>
                                                            </div>
                                                            <div class="status-div">
                                                                <input id="Checkbox1" class="non-pre-selected-refinement" name="P^224" onclick="    HandleTemplateStatus(this, 'qtp_facet_actions_btn');" type="checkbox">
                                                                <label for="P^224" class="checkbox-label" id="Label1">License Products (13)</label>
                                                            </div>
                                                            <div class="status-div">
                                                                <input id="Checkbox2" class="non-pre-selected-refinement" name="P^181" onclick="    HandleTemplateStatus(this, 'qtp_facet_actions_btn');" type="checkbox">
                                                                <label for="P^181" class="checkbox-label" id="Label2">Direct Ship (1)</label>
                                                            </div>
                                                            <div class="status-div">
                                                                <input id="Checkbox3" class="non-pre-selected-refinement" name="P^179" onclick="    HandleTemplateStatus(this, 'qtp_facet_actions_btn');" type="checkbox">
                                                                <label for="P^179" class="checkbox-label" id="Label3">Discontinued (1)</label>
                                                            </div>
                                                            <div class="status-div">
                                                                <input id="Checkbox4" class="non-pre-selected-refinement" name="P^270" onclick="    HandleTemplateStatus(this, 'qtp_facet_actions_btn');" type="checkbox">
                                                                <label for="P^270" class="checkbox-label" id="Label4">Pre-Order (1)</label>
                                                            </div>
                                                            <div class="facet-actions" id="statusFacetActionLinks"><a id="qtp_facet_actions_btn" href="javascript:doMutlipleRefinementsSearch('refine-status-link')" class="search-btn tooltip-clickable open-price" rel="global-search-tooltip">Search</a>           </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="language-codes-facet"></div>
                                                <div id="tech-specs-facet">
                                                    <div class="left-col-block tech-chars facet-block">
                                                        <div class="header grey-header active">
                                                            <div class="arrow-container"></div>
                                                            <h3>Tech Specs</h3>
                                                        </div>
                                                        <div style="display: block;" class="content">
                                                            <label>Product Type (71)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288323865">Cable Analyzer (1)</option>
                                                                <option value="4288812118">Cable Panel (1)</option>
                                                                <option value="4288831294">Camera Strap (1)</option>
                                                                <option value="4288831631">Carrying Case (4)</option>
                                                                <option value="4288831427">Data Transfer Adapter (1)</option>
                                                                <option value="4288812129">Fan Tray (1)</option>
                                                                <option value="4288521332">Handheld Barcode Scanner (1)</option>
                                                                <option value="4288831613">Print Server (1)</option>
                                                                <option value="4288830822">Printed Circuit Board Assembly (PCBA) (1)</option>
                                                                <option value="4288830808">Printer Cutter (1)</option>
                                                                <option value="4288725500">Probe (1)</option>
                                                                <option value="4288831560">Projection Screen (34)</option>
                                                                <option value="4288831579">Service (1)</option>
                                                                <option value="4288830656">Software (3)</option>
                                                                <option value="4288831591">Software Licensing (14)</option>
                                                                <option value="4288830701">System Diagnostic Device (1)</option>
                                                                <option value="4288831546">Upgrade Kit (1)</option>
                                                                <option value="4288831615">Video Cable (1)</option>
                                                                <option value="4288450266">Video Testing Device (2)</option>
                                                            </select>
                                                            <label>Product Family (35)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288516394">Descender Electrol (8)</option>
                                                                <option value="4288516401">Descender RF Electrol (8)</option>
                                                                <option value="4288832494">DS3508 (1)</option>
                                                                <option value="4288516407">Elpro Electrol (9)</option>
                                                                <option value="4288565244">Elpro RF Electrol (9)</option>
                                                            </select>
                                                            <label>Screen Fabric (34)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288830200">Matte White (34)</option>
                                                            </select>
                                                            <label>Screen Folding Mechanism (34)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288830177">Electric (34)</option>
                                                            </select>
                                                            <label>Screen Height (34)</label>
                                                            <select id="techSpec4Mod" class="modifier-dropdown">
                                                                <option selected="selected" value="eq">=</option>
                                                                <option value="lt">&lt;=</option>
                                                                <option value="gt">&gt;=</option>
                                                            </select>
                                                            <select id="techSpec4" class="short-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288112022:rpScreenHeight#rpScreenHeight:1600.2:Screen Height:160 cm (63&quot;)">160 cm (63") (2)</option>
                                                                <option value="4288112034:rpScreenHeight#rpScreenHeight:1800:Screen Height:180 cm (70.9&quot;)">180 cm (70.9") (4)</option>
                                                                <option value="4288112047:rpScreenHeight#rpScreenHeight:2000:Screen Height:200 cm (78.7&quot;)">200 cm (78.7") (4)</option>
                                                                <option value="4288112058:rpScreenHeight#rpScreenHeight:2200:Screen Height:220 cm (86.6&quot;)">220 cm (86.6") (4)</option>
                                                                <option value="4288112071:rpScreenHeight#rpScreenHeight:2399.79:Screen Height:240 cm (94.5&quot;)">240 cm (94.5") (4)</option>
                                                                <option value="4288112091:rpScreenHeight#rpScreenHeight:2800:Screen Height:280 cm (110.2&quot;)">280 cm (110.2") (4)</option>
                                                                <option value="4288112099:rpScreenHeight#rpScreenHeight:3000:Screen Height:300 cm (118.1&quot;)">300 cm (118.1") (4)</option>
                                                                <option value="4288112108:rpScreenHeight#rpScreenHeight:3200.4:Screen Height:320 cm (126&quot;)">320 cm (126") (4)</option>
                                                                <option value="4288112114:rpScreenHeight#rpScreenHeight:3400:Screen Height:340 cm (133.9&quot;)">340 cm (133.9") (4)</option>
                                                            </select>
                                                            <label>Diagonal Image Size (34)</label>
                                                            <select id="techSpec5Mod" class="modifier-dropdown">
                                                                <option selected="selected" value="eq">=</option>
                                                                <option value="lt">&lt;=</option>
                                                                <option value="gt">&gt;=</option>
                                                            </select>
                                                            <select id="techSpec5" class="short-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288516473:rpDiagonalImageSize#rpDiagonalImageSize:2210:Diagonal Image Size:221 cm (87&quot;)">221 cm (87") (2)</option>
                                                                <option value="4288255842:rpDiagonalImageSize#rpDiagonalImageSize:2520:Diagonal Image Size:252 cm (99.2&quot;)">252 cm (99.2") (4)</option>
                                                                <option value="4288130017:rpDiagonalImageSize#rpDiagonalImageSize:2829.56:Diagonal Image Size:283 cm (111.4&quot;)">283 cm (111.4") (4)</option>
                                                                <option value="4288255887:rpDiagonalImageSize#rpDiagonalImageSize:3108.96:Diagonal Image Size:310.9 cm (122.4&quot;)">310.9 cm (122.4") (4)</option>
                                                                <option value="4288129947:rpDiagonalImageSize#rpDiagonalImageSize:3390.9:Diagonal Image Size:339.1 cm (133.5&quot;)">339.1 cm (133.5") (4)</option>
                                                                <option value="4288130018:rpDiagonalImageSize#rpDiagonalImageSize:3959.8:Diagonal Image Size:396 cm (155.9&quot;)">396 cm (155.9") (4)</option>
                                                                <option value="4288130019:rpDiagonalImageSize#rpDiagonalImageSize:4239.26:Diagonal Image Size:423.9 cm (166.9&quot;)">423.9 cm (166.9") (4)</option>
                                                                <option value="4288129948:rpDiagonalImageSize#rpDiagonalImageSize:4528.82:Diagonal Image Size:452.9 cm (178.3&quot;)">452.9 cm (178.3") (4)</option>
                                                                <option value="4288130020:rpDiagonalImageSize#rpDiagonalImageSize:4810.76:Diagonal Image Size:481.1 cm (189.4&quot;)">481.1 cm (189.4") (4)</option>
                                                            </select>
                                                            <label>Aspect Ratio (34)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288841054">1:1 (34)</option>
                                                            </select>
                                                            <label>Screen Width (34)</label>
                                                            <select id="techSpec7Mod" class="modifier-dropdown">
                                                                <option selected="selected" value="eq">=</option>
                                                                <option value="lt">&lt;=</option>
                                                                <option value="gt">&gt;=</option>
                                                            </select>
                                                            <select id="techSpec7" class="short-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288111809:rpScreenWidth#rpScreenWidth:1600.2:Screen Width:160 cm (63&quot;)">160 cm (63") (2)</option>
                                                                <option value="4288111819:rpScreenWidth#rpScreenWidth:1800:Screen Width:180 cm (70.9&quot;)">180 cm (70.9") (4)</option>
                                                                <option value="4288111827:rpScreenWidth#rpScreenWidth:2000:Screen Width:200 cm (78.7&quot;)">200 cm (78.7") (4)</option>
                                                                <option value="4288111834:rpScreenWidth#rpScreenWidth:2200:Screen Width:220 cm (86.6&quot;)">220 cm (86.6") (4)</option>
                                                                <option value="4288111841:rpScreenWidth#rpScreenWidth:2399.79:Screen Width:240 cm (94.5&quot;)">240 cm (94.5") (4)</option>
                                                                <option value="4288111853:rpScreenWidth#rpScreenWidth:2800:Screen Width:280 cm (110.2&quot;)">280 cm (110.2") (4)</option>
                                                                <option value="4288111858:rpScreenWidth#rpScreenWidth:3000:Screen Width:300 cm (118.1&quot;)">300 cm (118.1") (4)</option>
                                                                <option value="4288111864:rpScreenWidth#rpScreenWidth:3200.4:Screen Width:320 cm (126&quot;)">320 cm (126") (4)</option>
                                                                <option value="4288111867:rpScreenWidth#rpScreenWidth:3400:Screen Width:340 cm (133.9&quot;)">340 cm (133.9") (4)</option>
                                                            </select>
                                                            <div class="facet-actions" id="techSpecsActionLinks"><a href="#" class="see-all">View All Tech Specs</a>         </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div id="license-specs-facet">
                                                    <div class="left-col-block tech-chars facet-block">
                                                        <div class="header grey-header active">
                                                            <div class="arrow-container"></div>
                                                            <h3>License Selector</h3>
                                                        </div>
                                                        <div style="display: block;" class="content">
                                                            <label><span class="dimnam">End User Type</span> (10)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288663253">Corporate (8)</option>
                                                                <option value="4288663255">Government (2)</option>
                                                            </select>
                                                            <label><span class="dimnam">Offering</span> (10)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288663141">Open 500+ (4)</option>
                                                                <option value="4288663203">Open Business (6)</option>
                                                            </select>
                                                            <label><span class="dimnam">Product Line</span> (10)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288662521">Visual Studio Test Pro w/MSDN (10)</option>
                                                            </select>
                                                            <label><span class="dimnam">Platform</span> (10)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288253193">Non-Specific Each (10)</option>
                                                            </select>
                                                            <label><span class="dimnam">Type of License</span> (10)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288663187">License/Software Assurance (5)</option>
                                                                <option value="4288663199">Software Assurance (5)</option>
                                                            </select>
                                                            <label><span class="dimnam">Maintenance Period</span> (10)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288663198">24 Months (10)</option>
                                                            </select>
                                                            <label><span class="dimnam">Edition</span> (10)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288253123">MPN Competency Required (4)</option>
                                                                <option value="4288253175">w/MSDN (6)</option>
                                                            </select>
                                                            <label><span class="dimnam">License Language</span> (8)</label>
                                                            <select class="long-spec-dropdown">
                                                                <option selected="selected" value="">All</option>
                                                                <option value="4288663088">All Languages (8)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div id="service-template-facet"></div>
                                                <div id="vendor-authorization-facet"></div>
                                            </div>
                                            <div id="recentlyviewed-products-container">
                                                <div class="left-col-block recent-history-vertical">
                                                    <div class="blue-header">
                                                        <h2>Recent Viewing History         </h2>
                                                    </div>
                                                    <div class="content">
                                                        <div class="see-all">
                                                            <div class="inner-spacing"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/RecentlyViewedProducts.aspx" class="red-arrow-link">View All</a>                           </div>
                                                        </div>
                                                        <div class="compare-container">
                                                            <div class="compare"><a class="small-red-btn tooltip-clickable clsCompare" rel="#recview_cmp" href="#">Compare<span class="small-red-btn-right"></span></a>             </div>
                                                            <div class="hide" id="recview_cmp">
                                                                <p class="msg">Select up to 4 products</p>
                                                            </div>
                                                        </div>
                                                        <div class="border">
                                                            <!--IE6-->
                                                        </div>
                                                        <table>
                                                            <!-- Add products here-->
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="recentview">
                                                                            <div class="inner-spacing">
                                                                                <div class="compare-checkbox">
                                                                                    <span class="prod-compare-checkbox">
                                                                                        <input onclick="javascript: handleRecentlyViewedCheckboxClick(this, 'a.clsCompare')" value="NL01@@4500@@10@@000000000001619911" type="checkbox">
                                                                                    </span>
                                                                                </div>
                                                                                <div class="product">
                                                                                    <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001619911">
                                                                                        <img src="../ScriptsImages/1020007293.jpg"  width="40"></a>
                                                                                    <p class="product-name" id="product-title-0"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001619911" rel="#rvp-product-title-0-info" class="tooltip-clickable"><span class="italic bold">STARTECH</span>&nbsp;MINI DISPLAYPORT TO DVI ACTIVE ADAPTER</a>                 </p>
                                                                                    <div class="in-stock-details" id="rvp-product-title-0-info">
                                                                                        <table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>StarTech.com MDP2DVIS Video Cable for Monitor, 
Audio/Video Device, TV - Mini DisplayPort Male Digital Audio/Video - 
DVI-D (Single-Link) Female Digital Video - Black                        
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Stock Display Start -->

                                                                                <div class="in-stock-container" id="rvp-stock-000000000001619911">
                                                                                    <div id="in-stock-000000000001619911" class="in-stock tooltip">
                                                                                        <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>
                                                                                        <span>44 In Stock
                                                                                            <br>
                                                                                        </span>
                                                                                    </div>
                                                                                    <div id="inline-stock-000000000001619911">
                                                                                        <div class="plantDetails">
                                                                                            <p><span>44</span><span>&nbsp;Lokaal</span> </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Stock Display End -->
                                                                                <p class="vpn-num">VPN: MDP2DVIS</p>
                                                                                <div class="pricing">
                                                                                    <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001619911" type="hidden">
                                                                                    <div class="pricing-info" id="rvp-price-000000000001619911">
                                                                                        <span class="your-price"><span class="resprice">€ 21,15</span><span class="excl-tax excltax">   Excl btw</span></span>
                                                                                    </div>
                                                                                    <div id="prod-fees-000000000001619911"></div>
                                                                                    <div class="centered-btns-container">
                                                                                        <div class="add-product centered-btns" id="rvp-addtobasket-000000000001619911">
                                                                                            <input class="amount" value="1" type="text">
                                                                                            <input value="000000000001619911" class="hidden-sku" type="hidden">
                                                                                            <input value="1619911" class="wt-hidden-sku" type="hidden">
                                                                                            <input value="€ 21,15" class="hidden-price" type="hidden">
                                                                                            <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                            <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                            <input class="hdnClassCode" value="" type="hidden">
                                                                                            <input class="hdnLocalStock" value="44" type="hidden">
                                                                                            <input class="hdnCurrentStock" value="44" type="hidden">
                                                                                            <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                            <span id="rvp-addtobasket-000000000001619911-ctrlwh">
                                                                                                <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                            </span>
                                                                                            <div id="rvp-addtobasket-000000000001619911-ctrlwh-info" style="visibility: hidden" class="in-stock-details">
                                                                                                <p></p>
                                                                                            </div>
                                                                                            <input value="" class="hidden-count-item-000000000001619911" type="hidden">
                                                                                            <div id="icon-tool-tip-000000000001619911" class="in-cart-msg">
                                                                                                <a href="#" rel="rvp-addtobasket-000000000001619911-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                                <div id="rvp-addtobasket-000000000001619911-Icon-info" class="hide">
                                                                                                    <p>There are&nbsp;&nbsp;units of this item in your active basket.</p>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div id="add-to-basket-msg-000000000001619911" class="in-cart-msg" style="display: none;">
                                                                                                <a href="#" rel="rvp-addtobasket-000000000001619911-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                                <div id="rvp-addtobasket-000000000001619911-Msg-info" class="hide add-to-bsk-msg-000000000001619911">
                                                                                                    <p>You have added&nbsp;<span class="totalCountValue-000000000001619911"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001619911"></span>&nbsp;of this item in your active basket.</p>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                </div>
                                                                                <a class="quantity-break product-sku-flag " id="QB-000000000001619911" style="display: none" href="#1619911" rel="#qb-singleproduct-000000000001619911-info">Quantity Breaks                                 </a>
                                                                                <div class="hide" id="qb-singleproduct-000000000001619911-info"></div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="compare-container">
                                                            <div class="compare"><a class="small-red-btn tooltip-clickable clsCompare" rel="#recview_cmp" href="#">Compare<span class="small-red-btn-right"></span></a>                                           </div>
                                                        </div>
                                                        <div class="hide" id="RefurbishedToolTip">
                                                            <p>This is a Refurbished Item   </p>
                                                        </div>
                                                        <div class="hide" id="free-item-info">
                                                            <p>Free Items available , Check the product description to find the details  </p>
                                                        </div>
                                                        <div class="hide" id="preOrderToolTip">
                                                            <p>Dit product is nog niet beschikbaar, maar u kunt vooruit bestellen zodat het verzonden word zodra het leverbaar is.  </p>
                                                        </div>
                                                        <div class="see-all">
                                                            <div class="inner-spacing">
                                                                <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/RecentlyViewedProducts.aspx" class="red-arrow-link">View All</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="hide" id="non-exportable-info-right">
                                                    <p>Based on Ingram Micro's Distribution Rights, <b>this product cannot be exported</b> to   </p>
                                                </div>
                                                <div class="hide" id="blowout-item-info">
                                                    <p>Special Price available until stock lasts  </p>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="right-col">

                                            <div id="redirected-search-msg-container"></div>
                                            <div id="search-adjustments-container"></div>
                                            <div id="did-you-mean-container"></div>
                                            <div id="search-category-menu"></div>
                                            <div id="vendor-ad-container"></div>
                                            <div id="recommended-products-container"></div>
                                            <div style="min-height: 500px;" class="products-search">
                                                <div style="display: block;" class="right-col-content hide">

                                                    <div class="content-spacing">

                                                        <div id="pna-offline-msg-container"></div>
                                                        <div id="search-results-msg-container"></div>
                                                        <div id="search-within-result-msg-container"></div>
                                                        <div id="top-sorting-container"></div>
                                                        <div id="top-paging-container">
                                                            <div class="pager-container">
                                                                <div class="compare"><a class="small-red-btn tooltip-clickable clsCompare" rel="#cmp_lbl" href="#">Compare<span class="small-red-btn-right"></span>                 </a></div>
                                                                <div class="sort-by-left sort-by" id="sortby">
                                                                    <label>Sort results by:</label>
                                                                    <select id="topSortSelect">
                                                                        <option selected="selected" style="background-color: #FFFFFF;" value="0">Relevance</option>
                                                                        <option value="5">Vendor Name, A to Z</option>
                                                                        <option value="6">Vendor Name, Z to A</option>
                                                                        <option value="1">Price, High to Low</option>
                                                                        <option value="2">Price, Low to High</option>
                                                                        <option value="3">Stock, High to Low</option>
                                                                        <option value="4">Stock, Low to High</option>
                                                                        <option value="7">VPN, A to Z</option>
                                                                        <option value="8">VPN, Z to A</option>
                                                                    </select>
                                                                    <input alt="" class="tooltip-clickable" rel="#chkSaveSortBy-info" id="chkSaveSortBy" type="checkbox">
                                                                </div>
                                                                <div class="show-picker">
                                                                    <b>Show:</b>
                                                                    <div class="imageView">
                                                                        <a class="tooltip-clickable" rel="#ListViewtooltip-info" href="#">List</a>  <span class="tooltip" id="ImageViewtooltip">With Images</span>
                                                                        <div id="ImageViewtooltip-info" class="hide">
                                                                            <p>With Images</p>
                                                                        </div>
                                                                        <div id="ListViewtooltip-info" class="hide">
                                                                            <p>List</p>
                                                                        </div>
                                                                        <div id="chkSaveSortBy-info" class="hide">
                                                                            <p>Save your Sorting Preference</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="sort-by-right sort-by" id="perpage">
                                                                    <label>Per page:</label>
                                                                    <select id="topPagingSelect">
                                                                        <option value="10" selected="selected">10</option>
                                                                        <option value="20">20</option>
                                                                        <option value="30">30</option>
                                                                    </select>
                                                                </div>
                                                                <div class="pager"><span class="page-num">1</span>               
                                                                    <a id="next1" class="page-num" href="#2">2</a>             
                                                                     <a id="next2" class="page-num" href="#3">3</a>    
                                                                              <span class="pager-ellipsis">...</span> 
                                                                                  <a id="lastpagelink" class="page-num" href="#16">16</a>    
                                                                              <a id="next" class="pager-button next" href="#2"></a><a id="last" class="page-num" href="#16">Last</a>      
                                                                </div>
                                                                <div class="compare-tt hide" id="cmp_lbl">
                                                                    <p class="msg">Select up to 4 products</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="search-results-container">
                                                            <div class="search-results">
                                                                <div class="single-result">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input onclick="javascript: handleRecentlyViewedCheckboxClick(this, 'a.clsCompare');" value="NL01@@4500@@10@@000000000001619911" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://inquirecontent2.ingrammicro.com/300/1020007293.jpg?noimage=logo" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/1020007293.jpg"  width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="Hidden1" type="hidden">
                                                                                        <div class="product-name">
                                                                                            <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001619911" class="ellipsis-multiline" rel="#product-title-0-info"><b><i>STARTECH</i></b>.com
 MDP2DVIS Video Cable for Monitor, Audio/Video Device, TV - Mini 
DisplayPort Male Digital Audio/Video - DVI-D (Single-Link) Female 
Digital Video - Black</a>
                                                                                        </div>
                                                                                        <div class="hide" id="product-title-0-info">
                                                                                            <p class="w350">
                                                                                                StarTech.com
 MDP2DVIS Video Cable for Monitor, Audio/Video Device, TV - Mini 
DisplayPort Male Digital Audio/Video - DVI-D (Single-Link) Female 
Digital Video - Black
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>MDP2DVIS</span>                                          |                                      EAN: <span>65030842334</span>                                          |                                      SKU: <span>1619911</span>
                                                                                            |                             
         Lang. Code:                                     
    <a rel="language-code-popup" class="ean tooltip" href="#">IN</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001619911" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001619911" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="A18" style="display: none" href="#1619911" rel="#qb-singleproduct-000000000001619911-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="Div2"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <div class="expanded-prod-details-container" id="expanded-0">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001619911">
                                                                                        <div id="Div3" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>44 In Stock
                                                                                            <br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="Div4" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                                        <td>44</td>
                                                                                                        <td>0</td>
                                                                                                        <td></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="Div5">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>44</span><span>&nbsp;Lokaal</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001619911"><span class="your-price"><span class="resprice">€ 21,15</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="Div6"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001619911">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001619911" class="hidden-sku" type="hidden">
                                                                                    <input value="1619911" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 21,15" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="44" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="44" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span id="addtobasket-000000000001619911-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001619911-ctrlwh-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p></p>
                                                                                    </div>
                                                                                    <input value="" class="hidden-count-item-000000000001619911" type="hidden">
                                                                                    <div id="Div7" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001619911-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001619911-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001619911"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="Div8" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001619911-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001619911-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001619911">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001619911"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001619911"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001619911" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result evenColor">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input value="NL01@@4500@@10@@000000000001588959" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://inquirecontent3.ingrammicro.com/300/1019998866.jpg?noimage=logo" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/1019998866.jpg"  width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001588959" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001588959" class="ellipsis-multiline" rel="#product-title-1-info"><b><i>CASE LOGIC</i></b> LAPST-107 Carrying Case (Sleeve) for 17.8 cm (7") iPad - Black - Polyester, Ethylene Vinyl Acetate (EVA)</a>                               </div>
                                                                                        <div class="hide" id="product-title-1-info">
                                                                                            <p class="w350">Case Logic LAPST-107 Carrying Case (Sleeve) for 17.8 cm (7") iPad - Black - Polyester, Ethylene Vinyl Acetate (EVA)</p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>LAPST107K</span>                                          |                                      EAN: <span>85854221832</span>                                          |                                      SKU: <span>1588959</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">NS</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001588959" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001588959" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001588959" style="display: none" href="#1588959" rel="#qb-singleproduct-000000000001588959-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001588959-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <div class="expanded-prod-details-container" id="expanded-1">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001588959">
                                                                                        <div id="in-stock-000000000001588959" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>6 In Stock
                                                                                            <br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001588959-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                                        <td>6</td>
                                                                                                        <td>0</td>
                                                                                                        <td></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="inline-stock-000000000001588959">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>6</span><span>&nbsp;Lokaal</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001588959"><span class="your-price"><span class="resprice">€ 9,47</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001588959"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001588959">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001588959" class="hidden-sku" type="hidden">
                                                                                    <input value="1588959" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 9,47" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="6" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="6" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span id="addtobasket-000000000001588959-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001588959-ctrlwh-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p></p>
                                                                                    </div>
                                                                                    <input value="" class="hidden-count-item-000000000001588959" type="hidden">
                                                                                    <div id="icon-tool-tip-000000000001588959" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001588959-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001588959-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001588959"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001588959" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001588959-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001588959-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001588959">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001588959"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001588959"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001588959" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input  value="NL01@@4500@@10@@000000000001589823" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://inquirecontent2.ingrammicro.com/300/1019998862.jpg?noimage=logo" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/1019998862.jpg"  width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001589823" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001589823" class="ellipsis-multiline" rel="#product-title-2-info"><b><i>CASE LOGIC</i></b> LAPST-110 Carrying Case (Sleeve) for 25.4 cm (10") iPad - Black - Ethylene Vinyl Acetate (EVA), Polyester - Textured</a>                               </div>
                                                                                        <div class="hide" id="product-title-2-info">
                                                                                            <p class="w350">Case Logic LAPST-110 Carrying Case (Sleeve) for 25.4 cm (10") iPad - Black - Ethylene Vinyl Acetate (EVA), Polyester - Textured</p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>LAPST110K</span>                                          |                                      EAN: <span>85854221849</span>                                          |                                      SKU: <span>1589823</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">NS</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001589823" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001589823" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001589823" style="display: none" href="#1589823" rel="#qb-singleproduct-000000000001589823-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001589823-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <!-- Special Flags End -->
                                                                                        <!-- EXPANDED PROD DETAILS GO HERE -->
                                                                                        <div class="expanded-prod-details-container" id="expanded-2">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001589823">
                                                                                        <div id="in-stock-000000000001589823" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>5 In Stock
                                                                                            <br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001589823-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                                        <td>5</td>
                                                                                                        <td>0</td>
                                                                                                        <td></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="inline-stock-000000000001589823">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>5</span><span>&nbsp;Lokaal</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001589823"><span class="your-price"><span class="resprice">€ 10,52</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001589823"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001589823">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001589823" class="hidden-sku" type="hidden">
                                                                                    <input value="1589823" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 10,52" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="5" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="5" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span id="addtobasket-000000000001589823-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001589823-ctrlwh-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p></p>
                                                                                    </div>
                                                                                    <input value="" class="hidden-count-item-000000000001589823" type="hidden">
                                                                                    <div id="icon-tool-tip-000000000001589823" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001589823-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001589823-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001589823"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001589823" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001589823-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001589823-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001589823">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001589823"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001589823"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001589823" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result evenColor">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input  value="NL01@@4500@@10@@000000000001721961" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://inquirecontent3.ingrammicro.com/300/1020389027.jpg?noimage=logo" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/1020389027.jpg"  width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001721961" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001721961" class="ellipsis-multiline" rel="#product-title-3-info"><b><i>STARTECH</i></b>.com Video Testing Device</a>                               </div>
                                                                                        <div class="hide" id="product-title-3-info">
                                                                                            <p class="w350">StarTech.com Video Testing Device</p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>VGAPATTERNEU</span>                                          |                                      EAN: <span>65030843256</span>                                          |                                      SKU: <span>1721961</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">IN</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001721961" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001721961" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001721961" style="display: none" href="#1721961" rel="#qb-singleproduct-000000000001721961-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001721961-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <!-- Special Flags End -->
                                                                                        <!-- EXPANDED PROD DETAILS GO HERE -->
                                                                                        <div class="expanded-prod-details-container" id="expanded-3">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001721961">
                                                                                        <div id="in-stock-000000000001721961" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>3 In Stock
                                                                                            <br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001721961-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                                        <td>3</td>
                                                                                                        <td>0</td>
                                                                                                        <td></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="inline-stock-000000000001721961">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>3</span><span>&nbsp;Lokaal</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001721961"><span class="your-price"><span class="resprice">€ 179,44</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001721961"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001721961">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001721961" class="hidden-sku" type="hidden">
                                                                                    <input value="1721961" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 179,44" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="3" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="3" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span id="addtobasket-000000000001721961-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001721961-ctrlwh-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p></p>
                                                                                    </div>
                                                                                    <input value="" class="hidden-count-item-000000000001721961" type="hidden">
                                                                                    <div id="icon-tool-tip-000000000001721961" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001721961-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001721961-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001721961"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001721961" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001721961-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001721961-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001721961">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001721961"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001721961"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001721961" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input onclick="javascript: handleRecentlyViewedCheckboxClick(this, 'a.clsCompare');" value="NL01@@4500@@10@@000000000001721929" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://inquirecontent3.ingrammicro.com/300/1020389026.jpg?noimage=logo" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/1020389026.jpg"  width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001721929" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001721929" class="ellipsis-multiline" rel="#product-title-4-info"><b><i>STARTECH</i></b>.com Video Testing Device</a>                               </div>
                                                                                        <div class="hide" id="product-title-4-info">
                                                                                            <p class="w350">StarTech.com Video Testing Device</p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>HDMIPATRN2EU</span>                                          |                                      EAN: <span>65030843232</span>                                          |                                      SKU: <span>1721929</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">IN</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001721929" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001721929" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001721929" style="display: none" href="#1721929" rel="#qb-singleproduct-000000000001721929-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001721929-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <!-- Special Flags End -->
                                                                                        <!-- EXPANDED PROD DETAILS GO HERE -->
                                                                                        <div class="expanded-prod-details-container" id="expanded-4">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001721929">
                                                                                        <div id="in-stock-000000000001721929" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>3 In Stock
                                                                                            <br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001721929-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                                        <td>3</td>
                                                                                                        <td>0</td>
                                                                                                        <td></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="inline-stock-000000000001721929">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>3</span><span>&nbsp;Lokaal</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001721929"><span class="your-price"><span class="resprice">€ 193,00</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001721929"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001721929">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001721929" class="hidden-sku" type="hidden">
                                                                                    <input value="1721929" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 193,00" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="3" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="3" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span id="addtobasket-000000000001721929-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001721929-ctrlwh-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p></p>
                                                                                    </div>
                                                                                    <input value="" class="hidden-count-item-000000000001721929" type="hidden">
                                                                                    <div id="icon-tool-tip-000000000001721929" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001721929-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001721929-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001721929"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001721929" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001721929-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001721929-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001721929">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001721929"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001721929"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001721929" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result evenColor">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input onclick="javascript: handleRecentlyViewedCheckboxClick(this, 'a.clsCompare');" value="NL01@@4500@@10@@000000000001589881" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://inquirecontent1.ingrammicro.com/300/1019998865.jpg?noimage=logo" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/1019998865.jpg"  width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001589881" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001589881" class="ellipsis-multiline" rel="#product-title-5-info"><b><i>CASE LOGIC</i></b> LNEO-7 Carrying Case (Sleeve) for 17.8 cm (7") Tablet PC, Digital Text Reader - Red - Weather Resistant - Neoprene, Polyester</a>                               </div>
                                                                                        <div class="hide" id="product-title-5-info">
                                                                                            <p class="w350">
                                                                                                Case
 Logic LNEO-7 Carrying Case (Sleeve) for 17.8 cm (7") Tablet PC, Digital
 Text Reader - Red - Weather Resistant - Neoprene, Polyester
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>LNEO7R</span>                                          |                                      EAN: <span>85854222310</span>                                          |                                      SKU: <span>1589881</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">NS</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001589881" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001589881" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001589881" style="display: none" href="#1589881" rel="#qb-singleproduct-000000000001589881-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001589881-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <!-- Special Flags End -->
                                                                                        <!-- EXPANDED PROD DETAILS GO HERE -->
                                                                                        <div class="expanded-prod-details-container" id="expanded-5">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001589881">
                                                                                        <div id="in-stock-000000000001589881" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>2 In Stock
                                                                                            <br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001589881-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                                        <td>2</td>
                                                                                                        <td>0</td>
                                                                                                        <td></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="inline-stock-000000000001589881">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>2</span><span>&nbsp;Lokaal</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001589881"><span class="your-price"><span class="resprice">€ 9,47</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001589881"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001589881">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001589881" class="hidden-sku" type="hidden">
                                                                                    <input value="1589881" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 9,47" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="2" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="2" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span id="addtobasket-000000000001589881-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001589881-ctrlwh-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p></p>
                                                                                    </div>
                                                                                    <input value="" class="hidden-count-item-000000000001589881" type="hidden">
                                                                                    <div id="icon-tool-tip-000000000001589881" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001589881-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001589881-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001589881"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001589881" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001589881-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001589881-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001589881">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001589881"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001589881"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001589881" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input onclick="javascript: handleRecentlyViewedCheckboxClick(this, 'a.clsCompare');" value="PE02@@4500@@10@@000000000001658031" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://inquirecontent3.ingrammicro.com/300/1019985065.jpg?noimage=logo" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/1019985065.jpg" width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001658031" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=PE02@@4500@@10@@000000000001658031" class="ellipsis-multiline" rel="#product-title-6-info"><b><i>MOTOROLA</i></b> DS3508-SR Handheld Barcode Scanner - Cable - 1D, 2D - Laser - Imager - Omni-directional</a>                               </div>
                                                                                        <div class="hide" id="product-title-6-info">
                                                                                            <p class="w350">Motorola DS3508-SR Handheld Barcode Scanner - Cable - 1D, 2D - Laser - Imager - Omni-directional</p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>DS3508-SRAU0200ZR</span>                                          |                                      SKU: <span>1658031</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">IN</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001658031" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001658031" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001658031" style="display: none" href="#1658031" rel="#qb-singleproduct-000000000001658031-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001658031-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <!-- Special Flags End -->
                                                                                        <!-- EXPANDED PROD DETAILS GO HERE -->
                                                                                        <div class="expanded-prod-details-container" id="expanded-6">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001658031">
                                                                                        <div id="in-stock-000000000001658031" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>106 In Stock
                                                                                            <br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001658031-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Centraal Magazijn (Straubing, Duitsland) </td>
                                                                                                        <td>106</td>
                                                                                                        <td>0</td>
                                                                                                        <td>14/03/2014</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="inline-stock-000000000001658031">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>106</span><span>&nbsp;Centraal</span> </p>
                                                                                                <p><span>(ETA:14/03/2014)</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001658031"><span class="your-price"><span class="resprice">€ 347,35</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001658031"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001658031">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001658031" class="hidden-sku" type="hidden">
                                                                                    <input value="1658031" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 347,35" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="0" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="106" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span class="tooltip" id="addtobasket-000000000001658031-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001658031-ctrlwh-info" style="visibility: hidden" class="in-stock-details">This product is shipped directly from our central European warehouse and may take 2 business working days to be delivered</div>
                                                                                    <input value="" class="hidden-count-item-000000000001658031" type="hidden">
                                                                                    <div id="icon-tool-tip-000000000001658031" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001658031-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001658031-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001658031"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001658031" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001658031-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001658031-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001658031">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001658031"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001658031"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001658031" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result evenColor">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input onclick="javascript: handleRecentlyViewedCheckboxClick(this, 'a.clsCompare');" value="PE02@@4500@@10@@000000000001623262" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <img src="../ScriptsImages/no-photo-sm.gif" width="40">
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001623262" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=PE02@@4500@@10@@000000000001623262" class="ellipsis-multiline" rel="#product-title-7-info"><span class="italic bold">ELO TOUCH SOLUTIONS</span> ELO-STAND-4200L-4600L -5500L</a>                               </div>
                                                                                        <div class="hide" id="product-title-7-info">
                                                                                            <p class="w350">ELO-STAND-4200L-4600L -5500L</p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>E448725</span>                                          |                                      SKU: <span>1623262</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">NS</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001623262" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001623262" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001623262" style="display: none" href="#1623262" rel="#qb-singleproduct-000000000001623262-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001623262-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <!-- Special Flags End -->
                                                                                        <!-- EXPANDED PROD DETAILS GO HERE -->
                                                                                        <div class="expanded-prod-details-container" id="expanded-7">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001623262">
                                                                                        <div id="in-stock-000000000001623262" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>1 In Stock
                                                                                            <br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001623262-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Centraal Magazijn (Straubing, Duitsland) </td>
                                                                                                        <td>1</td>
                                                                                                        <td>3</td>
                                                                                                        <td>07/02/2014</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="inline-stock-000000000001623262">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>1</span><span>&nbsp;Centraal</span> </p>
                                                                                                <p><span>(ETA:07/02/2014)</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001623262"><span class="your-price"><span class="resprice">€ 135,68</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001623262"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001623262">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001623262" class="hidden-sku" type="hidden">
                                                                                    <input value="1623262" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 135,68" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="0" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="1" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span class="tooltip" id="addtobasket-000000000001623262-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001623262-ctrlwh-info" style="visibility: hidden" class="in-stock-details">This product is shipped directly from our central European warehouse and may take 2 business working days to be delivered</div>
                                                                                    <input value="" class="hidden-count-item-000000000001623262" type="hidden">
                                                                                    <div id="icon-tool-tip-000000000001623262" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001623262-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001623262-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001623262"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001623262" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001623262-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001623262-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001623262">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001623262"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001623262"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001623262" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input onclick="javascript: handleRecentlyViewedCheckboxClick(this, 'a.clsCompare');" value="NL01@@4500@@10@@000000000001571552" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://icecatcontent1.ingrammicro.com/img/norm/high/1767738-6187.jpg" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/1767738.jpg"  width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="false" id="e4a-000000000001571552" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001571552" class="ellipsis-multiline" rel="#product-title-8-info"><span class="italic bold">KENSINGTON</span> USER KEY (RESELLER / ENDUSER)</a>                               </div>
                                                                                        <div class="hide" id="product-title-8-info">
                                                                                            <p class="w350">USER KEY (RESELLER / ENDUSER)</p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>K64019F</span>                                          |                                      EAN: <span>5028252266062</span>                                          |                                      SKU: <span>1571552</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">NS</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001571552" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001571552" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001571552" style="display: none" href="#1571552" rel="#qb-singleproduct-000000000001571552-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001571552-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <!-- Special Flags End -->
                                                                                        <!-- EXPANDED PROD DETAILS GO HERE -->
                                                                                        <div class="expanded-prod-details-container" id="expanded-8">
                                                                                            <div class="expanded-prod-details">
                                                                                                <div class="icons">
                                                                                                    <a href="#" rel="direct-ship-no-add-basket" class="tooltip">
                                                                                                        <img src="../ScriptsImages/pf_direct_ship.gif" alt="" title="" height="17" width="17">
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001571552">
                                                                                        <div id="in-stock-000000000001571552" class="in-stock tooltip">
                                                                                            <a href="#" class="warehouse-tip">Hover to View Warehouse Availability</a>                                           <span>Direct Ship Only<br>
                                                                                            </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001571552-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <p>Direct Ship: This item can be shipped directly from vendor.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001571552"><span class="your-price"><span class="resprice">€ 11,77</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001571552"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns">
                                                                                    <input class="amount" value="1" disabled="disabled" type="text">
                                                                                    <span id="addtobasket-000000000001571552" class="tooltip">
                                                                                        <input class="add-cart-btn-disabled" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div id="addtobasket-000000000001571552-info" style="visibility: hidden" class="in-stock-details">
                                                                                <p>Direct Ship: This item can be shipped directly from vendor.</p>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001571552" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="new-divider">
                                                                    <!-- IE6 -->
                                                                </div>
                                                                <div class="single-result evenColor">
                                                                    <div class="result-left">
                                                                        <div class="toggle-positioning-container">
                                                                            <div class="prod-compare-checkbox">
                                                                                <input onclick="javascript: handleRecentlyViewedCheckboxClick(this, 'a.clsCompare');" value="NL01@@4500@@10@@000000000001572269" type="checkbox">
                                                                            </div>
                                                                            <div class="prod-image">
                                                                                <a href="https://icecatcontent3.ingrammicro.com/img/norm/high/8869791-4957.jpg" rel="superbox[image]">
                                                                                    <img src="../ScriptsImages/8869791.jpg"  width="40"></a>
                                                                            </div>
                                                                            <div class="prod-details">
                                                                                <div class="top-details">
                                                                                    <div class="product-information">
                                                                                        <input class="hdnIsEligibleForAlert" value="true" id="e4a-000000000001572269" type="hidden">
                                                                                        <div class="product-name"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001572269" class="ellipsis-multiline" rel="#product-title-9-info"><span class="italic bold">NETGEAR</span> AD TECH SUP AND SW MAIN,CAT 4 IN</a>                               </div>
                                                                                        <div class="hide" id="product-title-9-info">
                                                                                            <p class="w350">AD TECH SUP AND SW MAIN,CAT 4 IN</p>
                                                                                        </div>
                                                                                        <div class="prod-number-container">
                                                                                            <div class="clear"></div>
                                                                                            VPN: <span>PAS0314-100EUS</span>                                          |                                      EAN: <span>606449079340</span>                                          |                                      SKU: <span>1572269</span>
                                                                                            |                             
         Lang. Code:                                     <a rel="language-code-popup" class="ean tooltip" href="#">IN</a>
                                                                                        </div>
                                                                                        <!-- Special Flags Start-->
                                                                                        <div class="clear"></div>
                                                                                        <div class="special-flags">
                                                                                            <a id="sp-price-000000000001572269" class="special-pricing product-sku-flag standard-tooltip" href="#" rel="#spPriceToolTip" style="display: none;">Special Pricing                                   </a>
                                                                                            <a id="web-disc-000000000001572269" class="web-discount product-sku-flag standard-tooltip" href="#" rel="#webDiscToolTip" style="display: none;">Web Only Price                                   </a><a class="quantity-break product-sku-flag " id="QB-000000000001572269" style="display: none" href="#1572269" rel="#qb-singleproduct-000000000001572269-info">Quantity Breaks                                 </a>
                                                                                            <div class="hide" id="qb-singleproduct-000000000001572269-info"></div>
                                                                                        </div>
                                                                                        <div class="clear"></div>
                                                                                        <!-- Special Flags End -->
                                                                                        <!-- EXPANDED PROD DETAILS GO HERE -->
                                                                                        <div class="expanded-prod-details-container" id="expanded-9">
                                                                                            <div class="expanded-prod-details"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display Start -->
                                                                                    <div class="in-stock-container" id="stock-000000000001572269">
                                                                                        <div id="pai-000000000001572269" class="tooltip alertContainer" rel=""><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/javascript%20void%280%29" class="stock-alert-icon tooltip"></a></div>
                                                                                        <div id="pai-000000000001572269-info" class="hide">Click to subscribe to a stock alert.</div>
                                                                                        <div id="in-stock-000000000001572269" class="in-stock tooltip">
                                                                                            <span>Out of Stock<br>
                                                                                            </span><span class="eta">(ETA: 03/03/2014)                              </span>
                                                                                        </div>
                                                                                        <div id="in-stock-000000000001572269-info" style="visibility: hidden" class="in-stock-details">
                                                                                            <table class="in-stock-details-table">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <th class="warehouse-header">Warehouse                             </th>
                                                                                                        <th class="stock-header">Stock                             </th>
                                                                                                        <th class="on-order-header">On Order                             </th>
                                                                                                        <th class="eta-header">ETA                             </th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                                        <td>0</td>
                                                                                                        <td>0</td>
                                                                                                        <td>03/03/2014</td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <p class="in-stock-details-message">Note:All orders will be picked from your reserved stock (if applicable) before being picked from Ingram's general stock</p>
                                                                                            <p class="in-stock-details-message">
                                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                            </p>
                                                                                        </div>
                                                                                        <div id="inline-stock-000000000001572269">
                                                                                            <div class="plantDetails">
                                                                                                <p><span>0</span><span>&nbsp;Lokaal</span> </p>
                                                                                                <p><span>(ETA:03/03/2014)</span> </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Stock Display End -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="result-right">
                                                                        <div class="pricing">
                                                                            <div class="pricing-info" id="price-000000000001572269"><span class="your-price"><span class="resprice">€ 647,62</span><span class="excl-tax excltax">   Excl btw</span></span>                     </div>
                                                                            <div id="prod-fees-000000000001572269"></div>
                                                                            <div class="centered-btns-container">
                                                                                <div class="add-product centered-btns" id="addtobasket-000000000001572269">
                                                                                    <input class="amount" value="1" type="text">
                                                                                    <input value="000000000001572269" class="hidden-sku" type="hidden">
                                                                                    <input value="1572269" class="wt-hidden-sku" type="hidden">
                                                                                    <input value="€ 647,62" class="hidden-price" type="hidden">
                                                                                    <input value="true" class="hidden-IsPriceable" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="0" type="hidden">
                                                                                    <input value="" class="hidden-IsCentrallyStocked" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="0" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">
                                                                                    <span id="addtobasket-000000000001572269-ctrlwh">
                                                                                        <input class="add-cart-btn" src="../ScriptsImages/add-btn-small.gif" type="image">
                                                                                    </span>
                                                                                    <div id="addtobasket-000000000001572269-ctrlwh-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p></p>
                                                                                    </div>
                                                                                    <input value="" class="hidden-count-item-000000000001572269" type="hidden">
                                                                                    <div id="icon-tool-tip-000000000001572269" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001572269-Icon" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001572269-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;<span class="countUnit-000000000001572269"></span>&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001572269" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001572269-Msg" class="tooltip"><span class="in-cart-tip hide"></span></a>
                                                                                        <div id="addtobasket-000000000001572269-Msg-info" style="display: none;" class="hide add-to-bsk-msg-000000000001572269">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001572269"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001572269"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="add-favorite"><a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001572269" class="add-fav">+ Add to Favourites</a>                 </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="hide" id="componentToolTip">
                                                                <p>This product belongs to one or more bundles. Click here to see all bundles.  </p>
                                                            </div>
                                                            <div class="hide" id="bundleToolTip">
                                                                <p>This is a bundled product  </p>
                                                            </div>
                                                            <div class="hide" id="bidToolTip">
                                                                <p>This product has special bid prices available. Click the [Select Bid button/Bid Info link] to see bid pricing options.  </p>
                                                            </div>
                                                            <div class="hide" id="newToolTip">
                                                                <p>This product is new to market  </p>
                                                            </div>
                                                            <div class="hide" id="spPriceToolTip">
                                                                <p>Discounts and/or offers apply to this item.  </p>
                                                            </div>
                                                            <div class="hide" id="webDiscToolTip">
                                                                <p>This price is only available for products purchased on the web  </p>
                                                            </div>
                                                            <div class="hide" id="Div9">
                                                                <p>This is a Refurbished Item   </p>
                                                            </div>
                                                            <div class="hide" id="yourStockTooltip">
                                                                <p>This item is owned by you  </p>
                                                            </div>
                                                            <div class="hide" id="Div10">
                                                                <p>
                                                                    Dit product is nog niet beschikbaar, maar u kunt vooruit 
bestellen zodat het verzonden word zodra het leverbaar is.    
                                                                </p>
                                                            </div>
                                                            <div class="hide" id="blowoutToolTip">
                                                                <p>Special Price available until stock lasts               </p>
                                                            </div>
                                                        </div>
                                                        <div id="bottom-paging-container">
                                                            <div class="pager-container">
                                                                <div class="compare"><a class="small-red-btn tooltip-clickable clsCompare" rel="#cmp_lbl" href="#">Compare<span class="small-red-btn-right"></span>                 </a></div>
                                                                <div class="sort-by-left sort-by" id="Div11">
                                                                    <label>Sort results by:</label>
                                                                    <select id="bottomSortSelect">
                                                                        <option selected="selected" style="background-color: #FFFFFF;" value="0">Relevance</option>
                                                                        <option value="5">Vendor Name, A to Z</option>
                                                                        <option value="6">Vendor Name, Z to A</option>
                                                                        <option value="1">Price, High to Low</option>
                                                                        <option value="2">Price, Low to High</option>
                                                                        <option value="3">Stock, High to Low</option>
                                                                        <option value="4">Stock, Low to High</option>
                                                                        <option value="7">VPN, A to Z</option>
                                                                        <option value="8">VPN, Z to A</option>
                                                                    </select>
                                                                    <input alt="" class="tooltip-clickable" rel="#chkSaveSortBy-info" id="Checkbox5" type="checkbox">
                                                                </div>
                                                                <div class="show-picker">
                                                                    <b>Show:</b>
                                                                    <div class="imageView">
                                                                        <a class="tooltip-clickable" rel="#ListViewtooltip-info" href="#">List</a>  <span class="tooltip" id="Span1">With Images</span>
                                                                        <div id="Div12" class="hide">
                                                                            <p>With Images</p>
                                                                        </div>
                                                                        <div id="Div13" class="hide">
                                                                            <p>List</p>
                                                                        </div>
                                                                        <div id="Div14" class="hide">
                                                                            <p>Save your Sorting Preference</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="sort-by-right sort-by" id="Div15">
                                                                    <label>Per page:</label>
                                                                    <select id="bottomPagingSelect">
                                                                        <option value="10" selected="selected">10</option>
                                                                        <option value="20">20</option>
                                                                        <option value="30">30</option>
                                                                    </select>
                                                                </div>
                                                                <div class="pager"><span class="page-num">1</span>               <a id="A19" class="page-num" href="#2">2</a>              <a id="A20" class="page-num" href="#3">3</a>              <span class="pager-ellipsis">...</span>               <a id="A21" class="page-num" href="#16">16</a>              <a id="A22" class="pager-button next" href="#2"></a><a id="A23" class="page-num" href="#16">Last</a>          </div>
                                                                <div class="compare-tt hide" id="Div16">
                                                                    <p class="msg">Select up to 4 products</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="bottom-sorting-container"></div>
                                                    </div>

                                                </div>
                                                <div id="acop-message-container"></div>
                                                <div id="back-to-top-container">
                                                    <div class="back-to-top" id="back-to-top"><a href="#top" class="searchtop">Back to Top</a>     </div>
                                                </div>
                                                <div class="matching-cat" id="matching-categories-container">
                                                    <h3 class="header">Matching Categories</h3>
                                                    <ul>
                                                        <li><a href="#4294821097" class="refine-sub-categories-link">Sub-Category &gt; Test Equipment</a></li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="clear"></div>


                                    <div id="tooltips-container">
                                        <div id="status-flag-tooltips-container">
                                            <div class="hide" id="special-order-info">
                                                <p>Item may take slightly longer to ship.  </p>
                                            </div>
                                            <div class="hide" id="export-item-info">
                                                <p>This item cannot be shipped to any foreign country.  </p>
                                            </div>
                                            <div class="hide" id="direct-ship-info">
                                                <p>Direct Ship: This item can be shipped directly from vendor.  </p>
                                            </div>
                                            <div class="hide" id="direct-ship-no-add-basket-info">
                                                <p>Direct Ship: This item can be shipped directly from vendor.  </p>
                                            </div>
                                            <div class="hide" id="no-free-freight-info">
                                                <p>This item has applicable freight costs.  </p>
                                            </div>
                                            <div class="hide" id="heavy-weight-info">
                                                <p>Customer Pays Freight.  </p>
                                            </div>
                                            <div class="hide" id="ltl-info">
                                                <p>This is a larger than truckload item.  </p>
                                            </div>
                                            <div class="hide" id="additional-fees-info">
                                                <p>Additional Shipping Fees     </p>
                                            </div>
                                            <div class="hide" id="shipping-limits-info">
                                                <p>Shipping Limitations     </p>
                                            </div>
                                            <div class="hide" id="return-limits-info">
                                                <p>Return Limitations     </p>
                                            </div>
                                            <div class="hide" id="end-user-info-req-info">
                                                <p>End-User Information Required     </p>
                                            </div>
                                            <div class="hide" id="selling-restrictions-info">
                                                <p>Selling Restrictions     </p>
                                            </div>
                                            <div class="hide" id="jtype-info">
                                                <p>Shipped from Central Warehouse     </p>
                                            </div>
                                            <div class="hide" id="ship-along-info">
                                                <p>Free Items     </p>
                                            </div>
                                            <div class="hide" id="bulk-freight-info">
                                                <p>Bulk freight charges apply     </p>
                                            </div>
                                            <div class="hide" id="download-info">
                                                <p>
                                                    Downloadable Product: The vendor will provide the download URL 
and product key in an email to the reseller and the end user.    
                                                </p>
                                            </div>
                                        </div>
                                        <div id="rich-media-tooltips-container">
                                            <div class="hide" id="rm-support">
                                                <p>Support  </p>
                                            </div>
                                            <div class="hide" id="rm-brochure">
                                                <p>Brochure  </p>
                                            </div>
                                            <div class="hide" id="rm-white-paper">
                                                <p>Whitepaper  </p>
                                            </div>
                                            <div class="hide" id="rm-video">
                                                <p>Video  </p>
                                            </div>
                                            <div class="hide" id="rm-tour">
                                                <p>Tour  </p>
                                            </div>
                                            <div class="hide" id="rm-gallery">
                                                <p>Gallery  </p>
                                            </div>
                                            <div class="hide" id="rm-ppt">
                                                <p>PowerPoint  </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hide" id="global-search-tooltip">
                                        <p>Search</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    
                <%--CMS insertion in this div, will use existing id="sidebar" to identify div--%>
                    <div id="sidebar">
                        <div style="height: 3963px;" class="filler" id="filler-div">
                            <div style="height: 3951px;" class="inner">
                                <img src="../Images/spinner.gif" />
                            </div>
                        </div>
                    </div>

                    <div id="footer">
                        <ul id="ctl00_imFooter_Ul1">
                            <li id="ctl00_imFooter_Products">
                                <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=Np:All" id="qtp_footer_search">Products
                                </a>
                            </li>

                            <li id="ctl00_imFooter_Vendors">
                                <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/AllVendors.aspx?mnc=true">Vendors
                                </a>
                            </li>
                            <li id="ctl00_imFooter_CloudServices">
                                <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:244">Cloud Services
                                </a>
                            </li>

                            <li id="ctl00_imFooter_Services">
                                <a href="https://test.ingrammicro.com/Pages/ServicesAndSupport.aspx" id="qtp_footer_support">Services &amp; Support
                                </a>
                            </li>


                            <li>
                                <a href="https://test.ingrammicro.com/_layouts/CommerceServer/IM/AllNews.aspx" id="qtp_footer_news">News and Events
                                </a>
                            </li>
                            <li>
                                <a href="https://test.ingrammicro.com/Pages/IGMContactUs.aspx" id="qtp_footer_contact">Contact Us
                                </a>
                            </li>
                            <li>
                                <a href="https://test.ingrammicro.com/Pages/IMAboutIngramMicro.aspx" id="qtp_footer_about">About Us
                                </a>
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                        <div class="copyright" id="qtp_footer_copyright">
                            <div class="bottom-footer-right">
                            </div>
                            <div class="bottom-footer-left">
                                Copyright © 2014 Ingram Micro. All rights reserved.
            <span class="spacing">&nbsp;</span><!-- Phone -->
                            </div>

                        </div>
                    </div>

                </div>
            </div>


            <div id="Div169" class="hide"></div>
        </div>
        <div style="display: none;" id="superbox-overlay"></div>
        <div style="display: none;" id="superbox-wrapper"></div>
    </form>
    <div id="ingramTooltipCover" class="tooltip-cover-container"></div>
        <script type="text/javascript" src="../Scripts/AsyncLoad.js"></script>
</body>
</html>
