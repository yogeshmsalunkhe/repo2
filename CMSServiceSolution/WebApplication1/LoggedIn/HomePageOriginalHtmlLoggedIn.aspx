﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomePageOriginalHtmlLoggedIn.aspx.cs" Inherits="TestWebAppImOnline.LoggedIn.HomePageOriginalHtmlLoggedIn" %>

<!DOCTYPE html>
<html>
<!-- CSDefault -->
<head id="ctl00_Head1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <title>Computer and Technology Products - Services for Business to Business Needs - Ingram Micro
    </title>
    <meta id="ctl00_sid" name="sidName" content="4008">
</head>
<body>

    <link href="../ScriptsImages/combined1.css" rel="stylesheet" type="text/css">
    <link href="../ScriptsImages/combined2.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="../ScriptsImages/print.css" media="print">
    <link rel="stylesheet" type="text/css" href="../ScriptsImages/license.css">
    <!--[if IE 6]>
<link rel="stylesheet" type="text/css" href="/_layouts/images/CSDefaultSite/styles/ie6.css">
<![endif]-->

    <!--[if IE 7]>
<link href="/_layouts/images/CSDefaultSite/styles/ie7.css" type="text/css" rel="stylesheet" />
<style type="text/css">
#superbox{margin-top: 40%;}

.in-stock img {padding-left:20px;}
</style>
<![endif]-->

    <!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="/_layouts/images/CSDefaultSite/styles/ie8.css">
<![endif]-->
    <!--[if lt IE 10]>
<script type="text/javascript" src="/_layouts/images/CSDefaultSite/javascripts/PIE.js"></script>
<![endif]-->

    <script type="text/javascript" src="../ScriptsImages/jquery-1.js"></script>
    <script type="text/javascript" src="../ScriptsImages/jquery-migrate-1.js"></script>
    <script type="text/javascript" src="../ScriptsImages/jquery-ui-1.js"></script>

    <meta name="description" content="Ingram Micro is the world�s largest distributor of computer and technology products, offering the largest selection of hardware and software products and services for business-to-business needs.">
    <meta name="keywords" content="computer and technology products, software products, business to business, supply chain management, technology resellers, Ingram Micro">


    <form name="aspnetForm" method="post" id="aspnetForm">

        <a class="hide" href="#skip-to-nav" id="qtp_skip_to_nav">Skip to main navigation</a>
        <a class="hide" href="#begin-content" id="qtp_begin_content">Skip to main content</a>
        <div id="wrapper" class="logged-in">
            <div id="header">
                <div id="headerContainer" class="float-left header-width ">
                    <div class="header-top " id="master_header_top">
                        <a href="javascript:window.location.href=get_hostname(window.location)" id="qtp_logo">
                            <img src="../ScriptsImages/logo_imi.gif" alt="Ingram Micro Inc." class="ingram-logo" border="0" height="50" width="85"></a>
                        <a href="#" class="open-country-select-popup" id="qtp_country">
                            <span class="country-change">Change Country</span>
                        </a>
                        <div class="location-language">
                            Ingram Micro
                <a href="#" class="open-country-select-popup" id="qtp_country_popup">Netherlands</a> <span id="ctl00_spanverticalBar" class="vert-bar-space">|</span>

                            <select name="ctl00$lngSelecter$LanguagesDropDown" onchange="javascript:setTimeout('__doPostBack(\'ctl00$lngSelecter$LanguagesDropDown\',\'\')', 0)" id="ctl00_lngSelecter_LanguagesDropDown" class="langselector">
                                <option value="nl-NL">Nederlands</option>
                                <option selected="selected" value="en-NL">English</option>

                            </select>
                            <input name="ctl00$lngSelecter$hidURLSelector" id="ctl00_lngSelecter_hidURLSelector" type="hidden">

                            <a href="#" class="open-country-select-popup" id="qtp_country_language"></a>


                            <div>
                                <a href="https://nl.ingrammicro.com/Pages/help.aspx" id="qtp_country_help">Help</a> <span class="vert-bar-space">|</span> <a href="https://nl.ingrammicro.com/Pages/IMAboutIngramMicro.aspx">About Us</a>

                                <span class="vert-bar-space">|</span>
                                <a id="ctl00_btnLogout" href="javascript:__doPostBack('ctl00$btnLogout','')">Log Out</a>
                                <a onclick="showBusy(false, null);" id="ctl00_LinkButton1" href="javascript:__doPostBack('ctl00$LinkButton1','')"></a>

                            </div>
                        </div>

                        <div class="help-about " id="login_help-about">
                            <span class="body-blue-header welcome-header">Darius  Lezama</span><br>
                            <div style="white-space: nowrap;">
                                IM Account #: <span class="colorBlack">186992</span>

                                <br>
                            </div>
                        </div>

                        <div class="header-right-border"></div>
                    </div>
                    <div id="main-nav-expand">
                        <a name="skip-to-nav" id="qtp_skip"></a>
                        <table id="ctl00_tableLoggedIn" class="logged-in">
                            <tbody>
                                <tr>
                                    <td id="ctl00_tdProducts"><a href="javascript:void(0)" class="prod" id="product-activate">Products               
                                    </a></td>
                                    <td><a href="javascript:void(0)" class="vend" id="vendor-activate">Vendors</a></td>
                                    <td id="ctl00_tdCloudServices"><a href="javascript:void(0)" id="cloud-activate" class="cloud">Cloud Services</a></td>
                                    <td id="ctl00_tdServices"><a href="https://nl.ingrammicro.com/Pages/ServicesAndSupport.aspx" class="serv" id="qtp_services">Services &amp; Support</a></td>
                                    <td><a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/AllNews.aspx" class="news" id="qtp_news">News &amp; Events</a></td>
                                    <td><a href="https://nl.ingrammicro.com/Pages/IGMContactUs.aspx" class="cont" id="qtp_contact">Contact Us</a></td>
                                    <td id="ctl00_tdMyAccount" class="border-right-none"><a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/MyAccount.aspx" class="acct" id="qtp_account">My Account</a></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>


                    <div id="sub-navigation">
                        <div id="products-menu-container" class="mega-menu-container">
                            <div class="primary-menu" id="primary-products-menu">
                                <div class="inner-spacing">
                                    <div>
                                        <div class="pane-title">Categories</div>
                                        <div class="all-link"><a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=Np:All&amp;mnc=true">View All</a></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="prodTopDown">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="prodTopUp" class="deactivate">Up</a></div>
                                    </div>
                                    <div class="pane-container" id="primary-menu-page-container">
                                        <div class="pane-menu">
                                            <ul id="primary-menu-links">

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288449839&amp;t=pTab" id="3_4288449839PP1">Audio/Visual
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294967244&amp;t=pTab" id="3_4294967244PP1">Cables
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288449895&amp;t=pTab" id="3_4288449895PP1">Cameras &amp; Scanners
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294954486&amp;t=pTab" id="3_4294954486PP1">Communications
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294637804&amp;t=pTab" id="3_4294637804PP1">Components
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288450088&amp;t=pTab" id="3_4288450088PP1">Computers
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288450111&amp;t=pTab" id="3_4288450111PP1">Connectivity Devices
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294953896&amp;t=pTab" id="3_4294953896PP1">Displays
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288450046&amp;t=pTab" id="3_4288450046PP1">Gaming
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294839863&amp;t=pTab" id="3_4294839863PP1">Home &amp; Appliances
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>


                                            </ul>
                                        </div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="prodBottomDown">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="prodBottomUp" class="deactivate">Up</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="secondary-menu hide" id="secondary-menu">
                                <div class="inner-spacing">
                                    <div class="pane-title">Sub-Categories</div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="catprodTopDown" href="#">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="catprodTopUp" href="#" class="deactivate">Up</a></div>
                                    </div>
                                    <div class="pane-container">
                                        <div class="pane-menu"></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="catprodBottomDown">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="catprodBottomUp" class="deactivate">Up</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-menu hide" id="products-vendor-menu">
                                <div class="inner-spacing">
                                    <div class="pane-title">Vendors</div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="subcatprodTopDown" href="#">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="subcatprodTopUp" href="#" class="deactivate">Up</a></div>
                                    </div>
                                    <div class="pane-container">
                                        <div class="pane-menu"></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="subcatprodBottomDown" href="#">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="subcatprodBottomUp" href="#" class="deactivate">Up</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="vendor-menu-container" class="mega-menu-container">
                            <div class="vendor-inner">
                                <div class="primary-menu" id="primary-vendors-menu">
                                    <div class="alpha-list" id="vendors-alpha-list">
                                        <ul>
                                            <li class="first"><a id="a1" href="#" class="">#</a></li>
                                            <li><a id="alpha-a" href="#" class="">a</a></li>
                                            <li><a id="alpha-b" href="#" class="">b</a></li>
                                            <li><a id="alpha-c" href="#" class="">c</a></li>
                                            <li><a id="alpha-d" href="#" class="">d</a></li>
                                            <li><a id="alpha-e" href="#" class="">e</a></li>
                                            <li><a id="alpha-f" href="#" class="">f</a></li>
                                            <li><a id="alpha-g" href="#" class="">g</a></li>
                                            <li><a id="alpha-h" href="#" class="">h</a></li>
                                            <li><a id="alpha-i" href="#" class="">i</a></li>
                                            <li><a id="alpha-j" href="#" class="">j</a></li>
                                            <li><a id="alpha-k" href="#" class="">k</a></li>
                                            <li><a id="alpha-l" href="#" class="">l</a></li>
                                            <li><a id="alpha-m" href="#" class="">m</a></li>
                                            <li><a id="alpha-n" href="#" class="">n</a></li>
                                            <li><a id="alpha-o" href="#" class="">o</a></li>
                                            <li><a id="alpha-p" href="#" class="">p</a></li>
                                            <li><a id="alpha-q" href="#" class="">q</a></li>
                                            <li><a id="alpha-r" href="#" class="">r</a></li>
                                            <li><a id="alpha-s" href="#" class="">s</a></li>
                                            <li><a id="alpha-t" href="#" class="">t</a></li>
                                            <li><a id="alpha-u" href="#" class="">u</a></li>
                                            <li><a id="alpha-v" href="#" class="">v</a></li>
                                            <li><a id="alpha-w" href="#" class="">w</a></li>
                                            <li><a id="alpha-x" href="#" class="">x</a></li>
                                            <li><a id="alpha-y" href="#" class="">y</a></li>
                                            <li><a id="alpha-z" href="#" class="">z</a></li>
                                        </ul>
                                    </div>
                                    <div>
                                        <div class="pane-title">Vendors</div>
                                        <div class="all-link"><a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/AllVendors.aspx">View All</a></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="vendorTopDown" href="#" class="">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="vendorTopUp" class="deactivate" href="#">Up</a></div>
                                    </div>
                                    <div class="pane-container">
                                        <div class="pane-menu">
                                            <ul>
                                                <li><a class="boutique-highlight" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/Boutiques.aspx?&amp;t=vTab" id="vb">Boutiques
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288110840&amp;t=vTab" id="4_4288110840PP1">3M
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294811954&amp;t=vTab" id="4_4294811954PP1">Acco
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294930432&amp;t=vTab" id="4_4294930432PP1">Acer
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4294723805&amp;t=vTab" id="4_4294723805PP1">Adaptec
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288135093&amp;t=vTab" id="4_4288135093PP1">Adcs
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="vendorBottomDown" href="#" class="">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="vendorBottomUp" class="deactivate" href="#">Up</a></div>
                                    </div>
                                </div>
                                <div class="category-menu hide" id="vendors-category-menu">
                                    <div class="pane-title">Product Categories</div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="catvendorTopDown" href="#" class="">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="catvendorTopUp" class="deactivate" href="#">Up</a></div>
                                    </div>
                                    <div class="pane-container">
                                        <div class="pane-menu"></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="catvendorBottomDown" href="#" class="">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="catvendorBottomUp" class="deactivate" href="#">Up</a></div>
                                    </div>
                                </div>
                                <div class="subcategory-menu hide" id="vendors-subcategory-menu">
                                    <div class="pane-title">Sub-Categories</div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="subcatvendorTopDown" href="#" class="">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="subcatvendorTopUp" class="deactivate" href="#">Up</a></div>
                                    </div>
                                    <div class="pane-container">
                                        <div class="pane-menu"></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="subcatvendorBottomDown" href="#" class="">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="subcatvendorBottomUp" class="deactivate" href="#">Up</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Div1">
                        <div id="cloud-menu-container" class="mega-menu-container" style="position: absolute; left: 175.85px;">
                            <div class="primary-menu" id="primary-clouds-menu">
                                <div class="inner-spacing">
                                    <div style="margin-left: 5px; margin-top: 5px" class="pane-menu"><a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/Education.aspx" style="width: 90px;" id="EducationBtn">Education<span style="position: absolute; left: 90px;"></span></a></div>
                                    <div>
                                        <div class="pane-title" style="clear: both; margin-top: 5px">Categories</div>
                                        <div class="all-link"><a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:244">View All</a></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="A2">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="A3" class="deactivate">Up</a></div>
                                    </div>

                                    <div class="pane-container" id="primary-menu-Cloudpage-container" style="clear: both;">
                                        <div class="pane-menu">
                                            <ul id="Ul1">

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288181888&amp;t=pTab" id="A4">Infrastructure-As-A-Service
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288259282&amp;t=pTab" id="A5">Service Provider Licenties
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                                <li><a class="showArrow" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:4288181911&amp;t=pTab" id="A6">Software-As-A-Service
                                                    <div class="menu-arrow-link"></div>
                                                </a></li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="A7">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="A8" class="deactivate">Up</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="secondary-menu hide" id="cloud-secondary-menu">
                                <div class="inner-spacing">
                                    <div class="pane-title">Sub-Categories</div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="A9" href="#">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="A10" href="#" class="deactivate">Up</a></div>
                                    </div>
                                    <div class="pane-container">
                                        <div class="pane-menu"></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a href="#" id="A11">Down</a></div>
                                        <div class="scroll-control scroll-up"><a href="#" id="A12" class="deactivate">Up</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="vendor-menu hide" id="clouds-vendor-menu">
                                <div class="inner-spacing">
                                    <div class="pane-title">Vendors</div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="A13" href="#">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="A14" href="#" class="deactivate">Up</a></div>
                                    </div>
                                    <div class="pane-container">
                                        <div class="pane-menu"></div>
                                    </div>
                                    <div class="arrow-container">
                                        <div class="scroll-control scroll-down"><a id="A15" href="#">Down</a></div>
                                        <div class="scroll-control scroll-up"><a id="A16" href="#" class="deactivate">Up</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="search" id="qtp_searchbar">
                        <div class="quick-tab">
                            <img style="display: none;" src="../ScriptsImages/New_Star.png" id="ctl00_PlaceHolderSearchBar_ctl00_imSearchStar1" class="new-star">
                            Quick Links<img class="quick-arrow" alt="" src="../ScriptsImages/quick-up-arrow.png">
                        </div>
                        <div class="search-content" id="searchOptions">
                            <div>
                                <div class="live-search-container" id="search-field-container">
                                    <input value="Search by Keyword(s), VPN or IM SKU" style="color: rgb(175, 175, 175);" id="searchBox_Global" class="search-text placeholder search-box-global" rel="Search by Keyword(s), VPN or IM SKU" autocomplete="off" type="text">
                                    <div id="live-search" class="global">
                                    </div>
                                </div>
                                <a href="#" class="search-submit tooltip-clickable" id="search-submit-anchor" rel="global-search-tooltip">Search:
                                </a>
                                <a href="#" class="search-tips tooltip-text-search" style="cursor: help;" id="search-tips-anchor"></a>
                                <div style="clear: left;"></div>
                            </div>
                        </div>
                        <div class="hide" id="global-search-tooltip-info">
                            <p>Search</p>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div id="error-message-dialog" class="hide">
                        <div class="modal-popup modal-popup-b medium-small-popup">
                            <div class="container">
                                <div class="error-top">
                                    <div class="warning-msg">
                                        <img alt="" src="../ScriptsImages/icon-alert.gif">
                                        <div class="msg">
                                            <p id="warning-message-dialog-body"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="buttons-container">
                                    <div class="centered-btns-container">
                                        <div class="centered-btns">
                                            <div id="retry-btns">
                                                <a href="javascript:location.reload(true)" class="large-red-submit-btn">Retry</a><div class="large-red-submit-btn-right"></div>
                                                <a href="#" class="cancel-btn close-dialog">Cancel</a>
                                            </div>
                                            <div id="ok-btns">
                                                <a href="#" class="large-red-submit-btn close-dialog">OK</a><div class="large-red-submit-btn-right"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="cancel-btn-top-no-text close-dialog">
                                    <img src="../ScriptsImages/my_dashboard_x.gif" alt="Close"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="height: 149px;" id="dashboardContainer" class="float-left w237 h100pct ">
                    <style type="text/css">
                        .modalBackground
                        {
                            background-color: Gray;
                            filter: alpha(opacity=70);
                            opacity: 0.7;
                            z-index: 10000;
                        }

                        .modalPopup
                        {
                            background-color: White;
                            z-index: 10001;
                        }
                    </style>

                    <div style="height: 144px;" class="dashboard end-customer-mode larger-sidebar" id="loggedInDashboard">
                        <div class="dashboard-inner-container">
                            <div style="height: 136px;" class="inner-border">
                                <div class="dashboard-top ">
                                    <div id="ctl00_imDashboard_panMiniCart">
                                        <input name="ctl00$imDashboard$hdnSelectedShopperList" id="ctl00_imDashboard_hdnSelectedShopperList" type="hidden">
                                        <input name="ctl00$imDashboard$hdnPostbackToChangeShopperList" id="ctl00_imDashboard_hdnPostbackToChangeShopperList" type="hidden">

                                        <input name="ctl00$imDashboard$hdnAddressId" id="ctl00_imDashboard_hdnAddressId" type="hidden">
                                        <input name="ctl00$imDashboard$HiddenAddressId" id="ctl00_imDashboard_HiddenAddressId" type="hidden">
                                        <input name="ctl00$imDashboard$HiddenCnpjNumber" id="ctl00_imDashboard_HiddenCnpjNumber" type="hidden">
                                        <input name="ctl00$imDashboard$HiddenCpfNumber" id="ctl00_imDashboard_HiddenCpfNumber" type="hidden">

                                        <div id="qtp_activeBasketContainer" class="active-basket-container">
                                            <div class="active-basket">
                                                <div class="items-in-cart">
                                                    0
                                                </div>
                                                <div class="basket-name-value">
                                                    <a id="ctl00_imDashboard_hypBasketName" class="basket" onclick="getClickTime(event);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/BasketDetails.aspx">MyBasket</a><br>
                                                    Value
                                        &nbsp; 
                : <span class="cart-total">€ 0,00
                </span>
                                                    <span class="cart-excl-tax">Excl btw</span>
                                                </div>
                                            </div>
                                            <div id="ctl00_imDashboard_pnlBasketOptions" class="basket-options">
                                                <a id="ctl00_imDashboard_lbnCheckOut" class="small-red-btn checkout-btn" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/BasketDetails.aspx">Basket</a>
                                                <div id="ctl00_imDashboard_pnlLbnCheckoutRight" class="small-red-btn-right">
                                                </div>
                                                <a href="#" class="change-basket" id="baskets-link">
                                                    <img alt="" src="../ScriptsImages/arrow_down_dashboard.gif" id="baskets-dropdown">
                                                    Change Baskets</a>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="active-baskets-container"></div>
                                    </div>
                                </div>
                                <div class="dashboard-bottom">
                                </div>

                                <div id="quickSearch-accordion" class="h57pct">
                                    <h3 class="header">Quick Order Search<a id="ctl00_imDashboard_imQuickOrderSearchControl_DimValGoButton" class="small-link" href='javascript:WebForm_DoPostBackWithOptions(new%20WebForm_PostBackOptions("ctl00$imDashboard$imQuickOrderSearchControl$DimValGoButton",%20"",%20true,%20"",%20"",%20false,%20true))'>View All</a>
                                    </h3>
                                    <div class="content-pane h80pct">
                                        <div class="order-search-dashboard h92pct">
                                            <div class="order-search-dashboard">
                                                <div onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_imDashboard_imQuickOrderSearchControl_btnSearchByNumber')">

                                                    <div class="item-entry-container">
                                                        <select name="ctl00$imDashboard$imQuickOrderSearchControl$ddlSearchBy" id="ctl00_imDashboard_imQuickOrderSearchControl_ddlSearchBy" class="quickOrderSearchSelect" style="font-size: X-Small; width: 130px;">
                                                            <option selected="selected" value="ResellerPONumber">Reseller PO Number</option>
                                                            <option value="IngramMicroOrderNumber">IM Order Number</option>
                                                            <option value="EndUserPONumber">End Customer PO Number</option>

                                                        </select>
                                                        <input name="ctl00$imDashboard$imQuickOrderSearchControl$txtSearchByNumberInput" id="ctl00_imDashboard_imQuickOrderSearchControl_txtSearchByNumberInput" class="number" onkeypress="CancelDefaultEvent(event);" style="font-size: X-Small; width: 127px;" type="text">
                                                        <a onclick="return ValidateSearchValue();" id="ctl00_imDashboard_imQuickOrderSearchControl_btnSearchByNumber" class="search-btn quick-search-btn tooltip-clickable" rel="global-search-tooltip" href='javascript:WebForm_DoPostBackWithOptions(new%20WebForm_PostBackOptions("ctl00$imDashboard$imQuickOrderSearchControl$btnSearchByNumber",%20"",%20true,%20"",%20"",%20false,%20true))'>Search</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="clear"></div>
            </div>

            <style type="text/css">
                .modalBackground
                {
                    background-color: Gray;
                    filter: alpha(opacity=70);
                    opacity: 0.7;
                }

                .modal-popup
                {
                    background-color: White;
                }
            </style>

            <div id="quick_links">
                <div class="quick-links">
                    <a id="ctl00_imMyLinks_rptrLinks_ctl00_lnkQK" class=" blue-background DisplayPreferences" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="javascript:void(0);"><b><span>Display Preferences</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl01_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl02_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl03_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl04_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl05_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl06_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl07_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl08_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_rptrLinks_ctl09_lnkQK" class=" disabled-background" onclick="javascript:dowebtrendSubmitOrderCall(this);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx"><b><span>Click to add</span></b></a>

                    <a id="ctl00_imMyLinks_HyperLink3" class="settings" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/DashboardPreferences.aspx">
                        <img src="../ScriptsImages/New_Star.png" id="ctl00_imMyLinks_Img12" class="new-star"><span>Settings</span>
                    </a>
                </div>
            </div>

            <div id="container">
                <a name="begin-content"></a>
                <div id="main-content">
                    <div id="ctl00_fullLayoutDiv">

                        <%--CMS insertion in this div, will add id="leaderboard" to identify this later --%>
                        <div  class="leaderboard main-block">
                            <div class="content">
                                <div>
                                    <a href="http://www.ingrammicro.nl/concrete/klanten/category_stores" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_promoties_nieuws_events_trainingen_banner','WT.dl','50'); " title="MOSS_promoties_nieuws_events_trainingen_banner">
                                        <img src="../ScriptsImages/Campaignblock-C-Store.jpg"></a>
                                </div>
                            </div>
                        </div>

                        <input name="ctl00$PlaceHolderIMHome$imAdvertisementSlider$hdnSliderTimeSpan" id="ctl00_PlaceHolderIMHome_imAdvertisementSlider_hdnSliderTimeSpan" value="5000" type="hidden">

                        <script type="text/javascript">

                            $(document).ready(function () {
                                var timeBetweenTransitions = $('#ctl00_PlaceHolderIMHome_imAdvertisementSlider_hdnSliderTimeSpan').val();
                                setupContentRotator('ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdSlider', 'ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdPager', timeBetweenTransitions);

                            });

                            function setupContentRotator(sliderId, pagerId) {
                                var sliderControl = $('#' + sliderId);
                                var arrSlides = new Array();
                                var pagerControl = $('#' + pagerId);
                                var Imagetitle;

                                if (sliderControl.children().length > 1) {
                                    sliderControl.cycle({
                                        pager: '#' + pagerId,
                                        cleartype: 1,
                                        pagerClick: function () {
                                            sliderControl.cycle('pause');
                                        },
                                        after: function (isNext, idx, slide) {
                                            Imagetitle = $('A', this)[0].title;
                                            if ($.inArray(Imagetitle, arrSlides) <= -1) {
                                                var v1 = $(this);
                                                cleanupWebtrends();
                                                dcsMultiTrack('DCS.dcsuri', '/Marketing Placement', 'WT.ad', $('A', this)[0].title, 'WT.dl', '50');
                                                arrSlides.push(Imagetitle);
                                            }
                                        }
                                    });
                                    pagerControl.css('left', sliderControl.width() / 2 - pagerControl.width() / 2 + 'px');
                                }
                                else if (sliderControl.children().length == 1) {
                                    pagerControl.hide();
                                    cleanupWebtrends();
                                    var imagetitle = '';
                                    if (sliderControl.children()[0].children.length > 0) {
                                        imagetitle = sliderControl.children()[0].children[0].title;
                                    }
                                    dcsMultiTrack('DCS.dcsuri', '/Marketing Placement', 'WT.ad', imagetitle, 'WT.dl', '50');

                                }
                                else {
                                    pagerControl.hide();
                                }
                            }
                        </script>

                        <%--CMS insertion in this div, will add id="adrotator" to identify along with custom rotator js --%>
                        <div class="main-block ad-rotator">
                            <div class="content">
                                <div id="ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdRotator" class="image-slider">

                                    <div style="left: 321.5px;" id="ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdPager" class="image-pager">

                                        <a class="" href="#">1</a><a class="" href="#">2</a><a class="" href="#">3</a><a class="activeSlide" href="#">4</a><a class="" href="#">5</a>
                                    </div>
                                    <div style="position: relative; width: 718px; height: 151px;" id="ctl00_PlaceHolderIMHome_imAdvertisementSlider_pnlAdSlider" class="image-slideshow">

                                        <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 5; opacity: 0; width: 718px; height: 151px;">
                                            <a href="http://www.ingrammicro.nl/concrete/index.php/klanten/category_stores/systems/can-touch-dinner-dark/" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_x-01768805_campagneblok-intel01','WT.dl','50'); " title="MOSS_x-01768805_campagneblok-intel01">
                                                <img src="../ScriptsImages/X-01768805_CAMPAGNEblok-Intel01.jpg"></a>
                                        </div>
                                        <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 5; opacity: 0; width: 718px; height: 151px;">
                                            <a href="http://www.ingrammicro.nl/concrete/index.php?cID=5574" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_m-01753115-campaignblock-hp_2','WT.dl','50'); " title="MOSS_m-01753115-campaignblock-hp_2">
                                                <img src="../ScriptsImages/M-01753115-campaignblock-HP_2.jpg"></a>
                                        </div>
                                        <div style="position: absolute; top: 0px; left: 0px; display: block; z-index: 5; opacity: 1; width: 718px; height: 151px;">
                                            <a href="http://www.ingrammicro.nl/concrete/index.php?cID=5853" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_m-01807407-campaignblock-microsoft_3','WT.dl','50'); " title="MOSS_m-01807407-campaignblock-microsoft_3">
                                                <img src="../ScriptsImages/M-01807407-Campaignblock-Microsoft_3.jpg"></a>
                                        </div>
                                        <div style="position: absolute; top: 0px; left: 0px; display: block; z-index: 6; opacity: 0; width: 718px; height: 151px;">
                                            <a href="https://www.implus.nl/site/?id=5929" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_m-01807414-campagneblok-microsoft_2','WT.dl','50'); " title="MOSS_m-01807414-campagneblok-microsoft_2">
                                                <img src="../ScriptsImages/M-01807414-campagneblok-Microsoft_2.jpg"></a>
                                        </div>
                                        <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 5; opacity: 0; width: 718px; height: 151px;">
                                            <a href="http://www.ingrammicro.nl/concrete/index.php?cID=5869" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_m-01809501-campagneblok-hpergotronnetgear-olympics','WT.dl','50'); " title="MOSS_m-01809501-campagneblok-hpergotronnetgear-olympics">
                                                <img src="../ScriptsImages/M-01809501-campagneblok-HPErgotronNetgear-Olympics.jpg"></a>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                buildSliders('#ctl00_PlaceHolderIMHome_ctl02_pnlRecoSlider');

                                $(".content-slider a[id*='productDetailsLink']").each(function () {
                                    $(this).addClass("tooltip-clickable-webtrends");
                                });
                                $(".content-slider p.product-name a").each(function () {
                                    $(this).addClass("tooltip-clickable-webtrends");
                                });
                                $(".tooltip-clickable-webtrends").click(function () {
                                    var href = $(this).attr('href');
                                    setTimeout(function () { window.location = href }, 3000);
                                    return false;
                                });
                            });
                        </script>

                        <div id="ctl00_PlaceHolderIMHome_ctl02_mainDiv" class="ingram-recommendations">
                            <div class="blue-header">
                                <h2>Ingram Micro Recommendations</h2>
                                <a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/MyOffers.aspx?zone=HOME_PAGE_BR_NAME">View All</a>
                            </div>

                            <div id="ctl00_PlaceHolderIMHome_ctl02_contentDiv" class="content">
                                <div id="ctl00_PlaceHolderIMHome_ctl02_pnlRecoSlider" class="content-slider">

                                    <div style="overflow: hidden;" class="slide-container">

                                        <div style="width: 715px;" class="display-control">
                                            <table style="float: left; width: 715px;" class="slide">
                                                <tbody>
                                                    <tr>
                                                        <td class="first-cell">
                                                            <a class="tooltip-clickable-webtrends" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_productDetailsLink" onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=PE02@@4500@@10@@000000000002424659">

                                                                <img src="../ScriptsImages/1024862971.jpg" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductImageControl1_imgProduct" onerror="javascript:setDefaultImageOnError(defaultImageURL,this,this.parentNode );setVisibleLnk(this, defaultImageURL);" width="95">
                                                            </a>
                                                            <div class="adj-height">

                                                                <p style="height: 42px; overflow: auto;" class="product-name">
                                                                    <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink" class="tooltip-clickable tooltip-clickable-webtrends" onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=PE02@@4500@@10@@000000000002424659"><span class="italic">MOTOROLA</span>&nbsp;ET1 WWAN 7INCH 1G/4G+4GSD</a>
                                                                </p>
                                                                <div class="in-stock-details" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink-info" visible="False">
                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductTitleLinkControl1$PTLCWebTrendsSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductTitleLinkControl1_PTLCWebTrendsSKU" value="2424659" type="hidden">

                                                                    <p>
                                                                        Motorola 8 GB Tablet - 17.8 cm (7") - 1 GHz - 1 GB RAM - Android
 2.3.4 Gingerbread - Slate - 1024 x 600 Multi-touch Screen Display (LED 
Backlight) - Bluetooth
                                                                    </p>
                                                                </div>

                                                                <p style="height: 15px; overflow: auto;" class="vpn-num">
                                                                    VPN: <span id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_VPN">ET1N2-7G2V1UEU</span>
                                                                </p>

                                                                <div class="pricing" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl_PNA_000000000002424659">
                                                                    <div class="centered-btns-container">
                                                                        <div class="centered-btns">

                                                                            <input id="e4a-000000000002424659" value="true" type="hidden">
                                                                            <div id="pai-000000000002424659" class="tooltip hide alertContainer"><a href="https://nl.ingrammicro.com/javascript%20void%280%29" class="stock-alert-icon tooltip"></a></div>
                                                                            <div id="pai-000000000002424659-info" class="hide">Click to subscribe to a stock alert.</div>
                                                                            <div style="height: 12px; overflow: auto;" class="in-stock tooltip" id="in-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl001singleProductHighlightControl1ProductPriceControl11StockControl1">
                                                                                <span class="warehouse-tip"></span>
                                                                                <span id="Span2">178 In Stock </span>
                                                                                <div class="eta" id="lblStock"></div>
                                                                            </div>

                                                                            <div class="in-stock-details" id="in-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl001singleProductHighlightControl1ProductPriceControl11StockControl1-info" visible="True">
                                                                                <table class="in-stock-details-table" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$StockControl1_StockPopUp_000000000002424659">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <th class="warehouse-header">Warehouse</th>
                                                                                            <th class="stock-header">Stock</th>
                                                                                            <th class="on-order-header">On Order</th>
                                                                                            <th class="eta-header">ETA</th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Centraal Magazijn (Straubing, Duitsland) </td>
                                                                                            <td align="center">178</td>
                                                                                            <td>0</td>
                                                                                            <td>13/03/2014</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <div class="warehouse-container" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$StockControl1_StockPopUpCIDiv_000000000002424659">
                                                                                    <p>
                                                                                        Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div id="inline-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl001singleProductHighlightControl1ProductPriceControl11StockControl1" class="plantStock">
                                                                                <div style="height: 31px;" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$StockControl1inline-stock-000000000002424659">
                                                                                    <div class="plantDetails">
                                                                                        <p><span>178</span><span> Centraal</span></p>
                                                                                        <p><span>(ETA: 13/03/2014)</span> </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                            <div class="clear">&nbsp;</div>
                                                                            <div class="pricing-info">
                                                                                <span class="your-price notax">
                                                                                    <span style="height: 14px; overflow: auto;" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_lblPrice">€ 765,18</span> &nbsp; 
                <span style="height: 14px; overflow: auto;" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_lblExclGST" class="excl-tax">Excl btw</span>
                                                                                </span>
                                                                                <span class="your-price" style="padding-top: 1px; margin-top: 0px"></span>

                                                                            </div>

                                                                            <div class="centered-btns-container">
                                                                                <div class="centered-btns add-product">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$AmountTextBox" value="1" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_AmountTextBox" class="amount" type="text"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$HiddenSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_HiddenSKU" value="000000000002424659" type="hidden">
                                                                                    <span id="addtobasket-000000000002424659">
                                                                                        <input style="border-width: 0px;" onclick="return false;" class="add-cart-btn" value="" type="submit">
                                                                                    </span>
                                                                                    <div id="icon-tool-tip-000000000002424659" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000002424659-Icon" class="tooltip">
                                                                                            <span class="in-cart-tip hide"></span>
                                                                                        </a>

                                                                                        <div id="addtobasket-000000000002424659-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000002424659" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000002424659-Msg" class="tooltip">
                                                                                            <span class="in-cart-tip hide"></span>
                                                                                        </a>

                                                                                        <div id="addtobasket-000000000002424659-Msg-info" class="hide add-to-bsk-msg-000000000002424659">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000002424659"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000002424659"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>


                                                                                    <input value="" class="hidden-count-item-000000000002424659" type="hidden">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$WebTrendsSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_WebTrendsSKU" value="2424659" type="hidden">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$IsCentralSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_IsCentralSKU" type="hidden">
                                                                                    <div class="clear">&nbsp;</div>
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$Currency" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_Currency" value="EUR" type="hidden">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$WebtrendsEnabled" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_WebtrendsEnabled" value="True" type="hidden">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$LocalCountryCode" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_LocalCountryCode" value="NL" type="hidden">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl00$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$IsPriceableSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_IsPriceableSKU" value="true" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="0" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="178" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">

                                                                                    <div id="addtobasket-000000000002424659-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p>This product is shipped directly from our central European warehouse and may take 2 business working days to be delivered</p>
                                                                                    </div>
                                                                                </div>
                                                                        
                                                                        
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="add-favorite">
                                                                        <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl00_singleProductHighlightControl_ProductPriceControl1_ProductAddToFavouritesControl1_lnk_AddToFavourites" class="add-fav" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000002424659">+ Add to Favourites</a>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>

                                                            <div class="specific-details">
                                                                <div class="special-flags">
                                                                    <a id="quantity-break-flag-000000000002424659" class="quantity-break product-sku-flag" href="#000000000002424659" style="display: none;" rel="#qb-singleproduct-000000000002424659-info">Quantity Breaks
                                                                    </a>
                                                                    <div class="hide" id="qb-singleproduct-000000000002424659-info"></div>
                                                                    <a id="sp-price-flag-000000000002424659" class="special-pricing product-sku-flag tooltip" rel="sp-available" href="#">Special Pricing
                                                                    </a>
                                                                    <div id="sp-available-info" class="hide">
                                                                        <p>Discounts and/or offers apply to this item.</p>
                                                                    </div>
                                                                    <a id="web-disc-flag-000000000002424659" class="web-discount product-sku-flag tooltip" style="display: none;" rel="web-discount" href="#">Web Only Price
                                                                    </a>
                                                                    <div id="web-discount-info" class="hide">
                                                                        <p>This price is only available for products purchased on the web</p>
                                                                    </div>

                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>

                                                            <div class="icons">
                                                            </div>

                                                        </td>
                                                        <td class="dotted-divider"></td>

                                                        <td class="second-cell">
                                                            <a class="tooltip-clickable-webtrends" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_productDetailsLink" onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=PE02@@4500@@10@@000000000001019938">
                                                           
                                                                <img src="../ScriptsImages/1016714833.jpg" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductImageControl1_imgProduct" onerror="javascript:setDefaultImageOnError(defaultImageURL,this,this.parentNode );setVisibleLnk(this, defaultImageURL);" width="95">
                                                            </a>
                                                            <div class="adj-height">

                                                                <p style="height: 42px; overflow: auto;" class="product-name">
                                                                    <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink" class="tooltip-clickable tooltip-clickable-webtrends" onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=PE02@@4500@@10@@000000000001019938"><span class="italic">DATALOGIC</span>&nbsp;POWERSCAN M8300 433MHZ</a>
                                                                </p>
                                                                <div class="in-stock-details" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink-info" visible="False">
                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductTitleLinkControl1$PTLCWebTrendsSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_PTLCWebTrendsSKU" value="1019938" type="hidden">

                                                                    <p>
                                                                        Datalogic PowerScan M8300 Handheld Barcode Scanner - Wireless - 35 scan/s - 50 m Scan Distance - Laser
                                                                    </p>
                                                                </div>

                                                                <p style="height: 15px; overflow: auto;" class="vpn-num">
                                                                    VPN: <span id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_VPN">PM8300-433</span>
                                                                </p>

                                                                <div class="pricing" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl_PNA_000000000001019938">

                                                                    <div class="centered-btns-container">
                                                                        <div class="centered-btns">

                                                                            <input id="e4a-000000000001019938" value="true" type="hidden">
                                                                            <div id="pai-000000000001019938" class="tooltip hide alertContainer"><a href="https://nl.ingrammicro.com/javascript%20void%280%29" class="stock-alert-icon tooltip"></a></div>
                                                                            <div id="pai-000000000001019938-info" class="hide">Click to subscribe to a stock alert.</div>
                                                                            <div style="height: 12px; overflow: auto;" class="in-stock tooltip" id="in-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl011singleProductHighlightControl1ProductPriceControl11StockControl1">
                                                                                <span class="warehouse-tip"></span>
                                                                                <span id="Span3">87 In Stock </span>
                                                                                <div class="eta" id="Div11"></div>
                                                                            </div>

                                                                            <div class="in-stock-details" id="in-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl011singleProductHighlightControl1ProductPriceControl11StockControl1-info" visible="True">
                                                                                <table class="in-stock-details-table" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$StockControl1_StockPopUp_000000000001019938">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <th class="warehouse-header">Warehouse</th>
                                                                                            <th class="stock-header">Stock</th>
                                                                                            <th class="on-order-header">On Order</th>
                                                                                            <th class="eta-header">ETA</th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Centraal Magazijn (Straubing, Duitsland) </td>
                                                                                            <td align="center">87</td>
                                                                                            <td>0</td>
                                                                                            <td>19/02/2014</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <div class="warehouse-container" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$StockControl1_StockPopUpCIDiv_000000000001019938">
                                                                                    <p>
                                                                                        Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div id="inline-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl011singleProductHighlightControl1ProductPriceControl11StockControl1" class="plantStock">
                                                                                <div style="height: 31px;" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$StockControl1inline-stock-000000000001019938">
                                                                                    <div class="plantDetails">
                                                                                        <p><span>87</span><span> Centraal</span></p>
                                                                                        <p><span>(ETA: 19/02/2014)</span> </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                            <div class="clear">&nbsp;</div>
                                                                            <div class="pricing-info">
                                                                                <span class="your-price notax">
                                                                                    <span style="height: 14px; overflow: auto;" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_lblPrice">€ 407,44</span> &nbsp; 
                <span style="height: 14px; overflow: auto;" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_lblExclGST" class="excl-tax">Excl btw</span>
                                                                                </span>
                                                                                <span class="your-price" style="padding-top: 1px; margin-top: 0px"></span>

                                                                            </div>

                                                                            <div class="centered-btns-container">
                                                                                <div class="centered-btns add-product">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$AmountTextBox" value="1" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_AmountTextBox" class="amount" type="text"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$HiddenSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_HiddenSKU" value="000000000001019938" type="hidden">
                                                                                    <span id="addtobasket-000000000001019938">
                                                                                        <input style="border-width: 0px;" onclick="return false;" class="add-cart-btn" value="" type="submit">
                                                                                    </span>
                                                                                    <div id="icon-tool-tip-000000000001019938" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000001019938-Icon" class="tooltip">
                                                                                            <span class="in-cart-tip hide"></span>
                                                                                        </a>

                                                                                        <div id="addtobasket-000000000001019938-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000001019938" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000001019938-Msg" class="tooltip">
                                                                                            <span class="in-cart-tip hide"></span>
                                                                                        </a>

                                                                                        <div id="addtobasket-000000000001019938-Msg-info" class="hide add-to-bsk-msg-000000000001019938">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000001019938"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000001019938"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <input value="" class="hidden-count-item-000000000001019938" type="hidden">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$WebTrendsSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_WebTrendsSKU" value="1019938" type="hidden"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$IsCentralSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_IsCentralSKU" type="hidden">
                                                                                    <div class="clear">&nbsp;</div>
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$Currency" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_Currency" value="EUR" type="hidden"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$WebtrendsEnabled" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_WebtrendsEnabled" value="True" type="hidden"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$LocalCountryCode" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_LocalCountryCode" value="NL" type="hidden"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$IsPriceableSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_IsPriceableSKU" value="true" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="0" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="87" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">

                                                                                    <div id="addtobasket-000000000001019938-info" style="visibility: hidden" class="in-stock-details">
                                                                                        <p>This product is shipped directly from our central European warehouse and may take 2 business working days to be delivered</p>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="add-favorite">
                                                                        <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToFavouritesControl1_lnk_AddToFavourites" class="add-fav" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000001019938">+ Add to Favourites</a>
                                                                    </div>

                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>

                                                            <div class="specific-details">
                                                                <div class="special-flags">
                                                                    <a id="quantity-break-flag-000000000001019938" class="quantity-break product-sku-flag" href="#000000000001019938" style="display: none;" rel="#qb-singleproduct-000000000001019938-info">Quantity Breaks
                                                                    </a>

                                                                    <div class="hide" id="qb-singleproduct-000000000001019938-info"></div>

                                                                    <a id="sp-price-flag-000000000001019938" class="special-pricing product-sku-flag tooltip" rel="sp-available" href="#">Special Pricing
                                                                    </a>
                                                                    <div id="Div13" class="hide">
                                                                        <p>Discounts and/or offers apply to this item.</p>
                                                                    </div>
                                                                    <a id="web-disc-flag-000000000001019938" class="web-discount product-sku-flag tooltip" style="display: none;" rel="web-discount" href="#">Web Only Price
                                                                    </a>
                                                                    <div id="Div14" class="hide">
                                                                        <p>This price is only available for products purchased on the web</p>
                                                                    </div>
                                                                    <div class="clear"></div>

                                                                </div>
                                                            </div>

                                                            <div class="icons">
                                                            </div>
                                                        </td>

                                                        <td class="dotted-divider"></td>

                                                        <td class="third-cell">
                                                            <a class="tooltip-clickable-webtrends" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_productDetailsLink" onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000002310333">
                   
                                                                <img src="../ScriptsImages/1023868752.jpg" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductImageControl1_imgProduct" onerror="javascript:setDefaultImageOnError(defaultImageURL,this,this.parentNode );setVisibleLnk(this, defaultImageURL);" width="95">
                                                            </a>

                                                            <div class="adj-height">

                                                                <p style="height: 42px; overflow: auto;" class="product-name">
                                                                    <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink" class="tooltip-clickable tooltip-clickable-webtrends" onclick="getClickTime(event);doWebtrends('IMRPClick',this,'HomePage');" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000002310333"><span class="italic">LEXMARK</span>&nbsp;Laser Mono MS510dn 42ppm USB/ENet 250Sh</a>
                                                                </p>
                                                                <div class="in-stock-details" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink-info" visible="False">
                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductTitleLinkControl1$PTLCWebTrendsSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductTitleLinkControl1_PTLCWebTrendsSKU" value="2310333" type="hidden">

                                                                    <p>
                                                                        Lexmark MS510DN Laser Printer - Monochrome - 1200 x 1200 dpi 
Print - Plain Paper Print - Desktop - 42 ppm Mono Print - 350 sheets 
Input - Automatic Duplex Print - LCD - Gigabit Ethernet - USB
                                                                    </p>
                                                                </div>

                                                                <p style="height: 15px; overflow: auto;" class="vpn-num">
                                                                    VPN: <span id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_VPN">35S0330</span>
                                                                </p>

                                                                <div class="pricing" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl_PNA_000000000002310333">

                                                                    <div class="centered-btns-container">
                                                                        <div class="centered-btns">

                                                                            <input id="e4a-000000000002310333" value="true" type="hidden">
                                                                            <div id="pai-000000000002310333" class="tooltip hide alertContainer"><a href="https://nl.ingrammicro.com/javascript%20void%280%29" class="stock-alert-icon tooltip"></a></div>
                                                                            <div id="pai-000000000002310333-info" class="hide">Click to subscribe to a stock alert.</div>
                                                                            <div style="height: 12px; overflow: auto;" class="in-stock tooltip" id="in-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl021singleProductHighlightControl1ProductPriceControl11StockControl1">
                                                                                <span class="warehouse-tip"></span>
                                                                                <span id="Span4">44 In Stock </span>
                                                                                <div class="eta" id="Div15"></div>
                                                                            </div>

                                                                            <div class="in-stock-details" id="in-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl021singleProductHighlightControl1ProductPriceControl11StockControl1-info" visible="True">
                                                                                <table class="in-stock-details-table" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$StockControl1_StockPopUp_000000000002310333">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <th class="warehouse-header">Warehouse</th>
                                                                                            <th class="stock-header">Stock</th>
                                                                                            <th class="on-order-header">On Order</th>
                                                                                            <th class="eta-header">ETA</th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                            <td align="center">44</td>
                                                                                            <td></td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <div class="warehouse-container" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$StockControl1_StockPopUpCIDiv_000000000002310333">
                                                                                    <p>
                                                                                        Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div id="inline-stock-ctl001PlaceHolderIMHome1ctl021RecommendedRepeater1ctl021singleProductHighlightControl1ProductPriceControl11StockControl1" class="plantStock">
                                                                                <div style="height: 31px;" id="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$StockControl1inline-stock-000000000002310333">
                                                                                    <div class="plantDetails">
                                                                                        <p><span>44</span><span> Lokaal</span></p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                            <div class="clear">&nbsp;</div>
                                                                            <div class="pricing-info">
                                                                                <span class="your-price notax">
                                                                                    <span style="height: 14px; overflow: auto;" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_lblPrice">€ 259,75</span> &nbsp; 
                <span style="height: 14px; overflow: auto;" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_lblExclGST" class="excl-tax">Excl btw</span>
                                                                                </span>
                                                                                <span class="your-price" style="padding-top: 1px; margin-top: 0px"></span>

                                                                            </div>

                                                                            <div class="centered-btns-container">
                                                                                <div class="centered-btns add-product">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$AmountTextBox" value="1" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_AmountTextBox" class="amount" type="text"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$HiddenSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_HiddenSKU" value="000000000002310333" type="hidden">
                                                                                    <span id="addtobasket-000000000002310333">
                                                                                        <input style="border-width: 0px;" onclick="return false;" class="add-cart-btn" value="" type="submit">
                                                                                    </span>
                                                                                    <div id="icon-tool-tip-000000000002310333" class="in-cart-msg">
                                                                                        <a href="#" rel="addtobasket-000000000002310333-Icon" class="tooltip">
                                                                                            <span class="in-cart-tip hide"></span>
                                                                                        </a>

                                                                                        <div id="addtobasket-000000000002310333-Icon-info" class="hide">
                                                                                            <p>There are&nbsp;&nbsp;units of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="add-to-basket-msg-000000000002310333" class="in-cart-msg" style="display: none;">
                                                                                        <a href="#" rel="addtobasket-000000000002310333-Msg" class="tooltip">
                                                                                            <span class="in-cart-tip hide"></span>
                                                                                        </a>

                                                                                        <div id="addtobasket-000000000002310333-Msg-info" class="hide add-to-bsk-msg-000000000002310333">
                                                                                            <p>You have added&nbsp;<span class="totalCountValue-000000000002310333"></span>&nbsp;of this item. There is now total of&nbsp;<span class="countValue-000000000002310333"></span>&nbsp;of this item in your active basket.</p>
                                                                                        </div>
                                                                                    </div>

                                                                                    <input value="" class="hidden-count-item-000000000002310333" type="hidden">
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$WebTrendsSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_WebTrendsSKU" value="2310333" type="hidden"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$IsCentralSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_IsCentralSKU" type="hidden">
                                                                                    <div class="clear">&nbsp;</div>
                                                                                    <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$Currency" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_Currency" value="EUR" type="hidden"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$WebtrendsEnabled" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_WebtrendsEnabled" value="True" type="hidden"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$LocalCountryCode" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_LocalCountryCode" value="NL" type="hidden"><input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$IsPriceableSKU" id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_IsPriceableSKU" value="true" type="hidden">
                                                                                    <input class="hdnClassCode" value="" type="hidden">
                                                                                    <input class="hdnLocalStock" value="44" type="hidden">
                                                                                    <input class="hdnCurrentStock" value="44" type="hidden">
                                                                                    <input class="hdnIsReturnable" value="true" type="hidden">

                                                                                    <div id="addtobasket-000000000002310333-info" style="visibility: hidden" class="in-stock-details"></div>
                                                                                </div>
                                                                                <input name="ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$hiddenTargetControlForAddToBasketFailurePopup" value="" onclick='javascript: WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$PlaceHolderIMHome$ctl02$RecommendedRepeater$ctl02$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$hiddenTargetControlForAddToBasketFailurePopup", "", true, "", "", false, false))' id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_hiddenTargetControlForAddToBasketFailurePopup" style="display: none;" type="submit"><div id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_panMaxBasketItemsReached" class="modal-popup modal-popup-b medium-small-popup" style="display: none; position: fixed; z-index: 100001;">

                                                                                    <div class="container delete-basket">
                                                                                        <div class="error-top">
                                                                                            <div class="warning-msg">
                                                                                                <img src="../ScriptsImages/icon-alert.gif" alt="">
                                                                                                <div class="msg">
                                                                                                    <p>Unable to  Add Item to Basket</p>
                                                                                                    <p>
                                                                                                        The current basket has reached the maximum number 
of line items (100). You will have to delete one item from the basket in
 order to add a new one. Alternatively, you can create a new basket.
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="buttons-container">
                                                                                            <div class="centered-btns-container">
                                                                                                <div class="centered-btns">
                                                                                                    <a href="#" class="single-cancel-btn hide-addtobasket-failure-popup" style="top: 0px">Close</a>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="clear"></div>
                                                                                        </div>
                                                                                        <a href="#" class="cancel-btn-top-no-text hide-addtobasket-failure-popup">
                                                                                            <img id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_Image1" src="../ScriptsImages/my_dashboard_x.gif" alt="Close" style="border-width: 0px;"></a>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="modalBackground" style="display: none; position: fixed; left: 0px; top: 0px; z-index: 10000;" id="Div16"></div>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="add-favorite">
                                                                        <a id="ctl00_PlaceHolderIMHome_ctl02_RecommendedRepeater_ctl02_singleProductHighlightControl_ProductPriceControl1_ProductAddToFavouritesControl1_lnk_AddToFavourites" class="add-fav" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/FavouriteProducts.aspx?productId=000000000002310333">+ Add to Favourites</a>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>

                                                            <div class="specific-details">
                                                                <div class="special-flags">
                                                                    <a id="quantity-break-flag-000000000002310333" class="quantity-break product-sku-flag" href="#000000000002310333" style="display: none;" rel="#qb-singleproduct-000000000002310333-info">Quantity Breaks
                                                                    </a>
                                                                    <div class="hide" id="qb-singleproduct-000000000002310333-info"></div>
                                                                    <a id="sp-price-flag-000000000002310333" class="special-pricing product-sku-flag tooltip" rel="sp-available" href="#">Special Pricing
                                                                    </a>
                                                                    <div id="Div17" class="hide">
                                                                        <p>Discounts and/or offers apply to this item.</p>
                                                                    </div>
                                                                    <a id="web-disc-flag-000000000002310333" class="web-discount product-sku-flag tooltip" style="display: none;" rel="web-discount" href="#">Web Only Price
                                                                    </a>
                                                                    <div id="Div18" class="hide">
                                                                        <p>This price is only available for products purchased on the web</p>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="icons">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="fourth-cell"></td>
                                                        <td class="dotted-divider"></td>

                                                        <td class="fifth-cell"></td>
                                                        <td class="dotted-divider"></td>

                                                        <td class="sixth-cell"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                    <div></div>
                                    <div style="cursor: default" class="slider-control left-control">
                                        <div class="tab-top-left"></div>
                                        <div class="tab-left control-off"></div>
                                        <div class="tab-bottom-left"></div>
                                    </div>
                                    <div style="cursor: default" class="slider-control right-control">
                                        <div class="tab-top-right"></div>
                                        <div class="tab-right control-off"></div>
                                        <div class="tab-bottom-right"></div>
                                    </div>
                                    <div class="clear"></div>

                                </div>
                            </div>
                        </div>

                        <div class="viewing-history">
                            <div class="blue-header">
                                <h3>Recent Viewing History</h3>
                                <a id="ctl00_PlaceHolderIMHome_ctl03_HorizontalViewAll" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/RecentlyViewedProducts.aspx">View All</a>

                            </div>
                            <div class="products">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="inner-spacing">
                                                    <a id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_productDetailsLink" onclick="getClickTime(event);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001619911">
                    
                                                        <img src="../ScriptsImages/1020007293.jpg" id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_ProductImageControl1_imgProduct" onerror="javascript:setDefaultImageOnError(defaultImageURL,this,this.parentNode );setVisibleLnk(this, defaultImageURL);" width="95">
                                                    </a>
                                                    <div class="adj-height">

                                                        <p style="height: 42px; overflow: auto;" class="product-name">
                                                            <a id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink" class="tooltip-clickable" onclick="getClickTime(event);" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/ProductDetails.aspx?id=NL01@@4500@@10@@000000000001619911"><span class="italic">STARTECH</span>&nbsp;MINI DISPLAYPORT TO DVI ACTIVE ADAPTER</a>
                                                        </p>
                                                        <div class="in-stock-details" id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_ProductDetailsLink-info" visible="False">
                                                            <input name="ctl00$PlaceHolderIMHome$ctl03$RecentRepeater$ctl01$singleProductHighlightControl$ProductTitleLinkControl1$PTLCWebTrendsSKU" id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_ProductTitleLinkControl1_PTLCWebTrendsSKU" value="1619911" type="hidden">

                                                            <p>
                                                                StarTech.com MDP2DVIS Video Cable for Monitor, Audio/Video 
Device, TV - Mini DisplayPort Male Digital Audio/Video - DVI-D 
(Single-Link) Female Digital Video - Black
                                                            </p>
                                                        </div>

                                                        <p style="height: 15px; overflow: auto;" class="vpn-num">
                                                            VPN: <span id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_VPN">MDP2DVIS</span>
                                                        </p>

                                                        <div class="pricing" id="ctl00$PlaceHolderIMHome$ctl03$RecentRepeater$ctl01$singleProductHighlightControl_PNA_000000000001619911">
                                                            <div class="centered-btns-container">
                                                                <div class="centered-btns">
                                                                    <input id="e4a-000000000001619911" value="true" type="hidden">
                                                                    <div id="pai-000000000001619911" class="tooltip hide alertContainer"><a href="https://nl.ingrammicro.com/javascript%20void%280%29" class="stock-alert-icon tooltip"></a></div>
                                                                    <div id="pai-000000000001619911-info" class="hide">Click to subscribe to a stock alert.</div>
                                                                    <div style="height: 12px; overflow: auto;" class="in-stock tooltip" id="in-stock-ctl001PlaceHolderIMHome1ctl031RecentRepeater1ctl011singleProductHighlightControl1ProductPriceControl11StockControl1">
                                                                        <span class="warehouse-tip"></span>
                                                                        <span id="Span5">44 In Stock </span>
                                                                        <div class="eta" id="Div19"></div>
                                                                    </div>

                                                                    <div class="in-stock-details" id="in-stock-ctl001PlaceHolderIMHome1ctl031RecentRepeater1ctl011singleProductHighlightControl1ProductPriceControl11StockControl1-info" visible="True">
                                                                        <table class="in-stock-details-table" id="ctl00$PlaceHolderIMHome$ctl03$RecentRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$StockControl1_StockPopUp_000000000001619911">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th class="warehouse-header">Warehouse</th>
                                                                                    <th class="stock-header">Stock</th>
                                                                                    <th class="on-order-header">On Order</th>
                                                                                    <th class="eta-header">ETA</th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Standaard Magazijn (Tilburg, Nederland) </td>
                                                                                    <td align="center">44</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <div class="warehouse-container" id="ctl00$PlaceHolderIMHome$ctl03$RecentRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$StockControl1_StockPopUpCIDiv_000000000001619911">
                                                                            <p>
                                                                                Local
 Warehouse items can be delivered in 24hrs after the order is received. 
Central Warehouse items can take from 3 to 5 days to reach the final 
delivery address
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div id="inline-stock-ctl001PlaceHolderIMHome1ctl031RecentRepeater1ctl011singleProductHighlightControl1ProductPriceControl11StockControl1" class="plantStock">
                                                                        <div style="height: 16px;" id="ctl00$PlaceHolderIMHome$ctl03$RecentRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$StockControl1inline-stock-000000000001619911">
                                                                            <div class="plantDetails">
                                                                                <p><span>44</span><span> Lokaal</span></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                    <div class="clear">&nbsp;</div>
                                                                    <div class="pricing-info">
                                                                        <span class="your-price notax">
                                                                            <span style="height: 14px; overflow: auto;" id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_lblPrice">€ 21,15</span> &nbsp; 
                <span style="height: 14px; overflow: auto;" id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_lblExclGST" class="excl-tax">Excl btw</span>
                                                                        </span>
                                                                        <span class="your-price" style="padding-top: 1px; margin-top: 0px"></span>

                                                                    </div>

                                                                    <div class="centered-btns-container">
                                                                        <div class="centered-btns add-product">
                                                                            <input name="ctl00$PlaceHolderIMHome$ctl03$RecentRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$AmountTextBox" value="1" id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_AmountTextBox" class="amount" type="text">
                                                                            <input name="ctl00$PlaceHolderIMHome$ctl03$RecentRepeater$ctl01$singleProductHighlightControl$ProductPriceControl1$ProductAddToBasketControl1$HiddenSKU" id="ctl00_PlaceHolderIMHome_ctl03_RecentRepeater_ctl01_singleProductHighlightControl_ProductPriceControl1_ProductAddToBasketControl1_HiddenSKU" value="000000000001619911" type="hidden">
                                                                            <span id="addtobasket-000000000001619911">
                                                                                <input style="border-width: 0px;" onclick="return false;" class="add-cart-btn" value="" type="submit">
                                                                            </span>
                                                                            <div id="icon-tool-tip-000000000001619911" class="in-cart-msg">
                                                                                <a href="#" rel="addtobasket-000000000001619911-Icon" class="tooltip">
                                                                                    <span class="in-cart-tip hide"></span>
                                                                                </a>

                                                                                <div id="addtobasket-000000000001619911-Icon-info" class="hide">
                                                                                    <p>There are&nbsp;&nbsp;units of this item in your active basket.</p>
                                                                                </div>
                                                                            </div>

                                                                            <input value="" class="hidden-count-item-000000000001619911" type="hidden">

                                                                            <div class="clear">&nbsp;</div>

                                                                            <input class="hdnClassCode" value="" type="hidden">
                                                                            <input class="hdnLocalStock" value="44" type="hidden">
                                                                            <input class="hdnCurrentStock" value="44" type="hidden">
                                                                            <input class="hdnIsReturnable" value="true" type="hidden">

                                                                            <div id="addtobasket-000000000001619911-info" style="visibility: hidden" class="in-stock-details"></div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="specific-details">
                                                        <div class="special-flags">
                                                            <a id="quantity-break-flag-000000000001619911" class="quantity-break product-sku-flag" href="#000000000001619911" style="display: none;" rel="#qb-singleproduct-000000000001619911-info">Quantity Breaks
                                                            </a>

                                                            <div class="hide" id="qb-singleproduct-000000000001619911-info"></div>

                                                            <a id="sp-price-flag-000000000001619911" class="special-pricing product-sku-flag tooltip" style="display: none;" rel="sp-available" href="#">Special Pricing
                                                            </a>

                                                            <a id="web-disc-flag-000000000001619911" class="web-discount product-sku-flag tooltip" style="display: none;" rel="web-discount" href="#">Web Only Price
                                                            </a>

                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="icons">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <a aria-disabled="false" role="button" id="ctl00_PlaceHolderIMHome_lbnOpenEndUserPopup" class="button popup-button end-user-button hide ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text">Select Vendor Special Bid Information</span></a>

                        <div class="modalBackground" style="display: none; position: fixed; left: 0px; top: 0px; z-index: 10000;" id="behaviorSpecialBid_backgroundElement"></div>
                    </div>
                </div>
            </div>

            <%--CMS insertion in this div, will use existing id="sidebar" to identify div--%>
            <div id="sidebar">

                    <div id="ctl00_PlaceHolderIMRightBar_divHomeBannerAdsSidebar" class="banner-ads sidebar-block">
                    <div class="ad">
                        <a onmousedown="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','ENDECA_mobilelright','WT.dl','50');" class="advertisementHperLinkNewWindow" href="http://www.ingrammicro.nl/concrete/index.php?cID=5041" target="_blank">
                            <img title="" src="../ScriptsImages/mobilelright.png" style="border-width: 0px;"></a>
                    </div>
                    <div class="divider">
                        <!---->
                    </div>
                    <div class="ad">
                        <a onmousedown="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','ENDECA_* Click2License_Homepage_RightAd_CTA','WT.dl','50');" class="advertisementHperLink1" href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=Aro:0,Np:All,A:pKeyMfrMaterial,N:224&amp;mnc=true%3f&amp;ADName=ENDECA_C2L">
                            <img title="" src="../ScriptsImages/113882-webspot50pxhoog-Click2License.jpg" style="border-width: 0px;"></a>
                    </div>
                    <div style="background: none repeat scroll 0% 0% transparent;" class="divider">
                        <!---->
                    </div>
                </div>

                <div class="banner-ads sidebar-block">
                    <div class="ad-top">
                        <div>
                            <a href="http://www.ingrammicro.nl/concrete/index.php?cID=5794" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_m-017686_03-webspot-intel','WT.dl','50'); " title="MOSS_m-017686_03-webspot-intel">
                                <img src="../ScriptsImages/M-017686_03-Webspot-Intel.jpg"></a>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="ad-top">
                        <div>
                            <a href="http://www.ingrammicro.nl/concrete/index.php?cID=5537" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_m-01779513-webspot--hp-supplies-','WT.dl','50'); " title="MOSS_m-01779513-webspot--hp-supplies-">
                                <img src="../ScriptsImages/M-01779513-Webspot--HP-Supplies-.jpg"></a>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="ad-top">
                        <div>
                            <a href="http://www.ingrammicro.nl/concrete/index.php/klanten/category_stores/networking/cisco-networking/cisco-smartnet-actie/" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_m-01441621-webspot-cisco','WT.dl','50'); " title="MOSS_m-01441621-webspot-cisco">
                                <img src="../ScriptsImages/M-01441621-webspot-Cisco.jpg"></a>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="ad-top">
                        <div>
                            <a href="http://www.ingrammicro.nl/concrete/index.php/klanten/category_stores/networking/hp-networking-store/hp-rok-software-speciaal-voor-hp-servers/" target="_blank" onclick="cleanupWebtrends();dcsMultiTrack('DCS.dcsuri', '/Marketing Placement','WT.ac','MOSS_x-030002_45-webspot-hp-en-microsoft','WT.dl','50'); " title="MOSS_x-030002_45-webspot-hp-en-microsoft">
                                <img src="../ScriptsImages/X-030002_45-webspot-HP-en-Microsoft.jpg"></a>
                        </div>
                    </div>
                    <div></div>
                </div>

                <div style="height: 292px;" class="filler" id="filler-div">
                    <div style="height: 280px;" class="inner"></div>
                </div>

            </div>
  
            <div id="footer">
                <ul id="ctl00_imFooter_Ul1">
                    <li id="ctl00_imFooter_Products">
                        <a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=Np:All" id="qtp_footer_search">Products
                        </a>
                    </li>

                    <li id="ctl00_imFooter_Vendors">
                        <a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/AllVendors.aspx?mnc=true">Vendors
                        </a>
                    </li>
                    <li id="ctl00_imFooter_CloudServices">
                        <a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/search2.aspx#PNavDS=N:244">Cloud Services
                        </a>
                    </li>

                    <li id="ctl00_imFooter_Services">
                        <a href="https://nl.ingrammicro.com/Pages/ServicesAndSupport.aspx" id="qtp_footer_support">Services &amp; Support
                        </a>
                    </li>

                    <li>
                        <a href="https://nl.ingrammicro.com/_layouts/CommerceServer/IM/AllNews.aspx" id="qtp_footer_news">News and Events
                        </a>
                    </li>
                    <li>
                        <a href="https://nl.ingrammicro.com/Pages/IGMContactUs.aspx" id="qtp_footer_contact">Contact Us
                        </a>
                    </li>
                    <li>
                        <a href="https://nl.ingrammicro.com/Pages/IMAboutIngramMicro.aspx" id="qtp_footer_about">About Us
                        </a>
                    </li>
                </ul>
                <div class="clear">
                </div>
                <div class="copyright" id="qtp_footer_copyright">
                    <div class="bottom-footer-right">
                    </div>
                    <div class="bottom-footer-left">
                        Copyright © 2014 Ingram Micro. All rights reserved.
            <span class="spacing">&nbsp;</span><!-- Phone -->
                    </div>

                </div>
            </div>

        </div>
    </form>

    <div id="ingramTooltipCover" class="tooltip-cover-container"></div>

    <div style="display: none;" id="tooltip">
        <h3></h3>
        <div class="body"></div>
        <div class="url"></div>
    </div>
    
</body>

</html>