/****** Object:  StoredProcedure [dbo].[IMCustomSP_GetContactGuid]    Script Date: 2/12/2014 7:33:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Darius Lezama
-- Create date:  2/3/2014
-- Description:	Retrieves records  tables
-- =============================================
CREATE PROCEDURE [dbo].[IMCustomSP_GetContactGuid]
	@SAMAccountName nvarchar(50),
	@SiteDisplayName nvarchar(200)
AS
BEGIN
 SET NOCOUNT ON;
 
 SELECT 
		c.ContactGUID as 'Guid' 
FROM
	   CMS_Site s with (nolock) join OM_Contact c with (nolock)
			on s.SiteID=c.ContactSiteID
WHERE
	  c.SAMAccountName=@SAMAccountName
	  and s.SiteDisplayName=@SiteDisplayName;
 
END
GO


