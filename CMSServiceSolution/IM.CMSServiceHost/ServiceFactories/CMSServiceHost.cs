﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace IM.CMSServiceHost.CMSServiceFactories
{
    public class CMSServiceHost: ServiceHost
    {
        public CMSServiceHost(Type serviceType, params Uri[] baseAddresses)
        : base(serviceType, baseAddresses) {
        }

    //Overriding ApplyConfiguration() allows us to 
    //alter the ServiceDescription or other properties if necessary prior to opening
    //the service host. 
    protected override void ApplyConfiguration()
    {
        //First, we call base.ApplyConfiguration()
        //to read any configuration that was provided for
        //the service we're hosting. After this call,
        //this.Description describes the service
        //as it was configured.
        base.ApplyConfiguration();     

        // Add custom code here - self hostng, drynamic changes of setting, custom endpoints, etc.

    }



    }
}
