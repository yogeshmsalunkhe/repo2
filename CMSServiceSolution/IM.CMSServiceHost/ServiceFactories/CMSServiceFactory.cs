﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;

using IM.CMSServiceHost;
using IM.ErrorLoggingUtilities;

namespace IM.CMSServiceHost.CMSServiceFactories
{
    //All the custom factory does is return a new instance
            //of our custom host class. The bulk of the custom logic should
            //live in the custom host (as opposed to the factory) 
            //for maximum reuse value outside of the IIS/WAS hosting environment.

    public class CMSServiceFactory: ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            ServiceHost serviceHost=new CMSServiceHost(serviceType,  baseAddresses);
            serviceHost.Opening += new EventHandler(hostOpeningEvent);
            return serviceHost;
        } 

        //this event happens whenever the host service starts/begins
        void hostOpeningEvent(object sender, EventArgs e)
        {
            //needed by kentico to be able to access ascx transformations of a target site. -- may not be necesarry
            // if this isn't being used ( you can also remove all kentico CMS.* references.)
            //CMS.IO.VirtualPathHelper.RegisterVirtualPathProvider();

            //needed to properly initialize enterprise libary error logging -- event log source name to create is passed
           ErrorLogging.SetupLoggingAndErrorHandling(Constants.ApplicationName);
        } 
    }
}
