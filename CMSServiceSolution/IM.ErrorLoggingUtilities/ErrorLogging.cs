﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

namespace IM.ErrorLoggingUtilities
{
    public class ErrorLogging
    {
        public static void SetupLoggingAndErrorHandling(string sourceName)
        {
            // uses app.config/web.config file to generate settings for the configuration object
            IConfigurationSource configurationSource = ConfigurationSourceFactory.Create();
            LogWriterFactory logWriterFactory = new LogWriterFactory(configurationSource);
            Logger.SetLogWriter(logWriterFactory.Create());

            var exceptionHandlerFactory = new ExceptionPolicyFactory(configurationSource);
            ExceptionPolicy.SetExceptionManager(exceptionHandlerFactory.CreateManager());

            //Note: you will get a security error here if the account used to run this assembly does not have 
            //  sufficient righs to even read the event log.
            if (!EventLog.SourceExists(sourceName))
            {
                EventLog.CreateEventSource(
                    sourceName, null); //will create source in Application log.
            }
        }

        public static void HandleException(Exception ex, string policy)
        {
            bool rethrow = false;
            try
            {
                // note depending on policy  rethrow settings this can cause application to stop on unhandled errors
                rethrow = ExceptionPolicy.HandleException(ex, policy);
            }
            catch (Exception innerEx)
            {
                string errorMsg = "An unexpected exception occurred while " +
                  "calling HandleException with policy'" + policy + "'. ";
                errorMsg += Environment.NewLine + innerEx.ToString();
                throw ex;
            }

            if (rethrow)
            {
                //WARNING: this will truncate the stack of the exception 
                throw ex;
            }
        }
    }
}
