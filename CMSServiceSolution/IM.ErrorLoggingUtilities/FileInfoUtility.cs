﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IM.ErrorLoggingUtilities
{
    public class FileInfoUtility
    {
        private static object syncRoot = new Object();
        private static volatile Dictionary<string, string> fileMimeType;

        internal static Dictionary<string, string> FileMimeType
        {
            get
            {
                if (fileMimeType == null)
                {
                    lock (syncRoot)
                    {
                        if (fileMimeType == null)
                        {
                            fileMimeType = new Dictionary<string, string>(){
                                {".xls", "application/vnd.ms-excel"},
                                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}
                            };
                        }
                    }
                }
                return fileMimeType;
            }
        }

        public static string GetMimeTypeByExtension(string fileExtension)
        {
            if (FileMimeType.ContainsKey(fileExtension))
                return FileMimeType[fileExtension];

            return string.Empty;
        }
    }
}
