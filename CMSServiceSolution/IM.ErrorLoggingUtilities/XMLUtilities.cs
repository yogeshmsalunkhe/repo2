﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Text;

namespace IM.ErrorLoggingUtilities
{

    public class XMLHelper
    {
        public static string Serialize<T>(T item)
        {
            // serialize Array object to xml here
            XmlSerializer x = new XmlSerializer(item.GetType());
            StringBuilder sb = new StringBuilder();
            using (StringWriterWithEncoding writer = new StringWriterWithEncoding(sb, new UTF8Encoding()))
            {
                x.Serialize(writer, item);
            }

            XmlDocument doc = new XmlDocument();

            // remove encoding node - let db handle this.
            doc.LoadXml(sb.ToString());
            foreach (XmlNode node in doc)
            {
                if (node.NodeType == XmlNodeType.XmlDeclaration)
                {
                    doc.RemoveChild(node);
                    break;
                }
            }
            return doc.OuterXml;
        }

        public static object DeSerialize(string sb, Type t)
        {
            XmlSerializer x = new XmlSerializer(t);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
            string strXML = doc.InnerXml;// Read XML in string
            StringReader strReader = new StringReader(strXML);
            return (object)x.Deserialize(strReader);
        }
     }

   public class StringWriterWithEncoding : StringWriter
   {
            Encoding encodingSetting;

            public StringWriterWithEncoding( StringBuilder builder, Encoding encoding ): base( builder )
                {
                   encodingSetting = encoding;
                }

            public override Encoding Encoding
            {
                get { return encodingSetting; }
            }
    }

}
