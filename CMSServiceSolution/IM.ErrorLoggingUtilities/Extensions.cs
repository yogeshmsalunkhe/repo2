﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace IM.ErrorLoggingUtilities
{
    public static class Extensions
    {
        public static T TryParse<T>(this IConvertible value, T defaultValue)
        {
            try
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch
            {
                return defaultValue;
            }
        }

        // Used to fetch data of particular field from data row. 
        public static T GetValue<T>(this DataRow row, string fieldName)
        {
            T result = default(T);
            if (row.Table.Columns.Contains(fieldName) && row[fieldName] != DBNull.Value)
            {
                if (typeof(T) == typeof(string))
                {
                    result = (T)Convert.ChangeType(row[fieldName], typeof(string));
                }
                if (typeof(T) == typeof(int))
                {
                    result = (T)Convert.ChangeType(row[fieldName], typeof(int));
                }
                if (typeof(T) == typeof(bool))
                {
                    result = (T)Convert.ChangeType(row[fieldName], typeof(bool));
                }
                if (typeof(T) == typeof(DateTime))
                {
                    result = (T)Convert.ChangeType(row[fieldName], typeof(DateTime));
                }
            }
            return result;
        }
    }
}
