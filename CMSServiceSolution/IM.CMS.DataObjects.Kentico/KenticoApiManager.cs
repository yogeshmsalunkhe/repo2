﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using IM.ErrorLoggingUtilities;

// Kentico namespaces
using CMS.DocumentEngine;
using CMS.Membership;
using CMS.DataEngine;
using CMS.OnlineMarketing;
using CMS.CustomTables;
using CMS.SiteProvider;
using VPCONSTANT=IM.CMS.Rules.Kentico;

//IM namespaces
using IM.CMS.CommonContracts;
using IM.CMS.CommonContracts.Constants;
using IM.CMS.Rules.Kentico;
using IM.CMS.Integration;

namespace IM.CMS.DataObjects.Kentico
{
    /// <summary>
    /// Manages all interactions with Kentico using Kentico API.
    /// Don't add any business logic in this class.
    /// </summary>
    public sealed class KenticoApiManager
    {
        private static volatile KenticoApiManager instance;
        private static object syncRoot = new Object();

        public static KenticoApiManager GetInstance(string connectionString)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new KenticoApiManager();
                }
            }
            ConnectionHelper.ConnectionString = connectionString;
            return instance;
        }

        // Initialize Kentico
        private KenticoApiManager()
        {
            Logger.Write("Start Kentico API initialization", Category.General, Priority.Highest, EventId.GeneralMessage);
            CMSApplication.PreInit(false);
            CMSApplication.Init();
            Logger.Write("End Kentico API initialization", Category.General, Priority.Highest, EventId.GeneralMessage);
        }

        // Returns Kentico operator. 
        private QueryOperator GetKenticoQueryOperator(Operator customOperator)
        {
            switch (customOperator)
            {
                case Operator.Equals:
                    return QueryOperator.Equals;
                case Operator.NotEquals:
                    return QueryOperator.NotEquals;
                case Operator.GreaterThan:
                    return QueryOperator.LargerThan;
                case Operator.GreaterThanOrEquals:
                    return QueryOperator.LargerOrEquals;
                case Operator.LessThan:
                    return QueryOperator.LessThan;
                case Operator.LessThanOrEquals:
                    return QueryOperator.LessOrEquals;
                case Operator.Like:
                    return QueryOperator.Like;
                case Operator.NotLike:
                    return QueryOperator.NotLike;
                default:
                    return QueryOperator.Equals;
            }
        }

        private void AddCriteria(WhereCondition whereCondition, string fieldName, Operator customOperator, string fieldValue)
        {
            switch (customOperator)
            {
                case Operator.Equals:
                    whereCondition.Where(fieldName, QueryOperator.Equals, fieldValue);
                    return;
                case Operator.NotEquals:
                    whereCondition.Where(fieldName, QueryOperator.NotEquals, fieldValue);
                    return;
                case Operator.GreaterThan:
                    whereCondition.Where(fieldName, QueryOperator.LargerThan, fieldValue);
                    return;
                case Operator.GreaterThanOrEquals:
                    whereCondition.Where(fieldName, QueryOperator.LargerOrEquals, fieldValue);
                    return;
                case Operator.LessThan:
                    whereCondition.Where(fieldName, QueryOperator.LessThan, fieldValue);
                    return;
                case Operator.LessThanOrEquals:
                    whereCondition.Where(fieldName, QueryOperator.LessOrEquals, fieldValue);
                    return;
                case Operator.Like:
                    whereCondition.Where(fieldName, QueryOperator.Like, fieldValue);
                    return;
                case Operator.NotLike:
                    whereCondition.Where(fieldName, QueryOperator.NotLike, fieldValue);
                    return;
                case Operator.IN:
                    List<string> items = fieldValue.Split(',').ToList();
                    whereCondition.WhereIn(fieldName, items);
                    return;
                default:
                    whereCondition.Where(fieldName, QueryOperator.Equals, fieldValue);
                    return;
            }
        }

        private DocumentQuery GetDocumentQuery(string documentTypeName, string userSelectedCulture, List<string> requiredFields, List<Criterion> criteria, bool ignoreCulture, string[] orderBy)
        {
            string documentClassName = string.Format("{0}.{1}", Constants.ContentListNameSpace, documentTypeName);
            TreeProvider tree = new TreeProvider(MembershipContext.AuthenticatedUser);
            var documentQuery = tree.SelectNodes(documentClassName).OrderBy(orderBy);

            if (!string.IsNullOrEmpty(userSelectedCulture) && !ignoreCulture)
            {
                documentQuery.Culture(userSelectedCulture);
            }
            else if (ignoreCulture)
            {
                documentQuery.AllCultures();
            }

            if (requiredFields != null && requiredFields.Count > 0)
            {
                documentQuery.Columns(requiredFields);
            }

            if (criteria != null && criteria.Count > 0)
            {
                WhereCondition whereCondition = new WhereCondition();
                for (int i = 0; i < criteria.Count; i++)
                {
                    var criterion = criteria[i];
                    if (i > 0)
                    {
                        if (criterion.Combiner == Combiner.And)
                        {
                            whereCondition.And();
                        }
                        else if (criterion.Combiner == Combiner.Or)
                        {
                            whereCondition.Or();
                        }
                    }
                    if (documentTypeName == Constants.SavedEvent)
                    {
                        AddCriteria(whereCondition, criterion.FieldName, criterion.Operator, criterion.FieldValue);
                    }
                    else
                    {
                        whereCondition.Where(criterion.FieldName, GetKenticoQueryOperator(criterion.Operator), criterion.FieldValue);
                    }
                                       
                }
                documentQuery.Where(whereCondition);
            }
            return documentQuery;
        }

        // Fetch list data stored as a Kentico documents and returns dataset with respective data.
        public DataSet GetDocumentData(string conStr, string documentTypeName, string userSelectedCulture, List<string> requiredFields, List<Criterion> criteria, bool ignoreCulture, string[] orderBy)
        {
            var documentQuery = GetDocumentQuery(documentTypeName, userSelectedCulture, requiredFields, criteria, ignoreCulture, orderBy);
            string query = documentQuery.GetFullQueryText();            
            DataSet result;
            using (var conScope = new CMSConnectionScope(conStr, true))
            {
                result = ConnectionHelper.ExecuteQuery(query, null, QueryTypeEnum.SQLQuery);
            }
            return result;
        }

        //Fetch Custom Table Data
        public DataSet GetCustomTableData(string documentTypeName, string userSelectedCulture, List<string> requiredFields, List<Criterion> criteria, bool ignoreCulture, string[] orderBy)
        {
            string customTableClassName = string.Format("{0}.{1}", Constants.CustumTableListNameSpace, documentTypeName);
            //---
            DataClassInfo customTable = DataClassInfoProvider.GetDataClassInfo(customTableClassName);
            if (customTable != null)
            {
                ObjectQuery<CustomTableItem> customTableItem = CustomTableItemProvider.GetItems(customTableClassName);
                if (requiredFields != null && requiredFields.Count > 0)
                {
                    customTableItem.Columns(requiredFields);
                }
                if (requiredFields != null && requiredFields.Count > 0)
                {
                    customTableItem.Columns(requiredFields);
                }

                if (criteria != null && criteria.Count > 0)
                {
                    WhereCondition whereCondition = new WhereCondition();
                    for (int i = 0; i < criteria.Count; i++)
                    {
                        var criterion = criteria[i];
                        if (i > 0)
                        {
                            if (criterion.Combiner == Combiner.And)
                            {
                                whereCondition.And();
                            }
                            else if (criterion.Combiner == Combiner.Or)
                            {
                                whereCondition.Or();
                            }
                        }
                        whereCondition.Where(criterion.FieldName, GetKenticoQueryOperator(criterion.Operator), criterion.FieldValue);
                    }
                    customTableItem.Where(whereCondition);
                }
                return customTableItem.Result;
            }
            else
            {
                Logger.Write("Custom table " + customTableClassName + " not found");
                return null;
            }
        }

        // Fetch file attachment data from Kentico and returns dataset with respective attachment data.
        public DataSet GetDocumentAttachment(int documentID)
        {
            TreeProvider tree = new TreeProvider(MembershipContext.AuthenticatedUser);
            DataSet ds = new DataSet();
            using (var document = tree.SelectSingleDocument(documentID))
            {
                ds = DocumentHelper.GetAttachments(document, string.Empty, string.Empty, true, tree);
            }
            return ds;
        }

        /// <summary>
        /// Returns the URL for the Kentico Banner Redirect page
        /// </summary>
        /// <param name="siteCode"></param>
        /// <returns></returns>
        public static string GetBannerRedirectUrl(string siteCode)
        {
            RulesController rc = new RulesController();
            string ContentDomain = rc.GetContentDomainName(siteCode);
            KenticoInstance ki = KenticoInstanceConfigHelper.Instance.GetKenticoInstance(ContentDomain);
            return ki.dmz + "/CMSModules/BannerManagement/CMSPages/BannerRedirect.ashx";

        }

        /// <summary>
        /// Returns the Advert URL for a given Banner ID
        /// </summary>
        /// <param name="bannerId"></param>
        /// <returns></returns>
        public string GetBannerUrl(int bannerId)
        {
            string qry = "select BannerUrl from CMS_Banner where BannerID = " + bannerId;
            QueryDataParameters qdp = new QueryDataParameters();
            QueryParameters qp = new QueryParameters(qry, qdp, QueryTypeEnum.SQLQuery, false);
            DataSet ds = ConnectionHelper.ExecuteQuery(qp);
            string rtnVar = string.Empty;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                rtnVar = row.ItemArray[0] as string;
            }
            return rtnVar;
        }
       
        public IEnumerable<WebExclusivePromotionsDTO> GetWebExclusivePromotions(string conStr, WebExclusivePromotionRequest request)
        {
            List<WebExclusivePromotionsDTO> mainTabs = new List<WebExclusivePromotionsDTO>();
            try
            {
                using (var conScope = new CMSConnectionScope(conStr, true))
                {

                    TreeProvider tree = new TreeProvider(MembershipContext.AuthenticatedUser);
                    //generate the document class name
                    string documentClassName = string.Format("{0}.{1}", Constants.ContentListNameSpace, request.ListName);

                    //select top menu tabs from content tree
                    var query = tree.SelectNodes().Type(documentClassName).Path("/Other-Configurable-Elements/Header/Top-Menu-Tabs", PathTypeEnum.Children).Culture(request.UserSelectedCulture).WithCoupledColumns().GetFullQueryText();
                    var ds = ConnectionHelper.ExecuteQuery(query, null, QueryTypeEnum.SQLQuery);
                    var pages = new List<TreeNode>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        pages.Add(TreeNode.New<TreeNode>(documentClassName, row, tree));
                    }

                    //for each node - 1)retrieve custom columns 2) select max 10 nodes where current node is the parent node 3) populate list

                    int parentNodeID;
                    int maxMainTabs = 2;
                    int maxSubMenus = 10;
                    mainTabs = new List<WebExclusivePromotionsDTO>();
                    WebExclusivePromotionsDTO mainTab;

                    if (!string.IsNullOrEmpty(request.SiteCode) && request.SiteCode.ToUpper() == IM.CMS.Rules.Kentico.Constants.VendorPortalCountryCodeUS)
                    {
                        maxMainTabs = 3;
                    }

                    //for each node - 1)retrieve custom columns 2) select max 10 nodes where current node is the parent node 3) populate list                

                    TreeNode page = pages.FirstOrDefault();
                    if (page != null)
                    {
                        //parent folder - Top menu tabs
                        int parentFolderID = page.NodeParentID;
                        //select main tabs - max 2
                        var mainTabNodes = pages.Where(r => r.NodeParentID == parentFolderID);

                        if (request.IsUserLoggedIn)
                        {

                            //if user is logged in - select ONLY tabs where
                            //MenuItemVisibilityPrivate == true AND MenuItemVisibilityPublic == true OR
                            //MenuItemVisibilityPrivate == false AND MenuItemVisibilityPublic == false OR
                            //MenuItemVisibilityPrivate == true AND MenuItemVisibilityPublic == false 
                            mainTabNodes = mainTabNodes.Where(r => (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == false && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == false)
                               || (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == true && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == true)
                               || (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == true && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == false));

                        }
                        else
                        {
                            //if user is logged out - select ONLY tabs where
                            //MenuItemVisibilityPrivate == true AND MenuItemVisibilityPublic == true OR
                            //MenuItemVisibilityPrivate == false AND MenuItemVisibilityPublic == false OR
                            //MenuItemVisibilityPrivate == false AND MenuItemVisibilityPublic == true 

                            mainTabNodes = mainTabNodes.Where(r => (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == false && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == false)
                               || (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == true && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == true)
                               || (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == false && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == true));
                        }

                        if (mainTabNodes != null && mainTabNodes.ToList().Count > 0)
                        {
                            mainTabNodes = mainTabNodes.OrderBy(node => node.NodeOrder);
                            foreach (TreeNode mainTabNode in mainTabNodes)
                            {
                                if (mainTabs.Count >= maxMainTabs)
                                    break;

                                if (mainTabNode.DocumentCulture.ToLower() != request.UserSelectedCulture.ToLower())
                                    continue;

                                parentNodeID = mainTabNode.NodeID;
                                mainTab = new WebExclusivePromotionsDTO();
                                mainTab.Name = mainTabNode.GetValue("Name").ToString();

                                if (mainTabNode.GetValue("URL") == null || mainTabNode.GetValue("URL").ToString().Trim().Length <= 0)
                                {
                                    mainTab.Url = "javascript:void(0)";
                                    mainTab.OpenURLIn = "";
                                }
                                else
                                {                                   
                                    mainTab.Url = mainTabNode.GetValue("URL").ToString();
                                    mainTab.OpenURLIn = mainTabNode.GetValue("OpenUrlIn").ToString();

                                }
                                mainTab.MenuItemVisibility = bool.Parse(mainTabNode.GetValue("MenuItemVisibilityPrivate").ToString()) == false ? 0 : 1;

                                List<WebExclusivePromotionsDTO> subMenuItems = new List<WebExclusivePromotionsDTO>();

                                var subPages = pages.Where(r => r.NodeParentID == parentNodeID);

                                if (request.IsUserLoggedIn)
                                {
                                    //logged in
                                    subPages = subPages.Where(r => (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == false && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == false)
                               || (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == true && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == true)
                               || (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == true && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == false));

                                }
                                else
                                {
                                    //logged out
                                    subPages = subPages.Where(r => (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == false && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == false)
                               || (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == true && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == true)
                               || (bool.Parse(r.GetValue("MenuItemVisibilityPrivate").ToString()) == false && bool.Parse(r.GetValue("MenuItemVisibilityPublic").ToString()) == true));
                                }
                                if (subPages != null && subPages.ToList().Count > 0)
                                {
                                    WebExclusivePromotionsDTO subMenuItem;
                                    subPages = subPages.OrderBy(node => node.NodeOrder);


                                    foreach (TreeNode subPage in subPages)
                                    {
                                        //ONLY 10 ALLOWED
                                        if (subMenuItems.Count >= maxSubMenus)
                                            break;

                                        if (subPage.DocumentCulture.ToLower() != request.UserSelectedCulture.ToLower())
                                            continue;
                                        // subPageNodeID = subPage.NodeID;
                                        subMenuItem = new WebExclusivePromotionsDTO();
                                        subMenuItem.Name = subPage.GetValue("Name").ToString();

                                        if (subPage.GetValue("URL") == null || subPage.GetValue("URL").ToString().Trim().Length <= 0)
                                        {
                                            subMenuItem.Url = "javascript:void(0)";
                                            subMenuItem.OpenURLIn = "";
                                        }
                                        else
                                        {                                           
                                            subMenuItem.Url = subPage.GetValue("URL").ToString();
                                            subMenuItem.OpenURLIn = subPage.GetValue("OpenUrlIn").ToString();

                                        }
                                        subMenuItem.MenuItemVisibility = bool.Parse(subPage.GetValue("MenuItemVisibilityPrivate").ToString()) == false ? 0 : 1;
                                        subMenuItems.Add(subMenuItem);

                                    }

                                    mainTab.MenuItems = subMenuItems;
                                }

                                mainTabs.Add(mainTab);
                            }

                        }
                    }//end main collection if                
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mainTabs;

        }

        public bool CheckContactGroupContainsContact(string ContactGUID, string ContactGroupNames, string SiteName)
        {
            bool contactFound = false;
            try
            {
                // Gets the contact group
                List<string> ContactGroupsList = ContactGroupNames.Split(',').ToList();
                SiteInfo currentSite = SiteInfoProvider.GetSiteInfo(SiteName);

                // Gets the contact as per ContactGUID                 
                ContactInfo contact = ContactInfoProvider.GetContacts()
                                                    .WhereEquals("ContactGUID", ContactGUID)
                                                    .WhereEquals("ContactSiteID", currentSite.SiteID)
                                                    .FirstObject;

                if (contact != null && ContactGroupsList != null && ContactGroupsList.Count > 0)
                {
                    List<ContactGroupInfo> contactGroupInfos = ContactGroupInfoProvider.GetContactGroups()
                               .WhereIn("ContactGroupDisplayName", ContactGroupsList)
                                                   .WhereEquals("ContactGroupSiteID", currentSite.SiteID).ToList();

                    foreach (ContactGroupInfo contactGrp in contactGroupInfos)
                    {

                        //check if contact is on any contact group

                        ContactGroupMemberInfo memberInfo = ContactGroupMemberInfoProvider.GetContactGroupMemberInfoByData(contactGrp.ContactGroupID, contact.ContactID, ContactGroupMemberTypeEnum.Contact);

                        if (memberInfo != null)
                        {
                            contactFound = true;
                            break;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return contactFound;
        }

        public TreeNode GetDocumentNode(string documentTypeName, string userSelectedCulture, List<string> requiredFields, List<Criterion> criteria)
        {
            var documentQuery = GetDocumentQuery(documentTypeName, userSelectedCulture, requiredFields, criteria, false, new string[0]);

            if (criteria == null || criteria.Count == 0)
                throw new Exception(string.Format("Criteria is not defined."));

            if (documentQuery.Count > 1)
                throw new Exception(string.Format("Multiple records found."));

            if (documentQuery.FirstObject == null)
                throw new Exception(string.Format("Document not found."));

            return documentQuery.FirstObject;
        }

        public IEnumerable<VendorConfigurableLinksDTO> GetVendorConfigurableLinks(string conStr, VendorConfigurableLinksRequest request)
        {
            List<VendorConfigurableLinksDTO> ConfigurableLinks = new List<VendorConfigurableLinksDTO>();
            try
            {
                using (var conScope = new CMSConnectionScope(conStr, true))
                {

                    TreeProvider tree = new TreeProvider(MembershipContext.AuthenticatedUser);
                    //generate the document class name
                    string documentClassName = string.Format("{0}.{1}", Constants.ContentListNameSpace, request.ListName);
                    string vendor = request.Vendor;
                    //select top menu tabs from content tree 
                    var query = tree.SelectNodes().Type(documentClassName).Path("/Other-Configurable-Elements/Right-Zone/Configurable-Links/" + vendor, PathTypeEnum.Children).Culture(request.UserSelectedCulture).WithCoupledColumns().GetFullQueryText();
                    var ds = ConnectionHelper.ExecuteQuery(query, null, QueryTypeEnum.SQLQuery);
                    var pages = new List<TreeNode>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        pages.Add(TreeNode.New<TreeNode>(documentClassName, row, tree));
                    }
                   

                    int parentNodeID;
                    //int maxLinks = 10;

                    VendorConfigurableLinksDTO ConfigurableLink;


                    //retrieve max 10 nodes              

                    TreeNode page = pages.FirstOrDefault();
                    if (page != null)
                    {
                       
                        int parentFolderID = page.NodeParentID;
                        
                        var DocumentLinkNodes = pages.Where(r => r.NodeParentID == parentFolderID);

                        if (DocumentLinkNodes != null && DocumentLinkNodes.ToList().Count > 0)
                        {
                            DocumentLinkNodes = DocumentLinkNodes.OrderBy(node => node.NodeOrder);
                            foreach (TreeNode DocumentLinkNode in DocumentLinkNodes)
                            {
                                //if (DocumentLinks.Count >= maxLinks)
                                //    break;

                                if (DocumentLinkNode.DocumentCulture.ToLower() != request.UserSelectedCulture.ToLower())
                                    continue;

                                parentNodeID = DocumentLinkNode.NodeID;
                                ConfigurableLink = new VendorConfigurableLinksDTO();
                                ConfigurableLink.ContentName = DocumentLinkNode.GetValue("ContentName").ToString();
                                ConfigurableLink.ContentType = DocumentLinkNode.GetValue("ContentType").ToString().ToUpper();
                                if (DocumentLinkNode.GetValue("ThumbnailUrl") != null)
                                {
                                    string thumbnailUrl = string.Empty;
                                    thumbnailUrl = DocumentLinkNode.GetValue("ThumbnailUrl").ToString();
                                    thumbnailUrl = thumbnailUrl.Replace("~", "");
                                    ConfigurableLink.ThumbnailUrl = thumbnailUrl;                                    
                                }
                                else
                                {
                                    ConfigurableLink.ThumbnailUrl = "";
                                }
                                if (ConfigurableLink.ContentType == VPCONSTANT.DocumentType.PAGE.ToString() || ConfigurableLink.ContentType == VPCONSTANT.DocumentType.VIDEO.ToString())
                                {
                                    if (DocumentLinkNode.GetValue("PageUrl") == null || DocumentLinkNode.GetValue("PageUrl").ToString().Trim().Length <= 0)
                                    {
                                        ConfigurableLink.Url = "javascript:void(0)";
                                    }
                                    else
                                    {                                        
                                        string url = string.Empty;
                                        url = DocumentLinkNode.GetValue("PageUrl").ToString();
                                        if (ConfigurableLink.ContentType == VPCONSTANT.DocumentType.PAGE.ToString())
                                        {
                                            url = IMSiteHelper.ModifyKenticoPageURL(url);
                                            //url = url.Replace("ContentPages", "c");
                                            //url = url.Replace("Content-Pages", "c");
                                            //if (!url.Contains(".aspx"))
                                            //{
                                            //    url += ".aspx";
                                            //}
                                        }
                                        ConfigurableLink.Url = url;
                                    }                                   
                                }
                                else
                                {
                                    if (DocumentLinkNode.GetValue("MediaUrl") == null || DocumentLinkNode.GetValue("MediaUrl").ToString().Trim().Length <= 0)
                                    {
                                        ConfigurableLink.Url = "javascript:void(0)";
                                    }
                                    else
                                    {
                                        string url = string.Empty;
                                        url = DocumentLinkNode.GetValue("MediaUrl").ToString();
                                        url = url.Replace("~", "");
                                        ConfigurableLink.Url = url;
                                    }                                   
                                }
                                int priority=0;
                                if (DocumentLinkNode.GetValue("Priority") != null && DocumentLinkNode.GetValue("Priority").ToString().Trim().Length >= 0)
                                {
                                    Int32.TryParse(DocumentLinkNode.GetValue("Priority").ToString(), out priority);
                                }
                                ConfigurableLink.Priority = priority;
                                if (DocumentLinkNode.GetValue("DocumentLastPublished") != null)
                                {
                                    ConfigurableLink.DocumentLastPublished = (DateTime)DocumentLinkNode.GetValue("DocumentLastPublished");
                                }
                                ConfigurableLinks.Add(ConfigurableLink);
                            }
                            if (ConfigurableLinks != null && ConfigurableLinks.Count() > 0)
                            {
                                var links = ConfigurableLinks.Where(d => d.ContentType == VPCONSTANT.DocumentType.IMAGE.ToString() || d.ContentType == VPCONSTANT.DocumentType.VIDEO.ToString());
                                if (links != null && links.Count() > 0)//have media links
                                {
                                    //take first media link
                                    VendorConfigurableLinksDTO MediaLinks = new VendorConfigurableLinksDTO();
                                    MediaLinks = links.OrderByDescending(p => p.Priority).ThenByDescending(p => p.DocumentLastPublished).FirstOrDefault();
                                    //remove all media link from main list
                                    ConfigurableLinks.RemoveAll(a => links.ToList().Exists(c => c.ContentName == a.ContentName));
                                    if (ConfigurableLinks != null && ConfigurableLinks.Count() > 0)//have links other then video and image
                                    {
                                        ConfigurableLinks = ConfigurableLinks.OrderByDescending(p => p.Priority).ThenByDescending(p => p.DocumentLastPublished).ToList();
                                        if (ConfigurableLinks.Count() > 4)
                                        {
                                            ConfigurableLinks = ConfigurableLinks.Take(4).ToList();
                                        }
                                        ConfigurableLinks.Insert(0, MediaLinks);
                                    }
                                }
                                else// do not have media links
                                {
                                    ConfigurableLinks = ConfigurableLinks.OrderByDescending(p => p.Priority).ThenByDescending(p => p.DocumentLastPublished).ToList();
                                    if (ConfigurableLinks.Count() > 5)
                                    {
                                        ConfigurableLinks = ConfigurableLinks.Take(5).ToList();
                                    }
                                }
                            }
                        }
                    }                
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ConfigurableLinks;

        }
    }
}
