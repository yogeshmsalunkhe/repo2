﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging;

//Kentico namespaces
using CMS.DocumentEngine;

//IM namespaces
using IM.ErrorLoggingUtilities;
using IM.CMS.Rules.Kentico;

namespace IM.CMS.DataObjects.Kentico
{
    public class AttachmentManager
    {

        public bool UpdateAttachment(TreeNode documentNode, string attachmentName, byte[] attachmentData, string attachmentField)
        {
            if (ValidateAttachmentRequest(attachmentName, attachmentData))
            {
                var attachmentGuid = documentNode.GetGuidValue(attachmentField, Guid.Empty);
                if (attachmentGuid != Guid.Empty)
                {
                    var attachment = AttachmentInfoProvider.GetAttachmentInfo(documentNode.DocumentID, attachmentGuid);
                    if (attachment != null)
                    {
                        attachment.AttachmentName = attachmentName;
                        attachment.AttachmentBinary = attachmentData;
                        attachment.AttachmentSize = attachmentData.Length;
                        attachment.AttachmentExtension = System.IO.Path.GetExtension(attachmentName);
                        attachment.AttachmentMimeType = FileInfoUtility.GetMimeTypeByExtension(attachment.AttachmentExtension);
                        attachment.Update();
                        return true;
                    }
                }
            }
            return false;
        }

        public bool ValidateAttachmentRequest(string attachmentName, byte[] attachmentData)
        {
            if (attachmentData == null || attachmentData.Length == 0)
            {
                Logger.Write(RuleMessages.AttachmentDataEmpty, Category.General, Priority.Highest, EventId.GeneralMessage);
                return false;
            }
            else if (string.IsNullOrEmpty(attachmentName))
            {
                Logger.Write(RuleMessages.AttachmentNameEmpty, Category.General, Priority.Highest, EventId.GeneralMessage);
                return false;
            }
            return true;
        }
    }
}
