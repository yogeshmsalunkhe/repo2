﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IM.CMS.CommonContracts;
using IM.CMS.Rules.Kentico;
using CMS.MediaLibrary;
using CMS.IO;
using CMS.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using IM.ErrorLoggingUtilities;
using System.Reflection;
using System.Web;
using System.Configuration;
using IM.CMS.Integration;

namespace IM.CMS.DataObjects.Kentico
{
    public class UploadImageManager
    {
        public KenticoApiManager KenticoApiManager { get; set; }
        private string conStr;

        public UploadImageManager(RequestBase request)
        {
            //  var rule = new RulesController();
            //  string conStr = rule.GetCmsDbString(request.SiteCode);

            conStr = GetConnectionString(request.SiteCode);
            KenticoApiManager = KenticoApiManager.GetInstance(conStr);
        }

        private KenticoInstance getKenticoInstance(string aSiteCode)
        {
            RulesController rc = new RulesController();
            string ContentDomain = rc.GetContentDomainName(aSiteCode);
            KenticoInstance ki = KenticoInstanceConfigHelper.Instance.GetKenticoInstance(ContentDomain);
            return ki;
        }

        public static string GetConnectionString(string siteCode)
        {
            RulesController rule = new RulesController();
            return rule.GetCmsDbString(siteCode);
        }

        public bool ImportQuotePreferenceLogo(UploadQuotePreferenceLogoRequest request)
        {
            bool FileStatus = false;
            string filePath = string.Empty;
            try
            {
                string Mediafiletitle = Constants.Mediafiletitle;
                // get the kentico instances
                RulesController rc = new RulesController();
                string ContentDomain = rc.GetContentDomainName(request.SiteCode);
                KenticoInstance ki = KenticoInstanceConfigHelper.Instance.GetKenticoInstance(ContentDomain);

                string sitename = ki.dmz;
               
                MediaLibraryInfo libraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(Constants.ExternalMediaLibrary, sitename);
                filePath = Path.Combine(request.Des_File_path, request.filename);

                if (libraryInfo != null)
                {
                    libraryInfo.Access = SecurityAccessEnum.AllUsers;
                    FileInfo file = FileInfo.New(filePath);
                    if (file != null)
                    {
                        filePath = MediaLibraryHelper.EnsurePhysicalPath(filePath);

                        if (File.Exists(filePath))
                        {
                            // Creates a new media library file object
                            MediaFileInfo mediaFile = new MediaFileInfo(filePath, libraryInfo.LibraryID, Constants.QuotesPreferencesLogo);

                            // Sets the media library file properties
                            mediaFile.FileName = file.Name;
                            mediaFile.FileTitle = Mediafiletitle;
                            mediaFile.FileDescription = Mediafiletitle;
                            mediaFile.FilePath = request.fileurl;
                            mediaFile.FileExtension = file.Extension;
                            mediaFile.FileMimeType = MimeTypeHelper.GetMimetype(file.Extension);
                            mediaFile.FileSiteID = libraryInfo.LibrarySiteID;
                            mediaFile.FileLibraryID = libraryInfo.LibraryID;
                            mediaFile.FileSize = file.Length;
                            //MediaFileInfoProvider.SetMediaFileInfo(mediaFile, false);
                            MediaFileInfoProvider.ImportMediaFileInfo(mediaFile);
                            FileStatus = true;
                        }
                    }
                }
                else
                {
                    throw new Exception(string.Format("media Library Folder not found."));
                }
            }
            catch (Exception ex)
            {
                Logger.Write("Error in loading Image on media library in kentico. Error details: " + ex.Message + ex.StackTrace, Category.General, Priority.Highest, EventId.GeneralMessage);
            }
            return FileStatus;
        }


        public bool DeleteQuotePreferenceLogo(UploadQuotePreferenceLogoRequest request)
        
        {
            bool FileStatus = false;
            string filePath = string.Empty;
            try
            {
                // get the kentico instances
                RulesController rc = new RulesController();
                string ContentDomain = rc.GetContentDomainName(request.SiteCode);
                KenticoInstance ki = KenticoInstanceConfigHelper.Instance.GetKenticoInstance(ContentDomain);

                string sitename = ki.dmz;


                MediaLibraryInfo libraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(Constants.ExternalMediaLibrary, sitename);

                if (libraryInfo != null)
                {
                    // Gets the media file
                    MediaFileInfo deleteFile = MediaFileInfoProvider.GetMediaFileInfo(libraryInfo.LibraryID, request.filename);

                    if (deleteFile != null)
                    {
                        // Deletes the media file
                        MediaFileInfoProvider.DeleteMediaFileInfo(deleteFile);
                        FileStatus = true;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Write("Error in deleting quotes preferences logo on media library. Error details: " + ex.Message + ex.StackTrace, Category.General, Priority.Highest, EventId.GeneralMessage);
            }
            return FileStatus;
        }

       
        
       

    }
}
