﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;

// Kentico namespaces
using CMS.DataEngine;

//IMOnline namespaces
using IM.ErrorLoggingUtilities;
using IM.CMS.Rules.Kentico;

namespace IM.CMS.DataObjects.Kentico
{
    public class DataLayer
    {
        private static DatabaseProviderFactory factory = new DatabaseProviderFactory();
        private Database db;
        public Database Database { get { return db; } }

        public DataLayer(string dbConfig)
        {
            db = factory.Create(dbConfig);
        }

        public string GetContactGuid(string samAccountName, string siteDisplayName)
        {
            DataTable dt;
            string guid = string.Empty;
            using (DbCommand dbc = db.GetStoredProcCommand(Constants.GetGuidSP))
            {
                db.AddInParameter(dbc, "@SAMAccountName", DbType.String, samAccountName);
                db.AddInParameter(dbc, "@SiteDisplayName", DbType.String, siteDisplayName);

                using (IDataReader reader = db.ExecuteReader(dbc))
                {
                    dt = new DataTable();
                    dt.Load(reader);
                    if (dt.Rows.Count > 0)
                    {
                        guid = dt.Rows[0]["Guid"].ToString();
                    }
                }
            }
            return guid;
        }

        /// <summary>
        /// Fetches the IMResellerID column from Contacts who belong to the specified contact group sorted into ascending order.
        /// </summary>
        /// <param name="aContactGroupId"></param>
        /// <returns></returns>
        public string[] GetResellerIDs(int aContactGroupId)
        {
            List<string> data = new List<string>();
            try
            {
                DataSet contacts = new DataQuery()
                            .From(
                                new QuerySource("OM_Contact")
                                    .LeftJoin("OM_ContactGroupMember", "ContactID", "ContactGroupMemberRelatedID")
                            )
                            .Where("ContactGroupMemberContactGroupID = " + aContactGroupId).And().Where("IMResellerID is not null").Column("IMResellerID").Distinct().Execute();

                if (contacts.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow con in contacts.Tables[0].Rows)
                    {
                        data.Add(con["IMResellerID"].ToString());
                    }
                }
                else
                {
                    Logger.Write("No data found for contact group id :" + aContactGroupId, Category.General, Priority.Highest, EventId.GeneralMessage);
                }
            }
            catch (Exception ex)
            {
                Logger.Write("Error in GetResellerIDs. Error details: " + ex.Message + ex.StackTrace, Category.General, Priority.Highest, EventId.GeneralMessage);
            }
            return data.ToArray();
        }
    }
}
