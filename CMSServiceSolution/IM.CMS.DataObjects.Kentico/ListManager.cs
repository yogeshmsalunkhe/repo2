﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using Microsoft.Practices.EnterpriseLibrary.Logging;

//IM namespaces
using IM.CMS.CommonContracts;
using IM.CMS.Rules.Kentico;
using IM.CMS.CommonContracts.Constants;
using IM.CMS.Integration;
using IM.ErrorLoggingUtilities;
using CMS.DocumentEngine;
using CMS.Membership;

namespace IM.CMS.DataObjects.Kentico
{
    /// <summary>
    /// Act as a middle layer between service call and Kentico API manager to get list data.
    /// </summary>
    public class ListManager
    {
        private DataLayer dbInstance;
        private string conStr;

        public KenticoApiManager KenticoApiManager { get; set; } // Manage Kentico API.       


        // Define and initialize Kentico API manager.
        public ListManager(ListRequest request)
        {
            conStr = GetConnectionString(request.SiteCode);
            KenticoApiManager = KenticoApiManager.GetInstance(conStr);
        }

        // Get connection string of Kentico DB from web.config as per site code. 
        public static string GetConnectionString(string siteCode)
        {
            RulesController rule = new RulesController();
            return rule.GetCmsDbString(siteCode);
        }

        private DataLayer GetDbInstance(string siteCode)
        {
            try
            {
                if (dbInstance != null)
                    return dbInstance;

                RulesController rc = new RulesController();
                string siteName = rc.GetContentDomainSiteDisplayName(siteCode);
                if (!string.IsNullOrEmpty(siteName))
                {
                    dbInstance = new DataLayer(Constants.DBConfigurationPrefix + ' ' + siteName);
                }
                else
                    throw new Exception("site name is blank");
            }
            catch (Exception ex)
            {
                Logger.Write("Error in GetDbInstance. Error details: " + ex.Message + ex.StackTrace, Category.General, Priority.Highest, EventId.GeneralMessage);
            }
            return dbInstance;
        }

        // Common method to fetch any list data from Knetico.
        public DataRowCollection GetListData(ListRequest request)
        {
            DataRowCollection listData = null;

            List<string> requiredFields = null;
            if (request.RequiredFields != null && request.RequiredFields.Count > 0)
                requiredFields = request.RequiredFields;
            else if (request.AllFields != null && request.AllFields.Count > 0)
                requiredFields = request.AllFields;
            else
                throw new System.Exception("No any field is requested to be fetch !");

            string[] orderBy = new string[0];
            if (request is BoutiqueRequest)
            {
                orderBy = new string[] { "NodeOrder" };
            }

            using (DataSet ds = KenticoApiManager.GetDocumentData(conStr, request.ListName, request.UserSelectedCulture, requiredFields, request.Criteria, request.IgnoreCulture, orderBy))
            {
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null)
                {
                    listData = ds.Tables[0].Rows;
                }
                else
                    throw new System.Exception("No data found !");
            }
            return listData;
        }
        // Common method to fetch any custom Table data from Knetico.
        private DataRowCollection GetCustomTableData(ListRequest request)
        {
            DataRowCollection listData = null;

            List<string> requiredFields = null;
            if (request.RequiredFields != null && request.RequiredFields.Count > 0)
                requiredFields = request.RequiredFields;
            else if (request.AllFields != null && request.AllFields.Count > 0)
                requiredFields = request.AllFields;
            else
                throw new System.Exception("No any field is requested to be fetch !");

            string[] orderBy = new string[0];

            using (DataSet ds = KenticoApiManager.GetCustomTableData(request.ListName, request.UserSelectedCulture, requiredFields, request.Criteria, request.IgnoreCulture, orderBy))
            {
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null)
                {
                    listData = ds.Tables[0].Rows;
                }
                else
                    throw new System.Exception("No data found !");
            }
            return listData;
        }
        // Common method to fetch any file attachment data from Kentico.
        private DataRowCollection GetFileAttachmentData(int documentID)
        {
            DataRowCollection attachmentData = null;
            using (DataSet dsAttachment = KenticoApiManager.GetDocumentAttachment(documentID))
            {
                if (dsAttachment != null && dsAttachment.Tables != null && dsAttachment.Tables.Count > 0 && dsAttachment.Tables[0].Rows != null)
                {
                    attachmentData = dsAttachment.Tables[0].Rows;
                }
            }
            return attachmentData;
        }

        // Updates image source URL with respective .com site
        // Currently its only implemeted for custom promotion
        // To Do: support any listobject data and create isolated project (dll).
        private void MakeUrlsAbsolute(IEnumerable<CustomPromotionDTO> aData, string aSiteCode)
        {
            RulesController rc = new RulesController();
            string ContentDomain = rc.GetContentDomainName(aSiteCode);
            KenticoInstance ki = KenticoInstanceConfigHelper.Instance.GetKenticoInstance(ContentDomain);
            if (ki != null)
            {
                string newUrl = "//" + ki.com;
                if (!string.IsNullOrEmpty(ki.site))
                {
                    newUrl += "/" + ki.site;
                }
                foreach (CustomPromotionDTO item in aData)
                {
                    if (!string.IsNullOrEmpty(item.LargeIconPath))
                    {
                        item.LargeIconPath = newUrl + item.LargeIconPath.Replace("~", "");
                    }
                    if (!string.IsNullOrEmpty(item.SmallIconPath))
                    {
                        item.SmallIconPath = newUrl + item.SmallIconPath.Replace("~", "");
                    }
                }
            }
        }

        // Fetch custom promotion data and populate respective list of DTOs.
        public IEnumerable<CustomPromotionDTO> GetCustomPromotion(CustomPromotionRequest request)
        {
            var listData = new List<CustomPromotionDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {
                    var customPromotionDTO = new CustomPromotionDTO();
                    customPromotionDTO.Title = row.GetValue<string>(request.Title);
                    customPromotionDTO.Name = row.GetValue<string>(request.DocumentName);
                    customPromotionDTO.Url = row.GetValue<string>(request.Url);
                    customPromotionDTO.PromotionType = row.GetValue<string>(request.PromotionType);
                    customPromotionDTO.Description = IMSiteHelper.ParseHtml(row.GetValue<string>(request.Description));
                    customPromotionDTO.SmallIconPath = row.GetValue<string>(request.SmallIconPath);
                    customPromotionDTO.LargeIconPath = row.GetValue<string>(request.LargeIconPath);
                    customPromotionDTO.MenuItemVisibility = row.GetValue<int>(request.MenuItemVisibility);
                    customPromotionDTO.DocumentPublishFrom = row.GetValue<DateTime>(request.DocumentPublishFrom);
                    customPromotionDTO.DocumentPublishTo = row.GetValue<DateTime>(request.DocumentPublishTo);
                    customPromotionDTO.IncludeInSearchFacet = row.GetValue<bool>(request.IncludeInSearchFacet);
                    customPromotionDTO.ContactGroup = row.GetValue<int>(request.ContactGroup);
                    customPromotionDTO.OpenUrlIn = row.GetValue<string>(request.OpenUrlIn);
                    customPromotionDTO.ValueUnit = row.GetValue<string>(request.ValueUnit);

                    int documentID = row.GetValue<int>(request.DocumentID);
                    if (documentID != 0)
                    {
                        foreach (DataRow rowAttachment in GetFileAttachmentData(documentID))
                        {
                            customPromotionDTO.Attachment = (byte[])rowAttachment[request.Attachment];
                            customPromotionDTO.AttachmentName = rowAttachment.GetValue<string>(request.AttachmentName);
                        }
                    }

                    if (request.FetchResellerIds && customPromotionDTO.ContactGroup > 0)
                    {
                        var db = GetDbInstance(request.SiteCode);
                        customPromotionDTO.ResellerIDs = db.GetResellerIDs(customPromotionDTO.ContactGroup);
                    }

                    listData.Add(customPromotionDTO);
                }
                MakeUrlsAbsolute(listData, request.SiteCode);
            }
            return listData;
        }

        // Fetch reusable contents data and populate respective list of DTOs.
        public IEnumerable<ReusableContentDTO> GetReusableContent(ReusableContentRequest request)
        {
            var listData = new List<ReusableContentDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {
                    var reusableContentDTO = new ReusableContentDTO();
                    reusableContentDTO.Title = row.GetValue<string>(request.Title);
                    reusableContentDTO.Content = IMSiteHelper.ParseHtml(row.GetValue<string>(request.Content));
                    reusableContentDTO.Comments = row.GetValue<string>(request.Comments);
                    reusableContentDTO.LastModified = row.GetValue<DateTime>(request.LastModified);
                    listData.Add(reusableContentDTO);
                }
            }
            return listData;
        }

        // Fetch ExternalSiteInIframe data and populate respective list of DTOs.
        public IEnumerable<ExternalSiteInIframeDTO> GetExternalSiteInIframe(ExternalSiteInIframeRequest request)
        {
            var listData = new List<ExternalSiteInIframeDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {
                    var externalSiteInIframeDTO = new ExternalSiteInIframeDTO();
                    externalSiteInIframeDTO.IMSiteName = row.GetValue<string>(request.IMSiteName);
                    externalSiteInIframeDTO.ExternalSiteURL = row.GetValue<string>(request.ExternalSiteURL);
                    externalSiteInIframeDTO.AddEncryptedIDtoURL = row.GetValue<bool>(request.AddEncryptedIDtoURL);
                    externalSiteInIframeDTO.MenuItemVisibility = row.GetValue<int>(request.MenuItemVisibility);
                    externalSiteInIframeDTO.DisplayedIn = row.GetValue<string>(request.DisplayedIn);
                    if (row.GetValue<string>(request.PunchoutSystemName) != null)
                        externalSiteInIframeDTO.PunchoutSystemName = row.GetValue<string>(request.PunchoutSystemName);
                    if (row.GetValue<string>(request.PunchbackURL) != null)
                        externalSiteInIframeDTO.PunchbackURL = row.GetValue<string>(request.PunchbackURL);
                    listData.Add(externalSiteInIframeDTO);
                }
            }
            return listData;
        }

        // Fetch marketing URLs data and populate respective list of DTOs.
        public IEnumerable<MarketingUrlDTO> GetMarketingUrl(MarketingUrlRequest request)
        {
            var listData = new List<MarketingUrlDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {
                    var marketingUrlDTO = new MarketingUrlDTO();
                    marketingUrlDTO.SearchBreadCrumbName = row.GetValue<string>(request.SearchBreadCrumbName);
                    marketingUrlDTO.CommaSeperatedListOfSkus = row.GetValue<string>(request.CommaSeperatedListOfSkus);
                    marketingUrlDTO.Comments = row.GetValue<string>(request.Comments);
                    marketingUrlDTO.Active = row.GetValue<bool>(request.Active);
                    marketingUrlDTO.NodeOwnerUserName = row.GetValue<string>(request.NodeOwnerUserName);
                    marketingUrlDTO.DocumentCreatedWhen = row.GetValue<DateTime>(request.DocumentCreatedWhen);
                    marketingUrlDTO.DocumentModifiedWhen = row.GetValue<DateTime>(request.DocumentModifiedWhen);
                    listData.Add(marketingUrlDTO);
                }
            }
            return listData;
        }

        // Fetch Cloud Services data and populate respective list of DTOs.
        public IEnumerable<CloudServicesDTO> GetCloudServices(CloudServiceRequest request)
        {
            var listData = new List<CloudServicesDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {

                    var cloudServicesDTO = new CloudServicesDTO();
                    cloudServicesDTO.CloudServicesID = row.GetValue<string>(request.CloudServicesID);
                    cloudServicesDTO.VPN = row.GetValue<string>(request.CloudServicesID);
                    cloudServicesDTO.ServiceID = row.GetValue<string>(request.CloudServicesID);
                    cloudServicesDTO.ServiceName = row.GetValue<string>(request.ServiceName);
                    cloudServicesDTO.Vendor = row.GetValue<string>(request.Vendor);
                    cloudServicesDTO.ShortDescription = row.GetValue<string>(request.ShortDescription);
                    cloudServicesDTO.Category = row.GetValue<string>(request.Category);
                    cloudServicesDTO.SubCategory = row.GetValue<string>(request.SubCategory);
                    cloudServicesDTO.ServiceType = row.GetValue<string>(request.ServiceType);
                    cloudServicesDTO.LargeLogo = row.GetValue<string>(request.LargeLogo);
                    cloudServicesDTO.SmallLogo = row.GetValue<string>(request.SmallLogo);
                    cloudServicesDTO.Technology = row.GetValue<string>(request.Technology);
                    cloudServicesDTO.Architecture = row.GetValue<string>(request.Architecture);
                    cloudServicesDTO.ShowMoreURL = row.GetValue<string>(request.ShowMoreURL);
                    cloudServicesDTO.WebVisible = row.GetValue<bool>(request.WebVisible);
                    cloudServicesDTO.IsShowMoreTemplate = row.GetValue<bool>(request.IsShowMoreTemplate);
                    cloudServicesDTO.DocumentCreatedWhen = row.GetValue<DateTime>(request.DocumentCreatedWhen);
                    cloudServicesDTO.LongDescription = row.GetValue<string>(request.LongDescription);
                    cloudServicesDTO.TechnicalSpecification = row.GetValue<string>(request.TechnicalSpecification);
                    cloudServicesDTO.PricingContent = row.GetValue<string>(request.PricingContent);


                    listData.Add(cloudServicesDTO);
                }
            }
            return listData;
        }

        // Fetch Boutiques data and populate respective list of DTOs.
        public IEnumerable<BoutiqueDTO> GetBoutiques(BoutiqueRequest request)
        {
            var listData = new List<BoutiqueDTO>();
            Criterion crit = new Criterion();
            crit.FieldName = "NodeLevel";
            crit.FieldValue = "4";
            crit.Operator = Operator.Equals;
            request.Criteria = new List<Criterion>();
            request.Criteria.Add(crit);
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                KenticoInstance ki = getKenticoInstance(request.SiteCode);
                foreach (DataRow row in listDataRows)
                {
                    var boutiqueDTO = new BoutiqueDTO();
                    //                    boutiqueDTO.Label = row.GetValue<string>(request.Label);
                    boutiqueDTO.Name = row.GetValue<string>(request.Name);
                    string url = row.GetValue<string>(request.Url);
                    if (string.IsNullOrEmpty(url))
                    {
                        url = row.GetValue<string>(request.NodeAliasPath);
                        url = url.Replace("ContentPages", "c");
                        //Advert Management Changes
                        url = url.Replace("Content-Pages", "c");
                        url += ".aspx";
                    }
                    boutiqueDTO.Url = url;
                    boutiqueDTO.IsPrivate = row.GetValue<bool>(request.MenuItemVisibility);
                    boutiqueDTO.FeaturedImage = AbsoluteURL(row.GetValue<string>(request.FeaturedImage), ki);
                    boutiqueDTO.FeaturedImageToolTip = row.GetValue<string>(request.FeaturedImageToolTip);
                    string temp = row.GetValue<string>(request.NodeAliasPath);
                    temp = temp.ToLower();
                    if (temp.Contains("/products/boutiques/"))
                    {
                        boutiqueDTO.Type = "P";
                    }
                    else if (temp.Contains("/vendors/boutiques/"))
                    {
                        boutiqueDTO.Type = "V";
                    }
                    listData.Add(boutiqueDTO);
                }
            }
            return listData;
        }

        // Fetch SocialMedia data and populate respective list of DTOs.
        public IEnumerable<SocialMediaDTO> GetSocialMedia(SocialMediaRequest request)
        {
            var listData = new List<SocialMediaDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {
                    var dto = new SocialMediaDTO();
                    dto.Name = row.GetValue<string>(request.Name);
                    dto.Url = row.GetValue<string>(request.Url);
                    listData.Add(dto);
                }
            }
            return listData;
        }

        // Fetch Direct Ship Content data and populate respective list of DTOs.
        public IEnumerable<DirectShipContentDTO> GetDirectShipContent(DirectShipContentRequest request)
        {
            var listData = new List<DirectShipContentDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {
                    var directShipContentDTO = new DirectShipContentDTO();
                    directShipContentDTO.Text = row.GetValue<string>(request.Text);
                    directShipContentDTO.Message = IMSiteHelper.ParseHtml(row.GetValue<string>(request.Message));
                    directShipContentDTO.VendorCode = row.GetValue<string>(request.VendorCode);
                    listData.Add(directShipContentDTO);
                }
            }
            return listData;
        }

        // Fetch Cloud Menu data and populate respective list of DTOs.
        public IEnumerable<CloudMenuDTO> GetCloudMenu(CloudMenuRequest request)
        {
            var listData = new List<CloudMenuDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {
                    var cloudMenutDTO = new CloudMenuDTO();
                    cloudMenutDTO._columnNameCloudMenuTitle = row.GetValue<string>(request.Title);
                    cloudMenutDTO._columnNameCloudMenuUrl = row.GetValue<string>(request.URL);
                    cloudMenutDTO.DisplayedIn = row.GetValue<string>(request.OpenURL);
                    listData.Add(cloudMenutDTO);
                }
            }
            return listData;
        }
        // Fetch Direct Ship Content data and populate respective list of DTOs.
        public IEnumerable<CloudContentDTO> GetCloudContent(CloudContentRequest request)
        {
            var listData = new List<CloudContentDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                KenticoInstance ki = getKenticoInstance(request.SiteCode);
                foreach (DataRow row in listDataRows)
                {
                    var CloudContentDTO = new CloudContentDTO();
                    CloudContentDTO.Title = row.GetValue<string>(request.Title);
                    CloudContentDTO.Publisher = row.GetValue<string>(request.Publisher);
                    CloudContentDTO.Topic = row.GetValue<string>(request.Topic);
                    CloudContentDTO.Description = row.GetValue<string>(request.Description);
                    CloudContentDTO.TypeOfFile = row.GetValue<string>(request.TypeOfFile);
                    CloudContentDTO.FileFromMedia = AbsoluteURL(row.GetValue<string>(request.FileFromMedia), ki);
                    CloudContentDTO.DocumentExternalURL = row.GetValue<string>(request.DocumentExternalURL);
                    CloudContentDTO.Page = row.GetValue<string>(request.Page);
                    CloudContentDTO.LogoURL = row.GetValue<string>(request.LogoURL);
                    CloudContentDTO.ContentType = row.GetValue<string>(request.ContentType);
                    CloudContentDTO.Technology = row.GetValue<string>(request.Technology);
                    CloudContentDTO.Architecture = row.GetValue<string>(request.Architecture);
                    CloudContentDTO.DateCreated = row.GetValue<DateTime>(request.DateCreated);
                    listData.Add(CloudContentDTO);
                }
            }
            return listData;
        }
        private static List<string> EducationList = new List<string>(new[]
        {
            Constants.Architecture,
            Constants.Technology
        });

        private static List<string> EducationContentList
        {
            get { return EducationList; }
        }
        // Fetch Direct Ship Content data and populate respective list of DTOs.
        public EducationListResponse GetEducationContentList(EducationListRequest request)
        {
            EducationListResponse EducationLists = new EducationListResponse();
            foreach (string list in EducationContentList)
            {
                request.CustomListName = list;
                var listData = new List<string>();
                var listDataRows = GetCustomTableData(request);
                if (listDataRows != null)
                {
                    foreach (DataRow row in listDataRows)
                    {
                        listData.Add(row.GetValue<string>(request.Name));
                    }
                }
                if (listData != null)
                {
                    EducationLists.GetType().GetProperty(list).SetValue(EducationLists, listData);
                }
            }
            return EducationLists;
        }
        private string AbsoluteURL(string url, KenticoInstance ki)
        {
            string absoluteUrl = url;
            if (!string.IsNullOrEmpty(absoluteUrl) && ki != null)
            {
                string newUrl = "//" + ki.com;
                if (!string.IsNullOrEmpty(ki.site))
                {
                    newUrl += "/" + ki.site;
                }
                absoluteUrl = newUrl + absoluteUrl.Replace("~", "");
            }
            return absoluteUrl;
        }
        private KenticoInstance getKenticoInstance(string aSiteCode)
        {
            RulesController rc = new RulesController();
            string ContentDomain = rc.GetContentDomainName(aSiteCode);
            KenticoInstance ki = KenticoInstanceConfigHelper.Instance.GetKenticoInstance(ContentDomain);
            return ki;
        }

        // Fetch Direct Ship Content data and populate respective list of DTOs.
        public CloudVendorsResponse GetCloudVendors(CloudVendorsRequest request)
        {
            CloudVendorsResponse response = new CloudVendorsResponse();
            var listDataRows = GetCustomTableData(request);
            if (listDataRows != null & listDataRows.Count > 0)
            {
                response.CompanyDetails = listDataRows[0].GetValue<string>(request.CompanyDetails);
            }
            return response;
        }

        public bool UpdateCustomPromotion(CustomPromotionRequest request, CustomPromotionDTO data, bool isUpdateAttachmentOnly)
        {
            var documentNode = KenticoApiManager.GetDocumentNode(request.ListName, request.UserSelectedCulture, null, request.Criteria);
            if (!isUpdateAttachmentOnly)
            {
                //Update all non attachment fields using DTO
            }

            AttachmentManager attachmentManager = new AttachmentManager();
            if (data.Attachment != null)
            {
                attachmentManager.UpdateAttachment(documentNode, data.AttachmentName, data.Attachment, request.AttachmentPromotion);
            }
            return true;
        }

        // Common method to fetch any list data from Knetico.

        public IEnumerable<SavedEventDTO> GetSavedEventLists(SavedEventRequest request)
        {
            var listData = new List<SavedEventDTO>();
            var listDataRows = GetListData(request);
            if (listDataRows != null)
            {
                foreach (DataRow row in listDataRows)
                {
                    var savedEventDTO = new SavedEventDTO();
                    savedEventDTO.EventName = row.GetValue<string>(request.EventName);
                    savedEventDTO.EventSponsor = row.GetValue<string>(request.EventSponsor);
                    savedEventDTO.EventLocation = row.GetValue<string>(request.EventLocation);
                    savedEventDTO.EventPublic = row.GetValue<bool>(request.EventPublic);
                    savedEventDTO.EventID = row.GetValue<int>(request.EventsID);
                    savedEventDTO.EventDateBegin = row.GetValue<DateTime>(request.EventDateBegin);
                    savedEventDTO.EventDateEnd = row.GetValue<DateTime>(request.EventDateEnd);
                    savedEventDTO.EventLinkURL = row.GetValue<string>(request.EventLinkURL);
                    savedEventDTO.NodeAlias = row.GetValue<string>(request.NodeAlias);
                    listData.Add(savedEventDTO);
                }
            }
            return listData;
        }
    }
}
