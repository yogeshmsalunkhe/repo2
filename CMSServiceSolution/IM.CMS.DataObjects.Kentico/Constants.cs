﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IM.CMS.DataObjects.Kentico
{
    public static class Constants
    {
        public const string DefaultCulture = "en-us";
        public const string DBConfigurationPrefix = "KenticoDBConfig";
        public const string GetGuidSP = "IMCustomSP_GetContactGuid";
        public const string GetContactGroupResellerIdsSP = "IMCustomSP_GetContactGroupResellerIds";

        #region Kentico document constants

        public const string ContentListNameSpace = "IngramMicro_DocumentType";        

        #endregion
        public const string CustumTableListNameSpace = "IngramMicro_CustomTable";

        #region Kentico Custom table constants
        //Custom table
        public const string Architecture = "Architecture";
        public const string Technology = "Technology";

        public const string QuotesPreferencesLogoPath = "QuotesPreferencesLogoPath";
        public const string ExternalMediaLibrary = "ExternalMediaLibrary";
        public const string Mediafiletitle = "Document Title";
        public const string QuotesPreferencesLogo = "QuotePreferencesLogos";
        public const string SavedEvent = "Events";
       
        #endregion
    }
}
