﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Controls
{
    /// <summary>
    /// Base class for transformation methods -- needed to access Kentico's CMS ascx transformations if needed later
    /// </summary>
    public partial class CMSTransformation //: CMSAbstractTransformation
    {
    }
}
