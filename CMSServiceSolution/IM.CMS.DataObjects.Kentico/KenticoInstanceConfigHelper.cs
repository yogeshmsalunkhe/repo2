﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Xml.Serialization;
namespace IM.CMS.Integration
{
    /// <summary>
    /// Class Representing a KenticoInstance
    /// </summary>
    public class KenticoInstance
    {
        [XmlElement("dmz")]
        public string dmz { get; set; }
        [XmlElement("com")]
        public string com { get; set; }
        [XmlElement("site")]
        public string site { get; set; }
        [XmlElement("dup")]
        public string dup { get; set; }
        [XmlElement("mediaFolderPathTargets")]
        public mediaFolderPathTargets mediaFolderPathTargets { get; set; }
    }

    public class mediaFolderPathTargets
    {
        [XmlElement("targetMediaFolderPath")]
        public List<string> targetMediaFolderPath { get; set; }
    }

    /// <summary>
    /// Class Representing data in KenticoInstance.config
    /// </summary>
    public class KenticoInstances
    {
        [XmlElement("KenticoInstance")]
        public KenticoInstance[] Instances { get; set; }
    }

    /// XML Parser to read KenticoInstance.config
    /// </summary>
    public static class KenticoInstanceConfigReader
    {
        public static KenticoInstances GetIMOnlineConfig()
        {
            try
            {
                string path = HttpRuntime.AppDomainAppPath + "\\KenticoInstance.config";
                using (TextReader reader = new StreamReader(path))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(KenticoInstances));
                    KenticoInstances ki = (KenticoInstances)serializer.Deserialize(reader);
                    return ki;
                    //                return (KenticoInstances)serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {
            }
            return null;
        }
    }

    /// <summary>
    /// Class to return corresponding public domain & site for a given DMZ
    /// </summary>
    public sealed class KenticoInstanceConfigHelper
    {
        // singleton instance
        private static volatile KenticoInstanceConfigHelper m_instance;
        private static object syncRoot = new Object();

        #region instance variables
        /// <summary>
        /// temporary list of KenticoInstances
        /// </summary>
        public KenticoInstances m_allKenticoInstances;
        
        #endregion

        #region constructor
        private KenticoInstanceConfigHelper()
        {
            // load the XML when the class is loaded:
            m_allKenticoInstances = KenticoInstanceConfigReader.GetIMOnlineConfig();            
        }
        #endregion

        /// <summary>
        /// Returns the singleton object
        /// </summary>
        public static KenticoInstanceConfigHelper Instance
        {
            get
            {
                // first time only - find the Kentico instance for the current application instance (e.g Spain stage 4)
                // and store it in the singleton instance variable
                if (m_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (m_instance == null)
                            m_instance = new KenticoInstanceConfigHelper();
                    }
                }
                return m_instance;
            }
        }

        /// <summary>
        /// Returns a KenticoInstance object for the given DMZ
        /// </summary>
        /// <param name="dmzSite"></param>
        /// <returns></returns>
        public KenticoInstance GetKenticoInstance(string dmzSite)
        {
            KenticoInstance rtnVar = null;
            foreach (var instance in m_allKenticoInstances.Instances)
            {
                if (instance.dmz.Equals(dmzSite))
                {
                    rtnVar = instance;
                    break;
                }
            }
            return rtnVar;
        }
    }
}
