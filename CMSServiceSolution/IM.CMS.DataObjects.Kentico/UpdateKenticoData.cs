﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Kentico namespaces
using CMS.Membership;

//IM namespaces
using IM.CMS.CommonContracts;
using IM.ErrorLoggingUtilities;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace IM.CMS.DataObjects.Kentico
{
    public class UpdateKenticoData
    {
        public static void CreateUserRequest(CreateUserRequest request)
        {
            try
            {
                Logger.Write("User creation started for User Name :", request.UserName);
                KenticoApiManager kam = KenticoApiManager.GetInstance(ListManager.GetConnectionString(request.SiteCode));

                UserInfo newUser = new UserInfo();
                newUser.UserName = request.UserName;
                newUser.FullName = request.FullName;
                newUser.UserGUID = request.UserGUID;
                newUser.UserEnabled = true;
                newUser.SetPrivilegeLevel(UserPrivilegeLevelEnum.None);
                UserInfoProvider.SetUserInfo(newUser);
                Logger.Write("User creation finished for User Name :", request.UserName);
            }
            catch (Exception ex)
            {               
                ErrorLogging.HandleException(ex, "Error while creating user in UpdateKenticoData fun");
                Logger.Write(string.Format("Error occured in CreateUser for user name {0} {1}", request.UserName, ex.Message), Category.General, Priority.Highest);
            }
        }
    }
}
