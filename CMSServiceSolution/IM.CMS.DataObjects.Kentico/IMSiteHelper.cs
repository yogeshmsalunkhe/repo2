﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.DataEngine;
using CMS.SettingsProvider;
using CMS.SiteProvider;
// Custom 3 party namespaces
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace IM.CMS.Integration
{
    /// <summary>
    /// Summary description for IMSiteHelper
    /// </summary>
    public class IMSiteHelper
    {
        public static SiteInfo GetSite()
        {
            //find dmz site url and .com site url.
            //InfoDataSet<SiteInfo> siteInfoList = SiteInfoProvider.GetAllSites();
            InfoDataSet<SiteInfo> siteInfoList = SiteInfoProvider.GetSites().TypedResult;
            foreach (SiteInfo site in siteInfoList)
            {
                if (site.DomainName.Contains(".dmz"))
                {
                    return site;
                }
            }
            return null;
        }

        public static KenticoInstance GetKenticoInstance(SiteInfo aSite)
        {
            return KenticoInstanceConfigHelper.Instance.GetKenticoInstance(aSite.DomainName);
        }

        public static string ModifyURL(string url)
        {
            string absoluteUrl = "";
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(url.Trim()))
            {
                if (!url.StartsWith("http") && !url.StartsWith("https") && !url.StartsWith("//"))
                {
                    string arrPath = GetArrPath();                    
                    if (url.StartsWith("/cms") || url.StartsWith("/CMSPages"))
                        absoluteUrl = url;
                    else if (url.StartsWith("/_layouts") || url.Contains("CMSScripts"))
                        absoluteUrl = url;
                    else if (!url.StartsWith("/c"))
                        absoluteUrl = "/c" + url;
                    else
                        absoluteUrl = url;
                }
                else
                    absoluteUrl = url;

            }
            return absoluteUrl.Replace("~", "");
        }
        public static string ParseHtml(string finalHtml)
        {           
            if (!string.IsNullOrEmpty(finalHtml))
            {
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(finalHtml);
                try
                {
                    HtmlNodeCollection nodes = htmlDoc.DocumentNode.SelectNodes("//@href | @a | //@src");
                    if (nodes != null)
                    {
                        SiteInfo site = IMSiteHelper.GetSite();
                        KenticoInstance ki = IMSiteHelper.GetKenticoInstance(site);
                        string arrPath = GetArrPath();
                        foreach (HtmlNode node in nodes)
                        {
                            if (node.Name.Equals("a"))
                            {
                                ProcessAnchorNode(node);
                            }
                            else if (node.Name.Equals("img"))
                            {
                                HtmlAttribute attImg = node.Attributes["src"];
                                ProcessSubNode(attImg, arrPath);
                            }
                            else if (node.Name.Equals("link"))
                            {
                                HtmlAttribute attHref = node.Attributes["href"];
                                ProcessSubNode(attHref, arrPath);
                            }
                            else if (node.Name.Equals("script"))
                            {
                                HtmlAttribute attSrc = node.Attributes["src"];
                                ProcessSubNode(attSrc, arrPath);
                            }
                        }
                        return htmlDoc.DocumentNode.OuterHtml;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return finalHtml;
        }

        public static void ProcessSubNode(HtmlAttribute attSrc, string arrPath)
        {
            if (attSrc != null)
            {
                string src = attSrc.Value;
                if (IsRelativeUrl(src))
                    attSrc.Value = ProcessResourceUrl(src, arrPath, string.Empty);
            }
        }
        public static string GetArrPath()
        {
            SiteInfo site = IMSiteHelper.GetSite();
            KenticoInstance ki = IMSiteHelper.GetKenticoInstance(site);
            string arrPath = ki != null ? ki.com : string.Empty;
            if (arrPath.Contains("cms"))
                arrPath = arrPath.Substring(0, arrPath.Length - 4);

            return arrPath;
        }
        private static int CheckTildSymbol(string url)
        {
            int urlStartIndex = 0;
            if (url.StartsWith("~"))
                urlStartIndex = 1;
            else if (url.StartsWith("%7e"))
                urlStartIndex = 3;

            return urlStartIndex;
        }

        private static string ProcessResourceUrl(string url, string arrPath, string protocol)
        {
            url = url.Substring(CheckTildSymbol(url));
            return string.Format("{0}//{1}{2}", protocol, arrPath, url);
        }

        private static void ProcessAnchorNode(HtmlNode anchorNode)
        {
            HtmlAttribute attHref = anchorNode.Attributes["href"];
            string arrPath = GetArrPath();
            if (attHref == null)
                attHref = anchorNode.Attributes["src"];

            if (attHref != null)
            {
                string href = attHref.Value;
                if (href.ToLower().StartsWith("javascript") || href.ToLower().StartsWith("mailto:") || href.ToLower().StartsWith("#"))
                    return;

                if (!anchorNode.Attributes.Contains("target"))
                    anchorNode.Attributes.Add("target", "_parent");
                else if (anchorNode.Attributes["target"].Value == "" || anchorNode.Attributes["target"].Value == "_self")
                    anchorNode.Attributes["target"].Value = "_parent";

                if (href == "" || href == "/")
                    return;

                bool isHrefModified = false;
                string urlType = string.Empty;
                HtmlAttribute attUrlType = anchorNode.Attributes["url-type"];
                if (attUrlType != null)
                    urlType = attUrlType.Value; // vanity: don't add any prefix.

                href = href.Substring(CheckTildSymbol(href));
                if (IsRelativeUrl(href) && !urlType.Equals("vanity"))
                {
                    string url = href;
                    if (urlType.Equals("static") || url.StartsWith("media") || url.StartsWith("secured-documents"))
                    {
                        href = "/cms/" + url;
                    }
                    else if (url.StartsWith("/_layouts") || url.StartsWith("/cms"))
                    {
                        href = url;
                    }
                    else
                    {
                        href = href.Replace("/ContentPages", "").Replace("/Content-Pages", "");
                        if (!href.StartsWith("/c"))
                            href = "/c" + href;                      
                    }
                    string lasttwoChar = href.Substring((href.Length - 3), 3);
                    if (lasttwoChar.ToLower() == "/c/" || href =="/c/")               
                        href = "/";
                  
                    isHrefModified = true;
                }
                if (isHrefModified)
                    attHref.Value = href;
            }
        }

        private static bool IsRelativeUrl(string url)
        {
            if (!url.StartsWith("http") && !url.StartsWith("//"))
                return true;

            return false;
        }
        public static string ModifyKenticoPageURL(string url)
        {
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(url.Trim()))
            {
                if (!url.StartsWith("http") && !url.StartsWith("//") && !url.ToUpper().StartsWith("WWW"))
                {                 
                    url = url.Replace("/ContentPages", "").Replace("/Content-Pages", "");
                    if (!url.StartsWith("/c"))
                    {
                        if(url.StartsWith("/"))
                        {
                        url = "/c" + url;
                        }
                        else
                        {
                            url = "/c/" + url;
                        }
                    }

                    if (!url.Contains(".aspx"))
                    {
                        url += ".aspx";
                    }
                }
            }
            return url.Replace("~", "");
        }
    }
}
