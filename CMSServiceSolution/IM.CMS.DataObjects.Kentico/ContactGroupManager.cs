﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging;

//Kentico namespaces
using CMS.MacroEngine;
using CMS.OnlineMarketing;
using CMS.DataEngine;

//IM namespaces
using IM.CMS.CommonContracts;
using IM.CMS.Rules.Kentico;
using IM.ErrorLoggingUtilities;

namespace IM.CMS.DataObjects.Kentico
{
    public class ContactGroupManager
    {
        public KenticoApiManager KenticoApiManager { get; set; } // Manage Kentico API.       


        // Define and initialize Kentico API manager.
        public ContactGroupManager(RequestBase request)
        {
            var rule = new RulesController();
            string conStr = rule.GetCmsDbString(request.SiteCode);
            KenticoApiManager = KenticoApiManager.GetInstance(conStr);
        }

        private string ModifyContactGroupCondition(UpdateContactGroupResellersRequest request)
        {
            string ruleName = "ContactFieldIsInACustomList";
            string field = "field";
            string fieldDataType = "string";
            string list = "list";
            string listDataType = "longtext";
            int macroPosition = 0;

            var rule = MacroRuleInfoProvider.GetMacroRuleInfo(ruleName);
            if (rule == null) return "";

            var ruleTree = new MacroRuleTree();
            ruleTree.AddRule(rule, macroPosition);

            var paramField = ruleTree.Children[macroPosition].Parameters[field];
            paramField.ValueType = fieldDataType;
            paramField.Text = "IM - Reseller ID";
            paramField.Value = "IMResellerID";

            var paramList = ruleTree.Children[macroPosition].Parameters[list];
            paramList.ValueType = listDataType;

            string resellersStr = string.Join("\n", request.Resellers.ToArray());
            paramList.Text = resellersStr;
            paramList.Value = resellersStr;

            var macroCondition = ruleTree.GetCondition();
            var xml = ruleTree.GetXML();

            var contactGroupDynamicConditionValue = "Rule(\"" + MacroElement.EscapeSpecialChars(macroCondition) + "\", \"" + MacroElement.EscapeSpecialChars(xml) + "\")";
            return contactGroupDynamicConditionValue;
        }

        public bool UpdateContactGroupResellers(UpdateContactGroupResellersRequest request)
        {
            var rule = new RulesController();
            string siteName = rule.GetContentDomainName(request.SiteCode);

            ContactGroupInfo contactGroup = null;
            var contactGroupQuery = ContactGroupInfoProvider.GetContactGroups();
            contactGroupQuery = contactGroupQuery.Where("ContactGroupDisplayName", QueryOperator.Equals, request.GroupName);
            contactGroup = contactGroupQuery.FirstObject;

            if (contactGroup != null)
            {
                contactGroup.ContactGroupDynamicCondition = ModifyContactGroupCondition(request);
                ContactGroupInfoProvider.SetContactGroupInfo(contactGroup);

                string url = string.Format("http://{0}/imcmsapi/imcmsapi/RebuildContactGroup?groupName={1}", siteName, contactGroup.ContactGroupName);
                var webRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                var webResponse = (System.Net.HttpWebResponse)webRequest.GetResponse();
                return true;
            }
            else
            {
                throw new Exception(string.Format("Contact group {0} not found.", request.GroupName));
            }
            return false;
        }
    }
}
