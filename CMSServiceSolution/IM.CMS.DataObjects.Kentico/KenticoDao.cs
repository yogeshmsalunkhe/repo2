﻿using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Web;
using System.Web.UI;

// IM namespaces
using IM.CMS.Rules.Kentico;
using IM.CMS.CommonContracts;
using IM.CMS.CommonContracts.CMSInterfaces;
using IM.CMS.DataObjects.Kentico;
using IM.CMS.Integration;
// Custom 3 party namespaces
using HtmlAgilityPack;

namespace IM.CMS.DataObjects.Kentico
{
    public class KenticoSegDao : ISegmentationDAO
    {
        private string ContactGuid { get; set; }
        private string ContactCulture { get; set; }
        private string ContentDomain { get; set; }
        private string ContentBaseUrl { get; set; }
        private RulesController KenticoRulesProvider { get; set; }
        private ISegmentationDAO SegDataAccess { get; set; }

        public KenticoSegDao()
        {
            //defaults and setup providers
            ContactGuid = string.Empty;
            ContentDomain = string.Empty;
            ContentBaseUrl = string.Empty;
            ContactCulture = Constants.DefaultCulture;
            KenticoRulesProvider = new RulesController();
        }

        public SegResultDAO GetDisplayRegions(RegionRequest request)
        {
            SegResultDAO result = new SegResultDAO();

            ContentBaseUrl = KenticoRulesProvider.GetContentBaseUrl(request.SiteCode); // e.g. "localhost/KenticoCMS7"
            result.CMSBaseUrl = ContentBaseUrl;

            ContentDomain = KenticoRulesProvider.GetContentDomainName(request.SiteCode); // e.g. "localhost"

            if (!string.IsNullOrEmpty(request.UserSelectedCulture))
            {
                ContactCulture = request.UserSelectedCulture.ToLower();
            }

            // Use agility pack to get html and parse dom.
            HtmlWeb web = new HtmlWeb();
            web.AutoDetectEncoding = true;
            // Set the agent to mimic a recent browser e.g = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5";
            web.UserAgent = request.BrowserUserAgent;

            // Determine ContactGuid in Kentico from contact info given in request or use it if it's already in the request
            if (String.IsNullOrEmpty(request.KenticoCurrentContactGuid) && !String.IsNullOrEmpty(request.SAMAccountName)
                && !String.IsNullOrEmpty(request.SiteCode))
            {
                ContactGuid = GetKenticoContactGuid(request.SiteCode, request.SAMAccountName);
            }
            else
            {
                ContactGuid = request.KenticoCurrentContactGuid;
            }
            web.UseCookies = true;
                web.PreRequest = new HtmlWeb.PreRequestHandler(OnPreRequest);
            web.PostResponse = new HtmlWeb.PostResponseHandler(OnPostResponse);

            // Get config -- setting -- pass culture get url of kentico external site. add page to it.also if logged in or not.  result is e.g. "http://localhost/KenticoCMS7/loggedout/homepage.aspx";
            // To Do:  can use issecureconnection property to update all http links to https if needed. assuming kentico doesn't have it done already.
            //Advert Management changes - added request.sitecode parameter to read config values
            string url = String.Format("http://{0}/{1}", ContentBaseUrl, KenticoRulesProvider.GetContentPage(request.SAMAccountName, request.HostPageName, request.UseLoggedInOut, request.SiteCode));
            if (request.ParamURL != null && request.ParamURL.Length > 0)
            {
                if (url.IndexOf("?") > -1)
                    url += "&";
                else
                    url += "?";

                url += "URL=" + request.ParamURL;
            }
            url = url.Replace("&amp;", "&");

            // Initiate web request            
            HtmlDocument doc = web.Load(url);
            if (isPage404(doc))
            {
                string newUrl = url.Replace("Content-Pages/", "").Replace("ContentPages/", "");
                doc = web.Load(newUrl);
            } 
            if (doc != null && doc.ParseErrors.ToList().Count > 0)
            {
                // Throw error if html is malformed (except if has unneeded tags)
                if (doc.ParseErrors.ToList().Find(x => x.Code != HtmlParseErrorCode.EndTagNotRequired) != null)
                {
                    throw new System.Exception("Document at " + url + " threw exception when parsing html.");
                }
            }

            // place html into region objects
            result.Regions = ParseHtml(doc, request.SiteCode);

            // To Do:  can use issecureconnection property to update all http links to https if needed. assuming kentico doesn't have it done already.

            result.ContactId = ContactGuid;
            return result;
        }
        private bool isPage404(HtmlDocument htmlDoc)
        {
            string headerBlock = "//head";
            HtmlNodeCollection resultHtml = htmlDoc.DocumentNode.SelectNodes(headerBlock);
            if (resultHtml != null && resultHtml.Count > 0)
            {
                foreach (var element in resultHtml.Nodes())
                {
                    if (element.Name.Equals("title"))
                    {
                        //Check if element title is "Page404"
                        if (element.InnerText.Contains("Page404"))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        } 
        // Called just after response
        private void OnPostResponse(HttpWebRequest request, HttpWebResponse response)
        {
           var httpResponse = request.GetResponse() as HttpWebResponse;
           if (httpResponse != null)
           {
               var cookies = httpResponse.Cookies;
                if (cookies != null && cookies["CurrentContact"] != null)
                    ContactGuid = cookies["CurrentContact"].Value.ToString();
           }
        }

        // Called just prior to when web request from agility pack is made
        private bool OnPreRequest(HttpWebRequest request)
        {
            request.CookieContainer = SetupKenticoCookieContainer();
            request.Referer = "http://im.cms.service"; // To notify Kentico application that cms service has made a request. 
            return true;
        }

        private CookieContainer SetupKenticoCookieContainer()
        {
            CookieContainer cookiePot = new CookieContainer();
            Cookie cookie;
            if (!String.IsNullOrEmpty(ContactGuid))
            {
                // Create contact cookie
                cookie = new Cookie("CurrentContact", ContactGuid);
                //e.g. contactCookie.Domain = "localhost";
                cookie.Domain = ContentDomain;
                cookie.Path = "/";
                cookie.HttpOnly = true;
                cookiePot.Add(cookie);
            }

            if (!String.IsNullOrEmpty(ContactCulture))
            {
                // Create culture cookie
                cookie = new Cookie("CMSPreferredCulture", ContactCulture);
                cookie.Domain = ContentDomain;
                cookie.Path = "/";
                cookie.HttpOnly = true;
                cookiePot.Add(cookie);
            }
            return cookiePot;
        }

        private IList<DisplayRegionDAO> ParseHtml(HtmlDocument htmlDoc, string aCallingDomain)
        {
            IList<DisplayRegionDAO> regions = new List<DisplayRegionDAO>();
            DisplayRegionDAO region = null;

            // Get the title and meta tags
            string headerBlock = "//head";
            HtmlNodeCollection resultHtml = htmlDoc.DocumentNode.SelectNodes(headerBlock);
            if (resultHtml != null && resultHtml.Count > 0)
            {
                foreach (var element in resultHtml.Nodes())
                {
                    if (element.Name.Equals("title"))
                    {
                        region = new DisplayRegionDAO();
                        region.Html = element.InnerHtml;
                        region.TagId = "Title";
                        regions.Add(region);
                    }
                    else if (element.Name.Equals("meta"))
                    {
                        region = new DisplayRegionDAO();
                        HtmlAttribute ha = element.Attributes["name"];
                        if (ha != null)
                        {
                            region.MetaName = element.Attributes["name"].Value;
                        }
                        ha = element.Attributes["content"];
                        if (ha != null)
                        {
                            region.MetaContent = element.Attributes["content"].Value;
                        } 
                        if (!string.IsNullOrEmpty(region.MetaName) && !string.IsNullOrEmpty(region.MetaContent))
                        {
                            region.TagId = "Meta";
                            region.Html = ""; // not null
                            regions.Add(region);
                        }
                    } 
                    else if (element.Name.Equals("link") && element.Attributes.Contains("rel") && element.Attributes["rel"].Value.ToLower().Equals("canonical")) 
                    {
                        region = new DisplayRegionDAO();
                        region.TagId = element.Attributes["rel"].Value.ToLower();
                        region.Html = "";
                        region.MetaContent = element.Attributes["href"].Value;
                        regions.Add(region);
                    }
                }
            }
            // Get all div html in the main block section
            string divmainBlock = "//div[@id='wrapper']";
            resultHtml = htmlDoc.DocumentNode.SelectNodes(divmainBlock);

            if (resultHtml != null && resultHtml.Count > 0)
            {
                // Get any collection of nodes elements that are in list of given ids
                // e.g. string divSelection = "//div[@id='leaderboard' or @id='adrotator' or @id='sidebar']";
                string divSelection = KenticoRulesProvider.GetDivsSelection();
                HtmlNodeCollection nodeCollection = resultHtml[0].SelectNodes(divSelection);
                if (nodeCollection != null)
                {
                    foreach (var element in nodeCollection)
                    {
                        if (element.NodeType == HtmlNodeType.Element && element.Name == "div")
                        {
                            region = new DisplayRegionDAO();
                            region.Html = element.InnerHtml;
                            region.TagId = element.Id;
                            if ("cms-main-content".Equals(element.Id) || "sidebarRight".Equals(element.Id))
                            {
                                // we need to prefix urls with /cms
                                region.Html = AddLogicalRootToUrls(region.Html, aCallingDomain);
                            }
                            regions.Add(region);
                        }
                    }
                }
            }
            return regions;
        }

        private string AddLogicalRootToUrls(string aHtml, string aSiteCode)
        {
            HtmlDocument fragment = new HtmlDocument();
            fragment.LoadHtml(aHtml);
            HtmlNodeCollection nodes = fragment.DocumentNode.SelectNodes("//a[@class='CMSListMenuLink'] | //a[@class='CMSListMenuLinkHighlighted'] | //a[@class='CMSBreadCrumbsLink'] | //a[@class='aNewsEventsTitle'] | //a[@class='CMSBreadCrumbsCurrentItem'] | //a[@class='aBackToAllNewsEvents'] | //a[@class='tab_CMSListMenuLink'] | //a[@class='tab_CMSListMenuLinkHighlighted'] | //div[@class='PagerNumberArea']//a | //div[@class='HorizontalMenu']//a ");
            if (nodes != null)
            {
                foreach (var node in nodes)
                {
                    //check for externalUrl attribute. Retain hrfe value for external urls
                    //To do (Service and support using this code for checking external URL)
                    //if (node.Attributes["externalUrl"] == null) {
                        var n1 = node.Attributes["href"];
                        if (n1 != null) {
                            if (IsRelativeUrl(n1.Value))
                            {
                                string href = n1.Value;
                                if (href.StartsWith("~"))
                                    href = href.Remove(0, 1);

                                href = "/c" + href;

                                node.Attributes["href"].Value = href.Replace("/ContentPages/", "/");
                                //Advert Management Changes
                                node.Attributes["href"].Value = href.Replace("/Content-Pages/", "/");

                            }
                        }
                    //}
                }
            }

            RulesController rc = new RulesController();
            string ContentDomain = rc.GetContentDomainName(aSiteCode);
            KenticoInstance ki = KenticoInstanceConfigHelper.Instance.GetKenticoInstance(ContentDomain);
            string newUrl = "http://";
            if (ki != null) {
                newUrl += ki.com;
                if (!string.IsNullOrEmpty(ki.site)) {
                    newUrl += "/" + ki.site;
                }

                // now deal with image URLs
                nodes = fragment.DocumentNode.SelectNodes("//img");
                if (nodes != null && nodes.Count > 0) {
                    foreach (var node in nodes) {
                        //check for externalUrl attribute. Retain href value for external urls
                        var n1 = node.Attributes["src"];
                        if (n1 != null)
                        {
                            string src = n1.Value;
                            if (!string.IsNullOrEmpty(src) && src.StartsWith("/media"))
                            {
                                n1.Value = newUrl + src;
                            }
                        }
                    }
                }

                // inline styles which use images, e.g. Page404
                nodes = fragment.DocumentNode.SelectNodes("//*[@style]"); // all nodes with a style attribute
                if (nodes != null && nodes.Count > 0) {
                    foreach (var node in nodes) {
                        string nval = node.Attributes["style"].Value;
                        if (!string.IsNullOrEmpty(nval)) {
                            int pos = nval.IndexOf("background-image");
                            if (pos > -1) {
                                pos = nval.IndexOf("url('/media", pos);
                                if (pos > -1) {
                                    // insert the absolute URL before /media
                                    string temp = nval.Substring(0, pos + 5);
                                    temp += newUrl;
                                    temp += nval.Substring(pos + 5);
                                    node.Attributes["style"].Value = temp;
                                }
                            }
                        }
                    }
                }
            }

            return fragment.DocumentNode.OuterHtml;
        }
        
        private string GetKenticoContactGuid(string siteId, string contactSAMAccount)
        {
            // Get kentico site name for given imonline siteid
            string siteName = KenticoRulesProvider.GetContentDomainSiteDisplayName(siteId);

            // This needs to change for data access based on domain to specify which db config to use.
            DataLayer dbAccess = new DataLayer(Constants.DBConfigurationPrefix + ' ' + siteName);

            // Use site name and sam account to find contact guid if any that exists in kentico
            return dbAccess.GetContactGuid(contactSAMAccount, siteName);

            /**
            posible alternate strategy if need to create Kentico contact as well:
            * (neeed userid, reseller, first/last name and list of roles|)
            use given reseller id, user id, and IM privileges 
            1) check if userid/reseller combo exists-- no? create it with priv settings.
            2) check if first/last name and im privieges are exact - no?  recreate it.
            3) look up contact list for matching company/userid combo. 
            -- get contact guid from table link.
            -- return guid 
            **/
        }

        /// <summary>
        /// Forwards a Banner Click event to Kentico to count the click
        /// </summary>
        /// <param name="bannerClickUrl"></param>
        public void BannerClick(string bannerClickUrl)
        {
            HtmlWeb web = new HtmlWeb();
            web.Load(bannerClickUrl); // increment the Kentico click counter
        }

        private static bool IsRelativeUrl(string url)
        {
            if (!url.StartsWith("http") && !url.StartsWith("//"))
                return true;

            return false;
        }
    }
}
