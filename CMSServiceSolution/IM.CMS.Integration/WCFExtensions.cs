﻿using System;
using System.ServiceModel;
using IngramMicro.Commerce.Portal.Utilities.Managers;

namespace IM.CMS.Integration
{
    public static class WcfExtensions
    {
        public static void Using<T>(this T client, Action<T> work)
            where T : ICommunicationObject
        {
            try
            {
                work(client);
                client.Close();
            }
            catch (CommunicationException e)
            {
                if (e.GetType() == typeof(ProtocolException))
                    LogManager.LogException("CMS service host entry may be missing", EventId.CMSServiceCallError, e);
                else if(e.GetType() == typeof(EndpointNotFoundException))
                    LogManager.LogException("CMS service host entry IP may be wrong", EventId.CMSServiceCallError, e);
                else
                    LogManager.LogException(string.Empty, EventId.CMSServiceCallError, e);

                client.Abort();
            }
            catch (TimeoutException e)
            {
                LogManager.LogException(string.Empty, EventId.CMSServiceTimeout, e);
                client.Abort();
            }
            catch (Exception e)
            {
                LogManager.LogException(string.Empty, EventId.CMSServiceCallError, e);
                client.Abort();
                throw;
            }
        }
    }
}
