﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using IM.CMS.Integration.CMSServiceProxy;
//using IM.CMS.CommonContracts;

namespace IM.CMS.Integration
{
    public class CMSController
    {
        private CMSServiceClient GetClient(string aServiceUrl)
        {
            CMSServiceClient client = new CMSServiceClient();
            if (!string.IsNullOrEmpty(aServiceUrl))
            {
                var address = new EndpointAddress(aServiceUrl);
                client.Endpoint.Address = address;
            }
            return client;
        }

        public RegionsResponse GetHtmlForRegions(string serviceUrl, RegionRequest request)
        {
            RegionsResponse response = new RegionsResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetDisplayRegions(request);
            });

            return response;
        }

        public void BannerClick(string serviceUrl, string bannerClickUrl)
        {
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                channel.BannerClick(bannerClickUrl);
            });

        }

        public CustomPromotionResponse GetCustomPromotion(string serviceUrl, CustomPromotionRequest request)
        {
            var response = new CustomPromotionResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetCustomPromotion(request);
            });
            return response;
        }

        public ReusableContentResponse GetReusableContent(string serviceUrl, ReusableContentRequest request)
        {
            var response = new ReusableContentResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetReusableContent(request);
            });
            return response;
        }

        public ExternalSiteInIframeResponse GetExternalSiteInIframe(string serviceUrl, ExternalSiteInIframeRequest request)
        {
            var response = new ExternalSiteInIframeResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetExternalSiteInIframe(request);
            });
            return response;
        }

        public MarketingUrlResponse GetMarketingUrl(string serviceUrl, MarketingUrlRequest request)
        {
            var response = new MarketingUrlResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetMarketingUrl(request);
            });
            return response;
        }


        public BoutiqueResponse GetBoutiques(string serviceUrl, BoutiqueRequest request)
        {
            var response = new BoutiqueResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetBoutiques(request);
            });
            return response;
        }

        public SocialMediaResponse GetSocialMedia(string serviceUrl, SocialMediaRequest request)
        {
            var response = new SocialMediaResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetSocialMedia(request);
            });
            return response;
        }      

        /// <summary>
        /// Returns the URL for the Kentico Banner Redirect page
        /// </summary>
        /// <param name="serviceUrl"></param>N B
        /// <param name="request"></param>
        /// <returns></returns>
        public string GetBannerRedirectUrl(string serviceUrl, BannerUrlRequest request)
        {
            CMSServiceClient client = GetClient(serviceUrl);
            string rtnVar = string.Empty;
            client.Using(channel =>
            {
                rtnVar = channel.GetBannerRedirectUrl(request);
            });
            return rtnVar;
        }

        /// <summary>
        /// Returns the Advert URL for a given Banner ID
        /// </summary>
        /// <param name="serviceUrl"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public string GetBannerUrl(string serviceUrl, BannerUrlRequest request)
        {
            CMSServiceClient client = GetClient(serviceUrl);
            string rtnVar = string.Empty; client.Using(channel =>
            {
                rtnVar = channel.GetBannerUrl(request);
            });
            return rtnVar;
        }

        public WebExclusivePromotionResponse GetWebExclusivePromotions(string serviceUrl, WebExclusivePromotionRequest request)
        {
            var response = new WebExclusivePromotionResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetWebExclusivePromotions(request);
            });
            return response;

        }

        public bool CheckContactGroupContainsContact(string serviceUrl, ContactGroupRequest request)
        {
            bool response = false;
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.CheckContactGroupContainsContact(request);
            });
            return response;

        }

        public void CreateUser(string serviceUrl, CreateUserRequest request)
        {
            CMSServiceClient client = GetClient(serviceUrl);
            client.Using(channel =>
            {
                channel.CreateUserRequest(request);
            });
        }
        public DirectShipContentResponse GetDirectShipContent(string serviceUrl, DirectShipContentRequest request)
        {
            var response = new DirectShipContentResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetDirectShipContent(request);
            });
            return response;
        }

        public CloudMenuResponse GetCloudMenu(string serviceUrl, CloudMenuRequest request)
        {
            var response = new CloudMenuResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetCloudMenu(request);
            });
            return response;
        }


      

        public EducationListResponse GetEducationContentList(string serviceUrl, EducationListRequest request)
        {
            var response = new EducationListResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetEducationContentList(request);
            });
            return response;
        }
        public CloudContentResponse GetCloudContent(string serviceUrl, CloudContentRequest request)
        {
            var response = new CloudContentResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetCloudContent(request);
            });
            return response;
        }

        public CloudServicesResponse GetCloudServices(string serviceUrl, CloudServiceRequest request)
        {
            var response = new CloudServicesResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetCloudServices(request);
            });
            return response;
        }

        public CloudVendorsResponse GetCloudVendors(string serviceUrl, CloudVendorsRequest request)
        {
            var response = new CloudVendorsResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetCloudVendors(request);
            });
            return response;
        }

        public ResponseBase UploadQuotePreferenceLogo(string serviceUrl, UploadQuotePreferenceLogoRequest request)
        {
            var response = new ResponseBase();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.UploadQuotePreferenceLogo(request);
            });
            return response;
        }

        public ResponseBase DeleteQuotePreferenceLogo(string serviceUrl, UploadQuotePreferenceLogoRequest request)
        {
            var response = new ResponseBase();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.DeleteQuotePreferenceLogo(request);
            });
            return response;
        }


        public SavedEventResponse GetSavedEvents(string serviceUrl, SavedEventRequest request)
        {
            var response = new SavedEventResponse();
            CMSServiceClient client = GetClient(serviceUrl);

            client.Using(channel =>
            {
                response = channel.GetSavedEvents(request);
            });
            return response;
        }
    }
}
