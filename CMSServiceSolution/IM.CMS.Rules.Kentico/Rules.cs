﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using Microsoft.Practices.EnterpriseLibrary.Logging;
using IM.ErrorLoggingUtilities;
using IM.CMS.CommonContracts;

namespace IM.CMS.Rules.Kentico
{
    public class RulesController
    {
        public string ValidateRegionRequest(RegionRequest request)
        {
            if (String.IsNullOrEmpty(request.SiteCode)) return RuleMessages.InvalidSiteCode;
            if (String.IsNullOrEmpty(request.UserSelectedCulture)) return RuleMessages.InvalidCulture;
            if (String.IsNullOrEmpty(request.HostPageName)) return RuleMessages.InvalidHostPageName;
            return RuleMessages.ValidationPassed;
        }

        public string ValidateListRequest(ListRequest request)
        {
            if (String.IsNullOrEmpty(request.SiteCode)) return RuleMessages.InvalidSiteCode;
            if (String.IsNullOrEmpty(request.UserSelectedCulture) && !request.IgnoreCulture) return RuleMessages.InvalidCultureConfig;
            return RuleMessages.ValidationPassed;
        }

        public string ValidateRequest(UpdateContactGroupResellersRequest request)
        {
            if (String.IsNullOrEmpty(request.SiteCode)) return RuleMessages.InvalidSiteCode;
            if (String.IsNullOrEmpty(request.GroupName)) return RuleMessages.GroupNameEmpty;
            return RuleMessages.ValidationPassed;
        }

        // get specific domain (url path) from web.config based on siteCode given
        public string GetContentDomainName(string siteCode)
        {
            // get specific domain (url path) from web.config based on culture given
            if (System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteDomainName + ' ' + siteCode] != null)
            {
                return System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteDomainName + ' ' + siteCode].ToString();
            }
            string logMessage = String.Format("{0} siteCode setting not found for domain. Using default.", siteCode);
            Logger.Write(logMessage, Category.General, Priority.Highest, EventId.GeneralMessage);
            // default to us settings.
            return System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteDomainName + ' ' + Constants.DefaultSiteCode].ToString();
        }

        // get kentico site name web.config based on siteCode given
        public string GetContentDomainSiteDisplayName(string siteCode)
        {
            // get specific domain (url path) from web.config based on culture given
            if (System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteDisplayNamePrefix + ' ' + siteCode] != null)
            {
                return System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteDisplayNamePrefix + ' ' + siteCode].ToString();
            }
            string logMessage = String.Format("{0} siteCode setting not found for kentico site name. Using default.", siteCode);
            Logger.Write(logMessage, Category.General, Priority.Highest, EventId.GeneralMessage);
            // default to us settings.
            return System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteDisplayNamePrefix + ' ' + Constants.DefaultSiteCode].ToString();
        }

        // Get CMS database connection string based on given sitecode
        public string GetCmsDbString(string siteCode)
        {
            string siteId = GetContentDomainSiteDisplayName(siteCode);
            if (System.Configuration.ConfigurationManager.ConnectionStrings[Constants.DBConfigurationPrefix + ' ' + siteId] != null)
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings[Constants.DBConfigurationPrefix + ' ' + siteId].ConnectionString;
            }
            string logMessage = String.Format("CMS database connection string not found for sitecode {0}", siteCode);
            Logger.Write(logMessage, Category.General, Priority.Highest, EventId.GeneralMessage);
            return string.Empty;
        }

        // get specific (url path) from web.config based on siteCode given
        public string GetContentBaseUrl(string siteCode)
        {

            if (System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteUrlPrefix + ' ' + siteCode] != null)
            {
                return System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteUrlPrefix + ' ' + siteCode].ToString();
            }
            string logMessage = String.Format("{0} siteCode setting not found for base URL. Using default.", siteCode);
            Logger.Write(logMessage, Category.General, Priority.Highest, EventId.GeneralMessage);
            // default to us settings.
            return System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoSiteUrlPrefix + ' ' + Constants.DefaultSiteCode].ToString();
        }

        //Advert Management Changes - added siteCode to read the Logged in and Logged out path from configs
        public string GetContentPage(string userId, string hostPageName, bool aUseLoggedInOut, string siteCode)
        {
            // if user id exists is logged in so goto logged in folder.
            if (aUseLoggedInOut)
            {
                //Advert Management Changes -
                if (!String.IsNullOrEmpty(userId))
                {
                    if (null != ConfigurationManager.AppSettings[Constants.KenticoContentPagesBannerMgmtLoggedIn + ' ' + siteCode])
                    {
                        return string.Format("{0}{1}", ConfigurationManager.AppSettings[Constants.KenticoContentPagesBannerMgmtLoggedIn + ' ' + siteCode], HttpUtility.HtmlEncode(hostPageName));
                    }
                    return string.Format("LoggedIn/{0}", HttpUtility.HtmlEncode(hostPageName));
                }

                if (null != ConfigurationManager.AppSettings[Constants.KenticoContentPagesBannerMgmtLoggedOut + ' ' + siteCode])
                {
                    return string.Format("{0}{1}", ConfigurationManager.AppSettings[Constants.KenticoContentPagesBannerMgmtLoggedOut + ' ' + siteCode], HttpUtility.HtmlEncode(hostPageName));
                }
                return string.Format("LoggedOut/{0}", HttpUtility.HtmlEncode(hostPageName));
            }

            return HttpUtility.HtmlEncode(hostPageName);

        }

        public string GetDivsSelection()
        {
            // get tag selection logic from web.config 
            if (System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoTagsPrefix + ' ' + Constants.DivSelection] != null)
            {
                return System.Configuration.ConfigurationManager.AppSettings[Constants.ConfigKenticoTagsPrefix + ' ' + Constants.DivSelection].ToString();
            }
            // default to these settings
            return "//div[@id='leaderboard' or @id='adrotator' or @id='sidebar']";
        }

        public string ValidateVendorLinkRequest(VendorConfigurableLinksRequest request)
        {
            if (String.IsNullOrEmpty(request.SiteCode)) return RuleMessages.InvalidSiteCode;
            if (String.IsNullOrEmpty(request.UserSelectedCulture) && !request.IgnoreCulture) return RuleMessages.InvalidCultureConfig;
            if (String.IsNullOrEmpty(request.Vendor)) return RuleMessages.VendorpNameEmpty;
            return RuleMessages.ValidationPassed;
        }
    }
}
