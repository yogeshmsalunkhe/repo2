﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IM.CMS.Rules.Kentico
{
    public class Constants
    {
        public const string DefaultSiteCode = "us";
        public const string DefaultCulture = "en-us";
        public const string DivSelection = "divSelection";

        public const string ConfigKenticoTagsPrefix = "KenticoContentSiteTags";
        public const string ConfigKenticoSiteUrlPrefix = "KenticoContentSiteUrl";
        public const string ConfigKenticoSiteDisplayNamePrefix = "KenticoContentSiteDisplayName";
        public const string ConfigKenticoSiteDomainName = "KenticoContentSiteDomainName";
        //Advert Management changes 
        public const string KenticoContentPagesBannerMgmtLoggedIn = "KenticoContentPagesBannerMgmtLoggedIn";
        public const string KenticoContentPagesBannerMgmtLoggedOut = "KenticoContentPagesBannerMgmtLoggedOut";
        public const string DBConfigurationPrefix = "KenticoDBConfig";
        public const string VendorPortalCountryCodeUS = "US";        
    }

    public class EventId
    {
        public const int GeneralMessage = 1000;
    }

    public class RuleMessages
    {
        public const string ValidationPassed = "success";
        public const string InvalidSiteCode = "Invalid SiteCode.";
        public const string InvalidCulture = "Invalid Culture.";
        public const string InvalidCultureConfig = "Invalid CultureConfig.";
        public const string InvalidHostPageName = "Invalid HostPageName.";

        public const string DocumentAttachmentDependentParamMissing =
            "DocumentID should be in the Required Fields to fetch Document Attachment.";

        public const string UpdateFailed = "Update failed.";

        public const string AttachmentNameEmpty = "Attachment name is not specified.";
        public const string AttachmentDataEmpty = "Attachment data is empty.";

        public const string GroupNameEmpty = "Group name is not specified.";

        public const string VendorpNameEmpty = "Vendor name is not specified.";
    }

    public enum DocumentType
    {
        IMAGE, 
        VIDEO,
        PAGE,
        DOCUMENT
    }    
}
